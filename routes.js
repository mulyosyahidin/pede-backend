import { Router } from "express"
import response from "./response.js"
import loginRoutes from './routes/loginroute.js'
import userRoutes from './routes/usersroute.js'
import opdRoutes from './routes/opdusersroute.js'
import priodRoutes from './routes/perioderoute.js'
import urusanRoutes from './routes/urusanroute.js'
import SubUrusanRoutes from './routes/suburusanroute.js'
import rpjmdRoutes from './routes/rpjmdroute.js'
import tahunRoutes from './routes/tahunroute.js'
import instrumenRoutes from './routes/instrumenroute.js'
import triwulanRoutes from './routes/triwulanroute.js'
import renjaRoutes from './routes/renjaroute.js'
import uraianRoutes from './routes/uraianroute.js'
import addHakAkses from './routes/hakaksesroute.js'
import rkpdRoute from "./routes/rkpd-route.js";


const router = Router()

router.use('/login', loginRoutes)
router.use('/user', userRoutes)
router.use('/opd', opdRoutes)
router.use('/priod', priodRoutes)
router.use('/urusan', urusanRoutes)
router.use('/sub', SubUrusanRoutes)
router.use('/rpjmd', rpjmdRoutes)
router.use('/tahun', tahunRoutes)
router.use('/instrumen', instrumenRoutes)
router.use('/triwulan', triwulanRoutes)
router.use('/uraian', uraianRoutes)
router.use('/renja', renjaRoutes)
router.use('/hak',addHakAkses)
router.use('/rkpd', rkpdRoute);

router.all('*', (req,res) => {
    response(res,404,false,'kamu masuk ke path yang salah, coba lagi')
})

export default router