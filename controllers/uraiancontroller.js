import response from "../response.js";
import { inputUraian, listUraian, updateUraian } from "../app/uraianservice.js"

const adding = async (req,res,next) => {
    const data = req.user
    if(data.roleId !== '9'){
        const respon = {
            "pesanError": "Menu ini hanya di akses oleh Admin" 
        }
        return response(res,401,false,'Request data gagal',respon)
    }
    
    try{
        const result = await inputUraian(req)
        return response(res,200,true,'Data uraian berhasil di tambahkan',result)
    }catch(e){
        next(e)
    }
}

const updating = async (req,res,next) => {
    const data = req.user
    if(data.roleId !== '9'){
        const respon = {
            "pesanError": "Menu ini hanya di akses oleh Admin" 
        }
        return response(res,401,false,'Request data gagal',respon)
    }
    
    try{
        const result = await updateUraian(req)
        return response(res,200,true,'Data uraian berhasil di tambahkan',result)
    }catch(e){
        next(e)
    }
}

const fetch = async (req,res,next) => {
    const data = req.user
    if(data.roleId !== '9'){
        const respon = {
            "pesanError": "Menu ini hanya di akses oleh Admin" 
        }
        return response(res,401,false,'Request data gagal',respon)
    }
    
    try{
        const result = await listUraian(req)
        return response(res,200,true,'Data rencana renja berhasil di ambil',result)
    }catch(e){
        next(e)
    }
}

const hapusData = async (req, res, next) => {
    const data = req.user;
    if (data.roleId !== '9') {
        const respon = {
            "pesanError": "Menu ini hanya di akses oleh admin saja"
        }
        return response(res, 401, false, 'Request data gagal', respon)
    }

    try {
        const result = await hapusUraian(req)
        return response(res, 200, true, 'Data uraian berhasil dihapus', result)
    } catch (e) {
        next(e)
    }

}

export{
    adding,
    updating,
    fetch
}