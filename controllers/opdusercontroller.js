import response from "../response.js";
import {listOpd, addOpd, updateOpd, hapusOpd, getSingle, opdKeyword, listAllOpd} from "../app/opdusersservice.js";

const list = async (req, res, next) =>{
    const data = req.user
    if(data.roleId !== '1' && data.roleId !== '9'){
        const respon = {
            "pesanError": "Menu ini hanya di akses oleh administrator saja" 
        }
        return response(res,401,false,'Request data gagal',respon)
    }

    try {
       const result = await listOpd(req)
       return response(res,200,true,'Data OPD berhasil di ambil',result)
    }catch(e){
        next(e)
    }
}

const add = async (req, res, next) =>{
    const data = req.user
    if(data.roleId !== '1'){
        const respon = {
            "pesanError": "Menu ini hanya di akses oleh administrator saja" 
        }
        return response(res,401,false,'Request data gagal',respon)
    }

    try {
        await addOpd(req)
        return response(res,200,true,'Data OPD berhasil ditambahkan')
    }catch(e){
        next(e)
    }
}

const update = async (req, res, next) =>{
    const data = req.user
    if(data.roleId !== '1'){
        const respon = {
            "pesanError": "Menu ini hanya di akses oleh administrator saja" 
        }
        return response(res,401,false,'Request data gagal',respon)
    }

    try {
        await updateOpd(req)
        return response(res,200,true,'Data OPD berhasil diubah')
    }catch(e){
        next(e)
    }
}

const hapus = async (req, res, next) =>{
    const data = req.user
    if(data.roleId !== '1'){
        const respon = {
            "pesanError": "Menu ini hanya di akses oleh administrator saja" 
        }
        return response(res,401,false,'Request data gagal',respon)
    }

    try {
        await hapusOpd(req)
        return response(res,200,true,'Data OPD berhasil dihapus')
    }catch(e){
        next(e)
    }
}

const getSingles = async (req, res, next) =>{
    const data = req.user
    if(data.roleId !== '1' && data.roleId !== '9'){
        const respon = {
            "pesanError": "Menu ini hanya di akses oleh administrator saja" 
        }
        return response(res,401,false,'Request data gagal',respon)
    }

    try {
        const result = await getSingle(req)
        return response(res,200,true,'Data OPD berhasil diambil', result)
    }catch(e){
        next(e)
    }
}


const listKeyword = async (req, res, next) =>{
    try {
       const result = await opdKeyword(req)
       return response(res,200,true,'Data OPD berhasil di ambil',result)
    }catch(e){
        next(e)
    }
}

const listAll  = async (req, res, next) =>{
    try {
       const result = await listAllOpd(req)
       return response(res,200,true,'Data OPD berhasil di ambil',result)
    }catch(e){
        next(e)
    }
}

export {
    list,
    add,
    update,
    hapus,
    getSingles,
    listKeyword,
    listAll,
}