import response from "../response.js";
import {inputEval,listEval,listTujuan,updateTujuan, logInstrumen, getTujuanByIdSubUrusan, getSasaranByIdTujuan, getProgramByIdSasaran, getKegiatanByIdProgram, getSubByIdKegiatan, hapusInstrumen} from "../app/instrumenservice.js"



const inputEvals = async (req,res,next)=>{
    const data = req.user
    if(data.roleId !== '9'){
        const respon = {
            "pesanError": "Menu ini hanya di akses oleh Admin " 
        }
        return response(res,401,false,'Request data gagal',respon)
    }
    try {
        const result = await inputEval(req)
        return response(res,200,true,'Data indikator instrumen berhasil ditambhkan',result)
    }catch(e){
        next(e)
    }
} 

const listEvals = async (req,res,next)=>{
    try {
        const result = await listEval(req)
        return response(res,200,true,'Data indikator instrumen berhasil diambil',result)
    }catch(e){
        next(e)
    }
} 

const tujuanList = async (req,res,next)=>{
    const data = req.user
    try{
        const result = await listTujuan(req)
        return response(res,200,true,'Data tujuan instrumen berhasil diambil',result)
    }catch(e){
        next(e)
    }
}

const hapusData = async (req,res,next)=> {
    const data = req.user
    if(data.roleId !== '9'){
        const respon = {
            "pesanError": "Menu ini hanya di akses oleh admin saja" 
        }
        return response(res,401,false,'Request data gagal',respon)
    }

    try{
        await hapusInstrumen(req)
        return response(res,200,true,'Data instrumen berhasil dihapus')
    }catch(e){
        next(e)
    }
}

const updateData = async (req,res,next)=>{
    const data = req.user
    if(data.roleId !== '9'){
        const respon = {
            "pesanError": "Menu ini hanya di akses oleh Admin saja" 
        }
        return response(res,401,false,'Request data gagal',respon)
    }

    try{
       const result = await updateTujuan(req)
        return response(res,200,true,'Data intrumen berhasil di Ubah',result)
    }catch(e){
        next(e)
    }
}

const instrumenLog = async (req,res,next)=>{
    try{
       const result = await logInstrumen(req)
        return response(res,200,true,'Data log berhasil di ambil',result)
    }catch(e){
        next(e)
    }
}

const getTujuanBysub = async (req,res,next)=>{
    try {
        const result = await getTujuanByIdSubUrusan(req)
        return response(res,200,true,'Data tujuan berhasil diambil',result)
    }catch(e){
        next(e)
    }
} 

const getSasaranByTujuan = async (req,res,next)=>{
    try {
        const result = await getSasaranByIdTujuan(req)
        return response(res,200,true,'Data Sasaran berhasil diambil',result)
    }catch(e){
        next(e)
    }
}

const getProgramBySasaran = async (req,res,next)=>{
    try {
        const result = await getProgramByIdSasaran(req)
        return response(res,200,true,'Data Program berhasil diambil',result)
    }catch(e){
        next(e)
    }
}

const getKegiatanByProgram = async (req,res,next)=>{
    try {
        const result = await getKegiatanByIdProgram(req)
        return response(res,200,true,'Data kegiatan berhasil diambil',result)
    }catch(e){
        next(e)
    }
}

const getSubByKegiatan = async (req,res,next)=>{
    try {
        const result = await getSubByIdKegiatan(req)
        return response(res,200,true,'Data sub kegiatan berhasil diambil',result)
    }catch(e){
        next(e)
    }
}

export {
    hapusData,
    inputEvals,
    listEvals,
    tujuanList,
    updateData,
    instrumenLog,
    getTujuanBysub,
    getSasaranByTujuan,
    getProgramBySasaran,
    getKegiatanByProgram,
    getSubByKegiatan
}