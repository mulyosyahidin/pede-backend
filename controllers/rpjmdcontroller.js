import { addFaktor, addRealisasi, approval, finalisasiRpjmd, getCatatan, getReport, getRevisi, listApproval, listCapaian, listFaktor, updateCapaian, updateFaktor} from "../app/rpjmdservice.js";
import response from "../response.js";

const inputRealisasi = async (req,res,next) => {
    const data = req.user
    if(data.roleId !== '2'){
        const respon = {
            "pesanError": "Menu ini hanya di akses oleh Admin OPD saja" 
        }
        return response(res,401,false,'Request data gagal',respon)
    }

    try {
        const result = await addRealisasi(req)
        return response(res,200,true,'Data Realisasi berhasil di tambahkan',result)
    }catch(e){
        next(e)
    }
}

const updateRealisasi = async (req,res,next) => {
    const data = req.user
    if(data.roleId !== '2'){
        const respon = {
            "pesanError": "Menu ini hanya di akses oleh Admin OPD saja" 
        }
        return response(res,401,false,'Request data gagal',respon)
    }

    try{
        const result = await updateCapaian(req)
        return response(res,200,true,'Data Realisasi berhasil diubah',result)
    }catch(e){
        next(e)
    }
}

const fetchCapaian = async (req,res,next)=>{
    const data = req.user
    if(data.roleId !== '2'){
        const respon = {
            "pesanError": "Menu ini hanya di akses oleh Admin OPD saja" 
        }
        return response(res,401,false,'Request data gagal',respon)
    }

    try {
        const result = await listCapaian(req)
        return response(res,200,true,'Data Realisasi berhasil diambil',result)
    }catch(e){
        next(e)
    }
}

const createFaktor = async (req,res,next) => {
    const data = req.user
    if(data.roleId !== '2'){
        const respon = {
            "pesanError": "Menu ini hanya di akses oleh Admin OPD saja" 
        }
        return response(res,401,false,'Request data gagal',respon)
    }

    try {
        const result = await addFaktor(req)
        return response(res,200,true,'Data faktor berhasil di add',result)
    }catch(e){
        next(e)
    }
}

const ubahFaktor = async (req,res,next)=>{
    const data = req.user
    if(data.roleId !== '2'){
        const respon = {
            "pesanError": "Menu ini hanya di akses oleh Admin OPD saja" 
        }
        return response(res,401,false,'Request data gagal',respon)
    }

    try {
        const result = await updateFaktor(req)
        return response(res,200,true,'Data faktor berhasil diubah',result)
    }catch(e){
        next(e)
    }
}

const fetchFaktor = async (req,res,next) => {
    const data = req.user
    if(data.roleId !== '2'){
        const respon = {
            "pesanError": "Menu ini hanya di akses oleh Admin OPD saja" 
        }
        return response(res,401,false,'Request data gagal',respon)
    }

    try{
        const result = await listFaktor(req)
        return response(res,200,true,'Data faktor berhasil diambil',result)
    }catch(e){
        next(e)
    }
}

const fetchReport = async (req,res,next)=>{
    try {
        const result = await getReport(req)
        return response(res,200,true,'Data faktor berhasil diambil', result)
    }catch(e){
        next(e)
    }
}

const approvRpjmd = async (req,res,next) => {
    const data = req.user
    if(data.roleId !== '2'){
        const respon = {
            "pesanError": "Menu ini hanya di akses oleh Admin OPD saja" 
        }
        return response(res,401,false,'Request data gagal',respon)
    }

    try {
        const result = await finalisasiRpjmd(req)
        return response(res,200,true,'Data faktor berhasil diambil',result)
    }catch(e){
        next(e)
    }
}

const fetchApproval = async (req,res,next)=> {
    const data = req.user
    if(data.roleId !== '3' && data.roleId !== '4'&& data.roleId !== '5'&& data.roleId !== '6'&& data.roleId !== '7'&& data.roleId !== '8'){
        const respon = {
            "pesanError": "Menu ini tidak dapat di akses" 
        }
        return response(res,401,false,'Request data gagal',respon)
    }

    try{
        const result = await listApproval(req)
        return response(res,200,true,'Data RPJMD berhasil dihapus',result)
    }catch(e){
        next(e)
    }
}

const actionApproval = async (req,res,next)=> {
    const data = req.user
    if(data.roleId !== '3' && data.roleId !== '4'&& data.roleId !== '5'&& data.roleId !== '6'&& data.roleId !== '7'&& data.roleId !== '8'){
        const respon = {
            "pesanError": "Menu ini tidak dapat di akses" 
        }
        return response(res,401,false,'Request data gagal',respon)
    }

    try{
        await approval(req)
        return response(res,200,true,'Aksi berhasil')
    }catch(e){
        next(e)
    }
}

const fetchRevisi = async (req,res,next)=> {
    const data = req.user
    if(data.roleId !== '2' && data.roleId !== '3'&& data.roleId !== '4'&& data.roleId !== '5'&& data.roleId !== '6'){
        const respon = {
            "pesanError": "Menu ini tidak dapat di akses" 
        }
        return response(res,401,false,'Request data gagal',respon)
    }

    try{
        const result = await getRevisi(req)
        return response(res,200,true,'Data revisi berhasil di ambil',result)
    }catch(e){
        next(e)
    }
}

const fetchCatatan = async (req,res,next)=> {
    const data = req.user
    if(data.roleId !== '2' && data.roleId !== '3'&& data.roleId !== '4'&& data.roleId !== '5'&& data.roleId !== '6'){
        const respon = {
            "pesanError": "Menu ini tidak dapat di akses" 
        }
        return response(res,401,false,'Request data gagal',respon)
    }

    try{
        const result = await getCatatan(req)
        return response(res,200,true,'Data catatan berhasil di ambil',result)
    }catch(e){
        next(e)
    }
}
export {
    inputRealisasi,
    updateRealisasi,
    fetchCapaian,
    createFaktor,
    ubahFaktor,
    fetchFaktor,
    fetchReport,
    approvRpjmd,
    fetchApproval,
    actionApproval,
    fetchRevisi,
    fetchCatatan
}