import response from "../response.js";
import { add, getByNamaSub, getByUrusan, getSingleData, hapus, listAll, update } from "../app/suburusanservice.js";

const adds = async (req,res,next) => {
    const data = req.user
    if(data.roleId !== '9'){
        const respon = {
            "pesanError": "Menu ini hanya di akses oleh administrator saja" 
        }
        return response(res,401,false,'Request data gagal',respon)
    }

    try {
        const result = await add(req)
        return response(res,200,true,'Data Sub Urusan berhasil ditambahkan',result)
    }catch (e) {
        next(e)
    }
}

const updates = async (req,res,next) => {
    const data = req.user
    if(data.roleId !== '9'){
        const respon = {
            "pesanError": "Menu ini hanya di akses oleh administrator saja" 
        }
        return response(res,401,false,'Request data gagal',respon)
    }

    try {
        const result = await update(req)
        return response(res,200,true,'Data Sub Urusan berhasil diubah',result)
    }catch (e) {
        next(e)
    }
}

const deletes = async (req,res,next) => {
    const data = req.user
    if(data.roleId !== '9'){
        const respon = {
            "pesanError": "Menu ini hanya di akses oleh administrator saja" 
        }
        return response(res,401,false,'Request data gagal',respon)
    }

    try {
        await hapus(req)
        return response(res,200,true,'Data Sub Urusan berhasil dihapus')
    }catch (e) {
        next(e)
    }
}

const list = async (req,res,next) => {
    const data = req.user
    if(data.roleId !== '9'){
        const respon = {
            "pesanError": "Menu ini hanya di akses oleh administrator saja" 
        }
        return response(res,401,false,'Request data gagal',respon)
    }

    try {
        const result = await listAll(req)
        // console.log(result)
        return response(res,200,true,'Data Sub Urusan berhasil diambil',result)
    }catch (e) {
        next(e)
    }
}

const getUrusanId = async (req,res,next) => {
    try {
        const result = await getByUrusan(req)
        return response(res,200,true,'Data Sub Urusan berhasil diambil',result)
    }catch (e) {
        next(e)
    }
}

const getByname = async (req,res,next) => {
    try {
        const result = await getByNamaSub(req)
        return response(res,200,true,'Data Sub Urusan berhasil diambil',result)
    }catch (e) {
        next(e)
    }
}

const getSingle = async (req,res,next) => {
    const data = req.user
    if(data.roleId !== '9'){
        const respon = {
            "pesanError": "Menu ini hanya di akses oleh administrator saja" 
        }
        return response(res,401,false,'Request data gagal',respon)
    }

    try {
        const result = await getSingleData(req)
        return response(res,200,true,'Data Sub Urusan berhasil diambil',result)
    }catch (e) {
        next(e)
    }
}

export {
    adds,
    updates,
    deletes,
    list,
    getUrusanId,
    getByname,
    getSingle
}