import response from "../response.js";
import { list} from "../app/triwulanservice.js"

const listing = async (req,res,next) => {
    const data = req.user
    if(data.roleId !== '9'){
        const respon = {
            "pesanError": "Menu ini hanya di akses oleh Admin" 
        }
        return response(res,401,false,'Request data gagal',respon)
    }
    
    try{
        const result = await list(req)
        return response(res,200,true,'Data target triwulan berhasil di ambil',result)
    }catch(e){
        next(e)
    }
}
export {
    listing
}