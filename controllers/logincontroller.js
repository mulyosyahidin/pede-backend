import response from "../response.js";
import { countAuth } from "../app/loginservice.js";

const auth = async (req, res, next) => {
    try{
        const hasil = await countAuth(req);
       return response(res,200,true,'Login Berhasil',hasil)
    }catch(e){
        next(e)
    }
    
}

export {auth}
