import response from "../response.js";
import {inputCapaian, inputRupiah, listCapaian, listCapaianRupiah, renjaListFinalisasi} from "../app/renjaservice.js"
import { addOutcome, fetchDataOutcome } from "../app/outcomeservice.js"
import {addFaktor, excelLaporanF, listLaporan} from "../app/faktorrenjaservice.js"
import * as fs from "fs";

const input = async (req, res, next) =>{
    const data = req.user
    if(data.roleId !== '2'){
        const respon = {
            "pesanError": "Menu ini hanya di akses oleh PPTK" 
        }
        return response(res,401,false,'Request data gagal',respon)
    }

    try {
       const result = await inputCapaian(req)
       return response(res,200,true,'Data capaian berhasil di input',result)
    }catch(e){
        next(e)
    }
}

const fetch = async (req, res, next) =>{
    try {
       const result = await listCapaian(req)
       return response(res,200,true,'Data capaian berhasil diambil',result)
    }catch(e){
        next(e)
    }
}

const rupiahInput = async (req, res, next) =>{
    const data = req.user
    if(data.roleId !== '2'){
        const respon = {
            "pesanError": "Menu ini hanya di akses oleh PPTK" 
        }
        return response(res,401,false,'Request data gagal',respon)
    }

    try {
       const result = await inputRupiah(req)
       return response(res,200,true,'Data capaian berhasil di input',result)
    }catch(e){
        next(e)
    }
}

const rupiahList = async (req, res, next) =>{
    const data = req.user
    if(data.roleId !== '2'){
        const respon = {
            "pesanError": "Menu ini hanya di akses oleh PPTK" 
        }
        return response(res,401,false,'Request data gagal',respon)
    }

    try {
       const result = await listCapaianRupiah(req)
       return response(res,200,true,'Data capaian berhasil di input',result)
    }catch(e){
        next(e)
    }
}


const inputOutcome = async (req, res, next) =>{
    const data = req.user
    if(data.roleId !== '2'){
        const respon = {
            "pesanError": "Menu ini hanya di akses oleh PPTK" 
        }
        return response(res,401,false,'Request data gagal',respon)
    }

    try {
       const result = await addOutcome(req)
       return response(res,200,true,'Input Outcome berhasil',result)
    }catch(e){
        next(e)
    }
}

const listOutcome = async (req, res, next) =>{
    try {
       const result = await fetchDataOutcome(req)
       return response(res,200,true,'Data capaian outcome berhasild di tampilkan',result)
    }catch(e){
        next(e)
    }
}

const inputFaktor = async (req, res, next) =>{
    const data = req.user
    if(data.roleId !== '2'){
        const respon = {
            "pesanError": "Menu ini hanya di akses oleh PPTK" 
        }
        return response(res,401,false,'Request data gagal',respon)
    }

    try {
       const result = await addFaktor(req)
       return response(res,200,true,'Input faktor berhasil',result)
    }catch(e){
        next(e)
    }
}

const laporanAkhir = async (req, res, next) =>{
    try {
       const result = await listLaporan(req)
       return response(res,200,true,'Data laporan berhasil di tampilkan',result)
    }catch(e){
        next(e)
    }
}

const excelLaporan = async (req, res, next) => {
    try {
        const result = await excelLaporanF(req);

        // return response(res, result.code, result.success, result.message, result.data);

        const fileName = result.data.file_name;
        const filePath = `${process.cwd()}/public/laporan/${fileName}`;

        if (fs.existsSync(filePath)) {
            res.download(filePath, fileName, (err) => {
                if (err) {
                    return response(res, 500, false, 'Gagal mendownload file', err);
                }

                fs.unlinkSync(filePath);
            });
        } else {
            return response(res, 404, false, 'File tidak ditemukan', null);
        }
    } catch (e) {
        next(e)
    }
}

const listFinalisasi = async (req, res, next) => {
    try {
        const result = await renjaListFinalisasi(req);

        return response(res, 200, true, 'Data berhasil diambil', result);
    }
    catch (e) {
        next(e);
    }
}

export {
    input,
    fetch,
    rupiahInput,
    rupiahList,
    inputOutcome,
    listOutcome,
    inputFaktor,
    laporanAkhir,
    excelLaporan,
    listFinalisasi,
}