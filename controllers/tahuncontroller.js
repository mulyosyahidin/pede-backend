import response from "../response.js";
import{addTahun,updateTahun,listTahun} from "../app/tahunservice.js"

const tahun = async (req,res,next) => {
    const data = req.user
    if(data.roleId !== '1'){
        const respon = {
            "pesanError": "Menu ini hanya di akses oleh Admin" 
        }
        return response(res,401,false,'Request data gagal',respon)
    }
    
    try{
        const result = await addTahun(req)
        return response(res,200,true,'Data tahun berhasil di tambahkan',result)
    }catch(e){
        next(e)
    }
}

const tahunUpdate = async (req,res,next) => {
    const data = req.user
    if(data.roleId !== '1'){
        const respon = {
            "pesanError": "Menu ini hanya di akses oleh Admin" 
        }
        return response(res,401,false,'Request data gagal',respon)
    }

    try {
        const result = await updateTahun(req)
        return response(res,200,true,'Data tahun berhasil diubah',result)
    }catch(e){
        next(e)
    }
}

const tahunList = async (req,res,next) => {
    const data = req.user
    if(data.roleId !== '1' && data.roleId !== '9'){
        const respon = {
            "pesanError": "Menu ini hanya di akses oleh Admin" 
        }
        return response(res,401,false,'Request data gagal',respon)
    }
    try {
        const result = await listTahun(req)
        return response(res,200,true,'Data tahun berhasil diambil',result)
    }catch(e){
        next(e)
    }
}

export {
    tahun,
    tahunUpdate,
    tahunList
}