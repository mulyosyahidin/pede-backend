import { addUrusan, filterByName, hapusUrusan, listAll, singleFetch, updateUrusan } from "../app/urusanservice.js";
import response from "../response.js";

const add = async (req,res,next) => {
    const data = req.user
    if(data.roleId !== '9'){
        const respon = {
            "pesanError": "Menu ini hanya di akses oleh administrator saja" 
        }
        return response(res,401,false,'Request data gagal',respon)
    }

    try{
        const result = await addUrusan(req)
        return response(res,200,true,'Data Urusan berhasil ditambahkan',result)
    }catch (e){
        next(e)
    }
}

const update = async (req,res,next) => {
    const data = req.user
    if(data.roleId !== '9'){
        const respon = {
            "pesanError": "Menu ini hanya di akses oleh administrator saja" 
        }
        return response(res,401,false,'Request data gagal',respon)
    }

    try{
        await updateUrusan(req)
        return response(res,200,true,'Data Urusan berhasil diubah')
    }catch (e){
        next(e)
    }
}

const hapus = async (req,res,next) => {
    const data = req.user
    if(data.roleId !== '9'){
        const respon = {
            "pesanError": "Menu ini hanya di akses oleh administrator saja" 
        }
        return response(res,401,false,'Request data gagal',respon)
    }

    try{
        await hapusUrusan(req)
        return response(res,200,true,'Data Urusan berhasil dihapus')
    }catch (e){
        next(e)
    }
}

const listAlls = async (req,res,next) => {
    try{
        const result = await listAll(req)
        return response(res,200,true,'Data Urusan berhasil diambil', result)
    }catch (e){
        next(e)
    }
}

const singleGet = async (req,res,next) => {
    const data = req.user
    if(data.roleId !== '9'){
        const respon = {
            "pesanError": "Menu ini hanya di akses oleh administrator saja" 
        }
        return response(res,401,false,'Request data gagal',respon)
    }

    try{
        const result = await singleFetch(req)
        return response(res,200,true,'Data Urusan berhasil diambil', result)
    }catch (e){
        next(e)
    }
}

const byName = async (req,res,next) => {
    
    try{
        const result = await filterByName(req)
        return response(res,200,true,'Data Urusan berhasil diambil', result)
    }catch (e){
        next(e)
    }
}

export {
    add,
    update,
    hapus,
    listAlls,
    singleGet,
    byName
}