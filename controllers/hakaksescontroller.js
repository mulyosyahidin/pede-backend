import response from "../response.js";
import { addHakAkses } from "../app/hakaksesservice.js"

const addHak = async (req,res,next)=>{
    const data = req.user
    if(data.roleId !== '1'){
        const respon = {
            "pesanError": "Menu ini hanya di akses oleh Admin " 
        }
        return response(res,401,false,'tambah data gagal',respon)
    }
    try {
        const result = await addHakAkses(req)
        return response(res,200,true,'Hak akses berhasil di tambahkan',result)
    }catch(e){
        next(e)
    }
}

export{
    addHak
}