import { addUser, updateUser, hapusUser, listAllUser, getUser, getUserAll, removeToken, updateSingleUser, userRole, listUserKeyword } from "../app/userservice.js";
import response from "../response.js";

const add = async (req, res, next) =>{
    const data = req.user
    if(data.roleId !== '1'){
        const respon = {
            "pesanError": "Menu ini hanya di akses oleh administrator saja" 
        }
        return response(res,401,false,'Add User Gagal',respon)
    }

    try {
        const result = await addUser(req)
       return response(res,200,true,'Add user berhasil',result)
    }catch(e){
        next(e)
    }
}

const update = async (req, res, next) =>{
    const data = req.user
    if(data.roleId !== '1'){
        const respon = {
            "pesanError": "Menu ini hanya di akses oleh administrator saja" 
        }
       return response(res,401,false,'Add User Gagal',respon)
    }

    try {
        const result = await updateUser(req)
      return  response(res,200,true,'Data User berhasil di ubah')
    }catch(e){
        next(e)
    }
}

const hapus = async (req, res, next) =>{
    const data = req.user
    if(data.roleId !== '1'){
        const respon = {
            "pesanError": "Menu ini hanya di akses oleh administrator saja" 
        }
       return response(res,401,false,'Add User Gagal',respon)
    }

    try {
      const result = await hapusUser(req)
      return  response(res,200,true,'Data User berhasil di hapus')
    }catch(e){
        next(e)
    }
}

const listAll = async (req, res, next) => {
    const data = req.user
    if(data.roleId !== '1'){
        const respon = {
            "pesanError": "Menu ini hanya di akses oleh administrator saja" 
        }
       return response(res,401,false,'Add User Gagal',respon)
    }

    try {
        const result = await listAllUser(req)
        return response(res,200,true,'Data Berhasil Diambil',result)
    } catch(e){
        next(e)
    }
}

const getUsers = async (req, res, next) => {
    const data = req.user
    if(data.roleId !== '1'){
        const respon = {
            "pesanError": "Menu ini hanya di akses oleh administrator saja" 
        }
       return response(res,401,false,'Add User Gagal',respon)
    }

    try {
        const result = await getUser(req)
        return response(res,200,true,'Data Berhasil Diambil',result)
    } catch(e){
        next(e)
    }
}

const getUserSingle= async (req, res, next) => {
    try {
        const result = await getUserAll(req)
        return response(res,200,true,'Data Berhasil Diambil',result)
    } catch(e){
        next(e)
    }
}

const removeTokens= async (req, res, next) => {
    try {
        await removeToken(req)
        return response(res,200,true,'Token berhasil terhapus')
    } catch(e){
        next(e)
    }
}

const updateUsers = async (req, res, next) => {
    try {
        await updateSingleUser(req)
        return response(res,200,true,'Data user berhasil di ubah')
    } catch(e){
        next(e)
    }
}

const roleAlluser = async (req,res,next) => {
    const data = req.user
    if(data.roleId !== '1'){
        const respon = {
            "pesanError": "Menu ini hanya di akses oleh administrator saja" 
        }
       return response(res,401,false,'Add User Gagal',respon)
    }

    try {
        const result = await userRole(req)
        return response(res,200,true,'Data berhasil di ambil',result)
    }catch(e){
        next(e)
    }
}

const listKeyword= async (req, res, next) => {
    const data = req.user
    if(data.roleId !== '1'){
        const respon = {
            "pesanError": "Menu ini hanya di akses oleh administrator saja" 
        }
       return response(res,401,false,'Add User Gagal',respon)
    }

    try {
        const result = await listUserKeyword(req)
        return response(res,200,true,'Data Berhasil Diambil',result)
    } catch(e){
        next(e)
    }
}




export {
    add,
    update,
    hapus,
    listAll,
    getUsers,
    getUserSingle,
    removeTokens,
    updateUsers,
    roleAlluser,
    listKeyword
}