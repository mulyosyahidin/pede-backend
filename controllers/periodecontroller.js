import response from "../response.js";
import {addPeriode, getSingleData, getSingleTahun, hapus, listData, priodGetUser, updatePeriode} from "./../app/periodeservice.js"

const add = async (req,res,next) => {
    const data = req.user
    if(data.roleId !== '1'){
        const respon = {
            "pesanError": "Menu ini hanya di akses oleh administrator saja" 
        }
        return response(res,401,false,'Request data gagal',respon)
    }

    try {
        const result = await addPeriode(req,res)
        return response(res,200,true,"Data Periode berhasil ditambahkan",result)
    }catch (e){
        next(e)
    }
}

const update = async (req,res,next) => {
    const data = req.user
    if(data.roleId !== '1'){
        const respon = {
            "pesanError": "Menu ini hanya di akses oleh administrator saja" 
        }
        return response(res,401,false,'Request data gagal',respon)
    }

    try {
        await updatePeriode(req)
        return response(res,200,true,"Data Periode berhasil diubah")
    }catch (e){
        next(e)
    }
}

const list = async (req,res,next) => {
    const data = req.user
    if(data.roleId !== '1' && data.roleId !== '9'){
        const respon = {
            "pesanError": "Menu ini hanya di akses oleh administrator saja" 
        }
        return response(res,401,false,'Request data gagal',respon)
    }

    try{
        const results = await listData(req)
        return response(res,200,true,"Data Periode berhasil diambil",results)
    }catch(e){
        next(e)
    }
}

const hapusPeriode = async (req,res,next) => {
    const data = req.user
    if(data.roleId !== '1'){
        const respon = {
            "pesanError": "Menu ini hanya di akses oleh administrator saja" 
        }
        return response(res,401,false,'Request data gagal',respon)
    }

    try{
        await hapus(req)
        return response(res,200,true,"Data Periode berhasil dihapus")
    }catch(e){
        next(e)
    }
}

const getSingle = async (req,res,next) => {
    const data = req.user
    if(data.roleId !== '1' && data.roleId !== '9'){
        const respon = {
            "pesanError": "Menu ini hanya di akses oleh administrator saja" 
        }
        return response(res,401,false,'Request data gagal',respon)
    }

    try{
        const results = await getSingleData(req)
        return response(res,200,true,"Data Periode berhasil diambil",results)
    }catch(e){
        next(e)
    }
}

const getTahun = async (req,res,next) => {
    const data = req.user
    if(data.roleId !== '1' && data.roleId !== '9'){
        const respon = {
            "pesanError": "Menu ini hanya di akses oleh administrator saja" 
        }
        return response(res,401,false,'Request data gagal',respon)
    }

    try{
        const results = await getSingleTahun(req)
        return response(res,200,true,"Data Periode berhasil diambil",results)
    }catch(e){
        next(e)
    }
}

const getUser = async (req,res,next) => {
    try{
        const results = await priodGetUser(req)
        return response(res,200,true,"Data Periode berhasil diambil",results)
    }catch(e){
        next(e)
    }
}


export {
    add,
    update,
    list,
    hapusPeriode,
    getSingle,
    getTahun,
    getUser
}
