/*
  Warnings:

  - You are about to alter the column `namaPeriode` on the `akumulasiperiode` table. The data in that column could be lost. The data in that column will be cast from `VarChar(500)` to `VarChar(191)`.

*/
-- AlterTable
ALTER TABLE `akumulasiperiode` MODIFY `namaPeriode` VARCHAR(191) NULL;

-- AlterTable
ALTER TABLE `periode` MODIFY `namaPeriode` VARCHAR(500) NULL;
