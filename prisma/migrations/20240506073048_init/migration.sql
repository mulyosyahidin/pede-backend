-- CreateTable
CREATE TABLE `users` (
    `userId` INTEGER NOT NULL AUTO_INCREMENT,
    `username` VARCHAR(25) NOT NULL,
    `email` VARCHAR(35) NOT NULL,
    `password` VARCHAR(100) NOT NULL,
    `roleId` VARCHAR(50) NOT NULL,
    `odpId` VARCHAR(50) NOT NULL,
    `token` VARCHAR(100) NULL,
    `idCreate` VARCHAR(50) NOT NULL,
    `waktuBerlaku` DATETIME(3) NULL,

    PRIMARY KEY (`userId`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `roleUser` (
    `roleId` INTEGER NOT NULL,
    `namaRole` VARCHAR(35) NOT NULL,

    PRIMARY KEY (`roleId`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `opdUser` (
    `opdId` INTEGER NOT NULL AUTO_INCREMENT,
    `namaOpd` VARCHAR(191) NOT NULL,

    PRIMARY KEY (`opdId`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `namaUser` (
    `namaId` INTEGER NOT NULL AUTO_INCREMENT,
    `userId` VARCHAR(50) NOT NULL,
    `namaUser` VARCHAR(35) NOT NULL,

    PRIMARY KEY (`namaId`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `periode` (
    `idPeriode` INTEGER NOT NULL AUTO_INCREMENT,
    `tahunPeriode` INTEGER NOT NULL,
    `namaPeriode` VARCHAR(500) NOT NULL,
    `waktuMulai` DATETIME(3) NULL,
    `waktuAkhir` DATETIME(3) NULL,
    `status` INTEGER NULL,

    PRIMARY KEY (`idPeriode`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `akumulasiPeriode` (
    `idAkumulasi` INTEGER NOT NULL AUTO_INCREMENT,
    `periode` INTEGER NOT NULL,
    `namaPeriode` VARCHAR(500) NOT NULL,
    `opd` INTEGER NOT NULL,
    `status` INTEGER NOT NULL,

    PRIMARY KEY (`idAkumulasi`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `urusan` (
    `idUrusan` INTEGER NOT NULL AUTO_INCREMENT,
    `namaUrusan` VARCHAR(100) NOT NULL,
    `idCreate` INTEGER NOT NULL,
    `createDate` DATETIME(3) NOT NULL,

    PRIMARY KEY (`idUrusan`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `subUrusan` (
    `idSub` INTEGER NOT NULL AUTO_INCREMENT,
    `urusan` INTEGER NOT NULL,
    `namaSubUrusan` VARCHAR(100) NOT NULL,
    `idCreate` INTEGER NOT NULL,
    `createDate` DATETIME(3) NOT NULL,

    PRIMARY KEY (`idSub`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `instrumen` (
    `idInstrumen` INTEGER NOT NULL AUTO_INCREMENT,
    `urusan` INTEGER NOT NULL,
    `parent` INTEGER NULL,
    `subUrusan` INTEGER NOT NULL,
    `periode` INTEGER NULL,
    `tujuan` VARCHAR(191) NOT NULL,
    `dateCreate` DATETIME(3) NOT NULL,
    `idCreate` INTEGER NOT NULL,
    `type` VARCHAR(191) NOT NULL,
    `opd` INTEGER NULL,
    `paguIndikatif` INTEGER NULL DEFAULT 254,

    PRIMARY KEY (`idInstrumen`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `indikatorInstrumen` (
    `idIndikator` INTEGER NOT NULL AUTO_INCREMENT,
    `idInstrumen` INTEGER NOT NULL,
    `indikator_kinerja` VARCHAR(191) NOT NULL,
    `data_capaian_awal` VARCHAR(191) NULL,
    `target_akhir_periode` VARCHAR(191) NULL,
    `satuan` VARCHAR(191) NULL,
    `target` INTEGER NULL DEFAULT 254,
    `data_capaian_akhir` INTEGER NULL DEFAULT 254,
    `rasio_capaian_akhir` VARCHAR(191) NULL,

    PRIMARY KEY (`idIndikator`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `targetRpmjd` (
    `idTargetRpjmd` INTEGER NOT NULL AUTO_INCREMENT,
    `indikator` INTEGER NOT NULL,
    `tahunKe` INTEGER NOT NULL,
    `capaian_rpjmd` INTEGER NOT NULL,
    `tingkatCapaianTarget` INTEGER NULL DEFAULT 254,
    `dateCreate` DATETIME(3) NOT NULL,
    `idCreate` INTEGER NOT NULL,

    PRIMARY KEY (`idTargetRpjmd`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `faktorRpjmd` (
    `idFaktorRpjmd` INTEGER NOT NULL AUTO_INCREMENT,
    `indikator` INTEGER NOT NULL,
    `tahunKe` INTEGER NOT NULL,
    `faktor_penghambat` VARCHAR(191) NOT NULL,
    `faktor_pendorong` VARCHAR(191) NOT NULL,
    `rekomendasi_tl` VARCHAR(191) NOT NULL,
    `dateCreate` DATETIME(3) NOT NULL,
    `idCreate` INTEGER NOT NULL,

    PRIMARY KEY (`idFaktorRpjmd`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `historiRpjmd` (
    `idHisRpmjd` INTEGER NOT NULL AUTO_INCREMENT,
    `finalisasi` INTEGER NOT NULL,
    `cekPoint` INTEGER NOT NULL,
    `periode` INTEGER NOT NULL,
    `odp` INTEGER NOT NULL,
    `rpjmd` INTEGER NOT NULL,

    PRIMARY KEY (`idHisRpmjd`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `catatanRpmjd` (
    `idCatatanRpmjd` INTEGER NOT NULL AUTO_INCREMENT,
    `histori` INTEGER NOT NULL,
    `catatan` VARCHAR(191) NULL,
    `idUser` INTEGER NOT NULL,

    PRIMARY KEY (`idCatatanRpmjd`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `approvalRpmjd` (
    `idApprovRpmjd` INTEGER NOT NULL AUTO_INCREMENT,
    `histori` INTEGER NOT NULL,
    `roleId` INTEGER NULL,
    `idUser` INTEGER NOT NULL,
    `dateCreate` DATETIME(3) NOT NULL,

    PRIMARY KEY (`idApprovRpmjd`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tahunRpmjd` (
    `idTh` INTEGER NOT NULL AUTO_INCREMENT,
    `tahunSatu` INTEGER NULL,
    `tahunDua` INTEGER NULL,
    `tahunTiga` INTEGER NULL,
    `tahunEmpat` INTEGER NULL,
    `tahunLima` INTEGER NULL,
    `tahunEnam` INTEGER NULL,
    `status` INTEGER NULL,

    PRIMARY KEY (`idTh`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `logAktivitas` (
    `idLog` INTEGER NOT NULL AUTO_INCREMENT,
    `userId` INTEGER NOT NULL,
    `data` INTEGER NOT NULL,
    `dateCreate` DATETIME(3) NOT NULL,
    `aktiviti` VARCHAR(191) NULL,

    PRIMARY KEY (`idLog`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `targetTriwulan` (
    `idTriwulan` INTEGER NOT NULL AUTO_INCREMENT,
    `idUser` INTEGER NOT NULL,
    `dateCreate` DATETIME(3) NOT NULL,
    `idSubKegiatan` INTEGER NOT NULL,
    `tahun` INTEGER NOT NULL,
    `opd` INTEGER NOT NULL,
    `t1` INTEGER NULL DEFAULT 254,
    `t2` INTEGER NULL DEFAULT 254,
    `t3` INTEGER NULL DEFAULT 254,
    `t4` INTEGER NULL DEFAULT 254,

    PRIMARY KEY (`idTriwulan`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `logRenja` (
    `idlogrenja` INTEGER NOT NULL AUTO_INCREMENT,
    `userId` INTEGER NOT NULL,
    `data` INTEGER NOT NULL,
    `dateCreate` DATETIME(3) NOT NULL,
    `aktiviti` VARCHAR(191) NULL,

    PRIMARY KEY (`idlogrenja`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `uraianRenja` (
    `idUraian` INTEGER NOT NULL AUTO_INCREMENT,
    `tahun` INTEGER NOT NULL,
    `opd` INTEGER NOT NULL,
    `idSubKegiatan` INTEGER NOT NULL,
    `namaUraian` VARCHAR(191) NOT NULL,
    `satuan` VARCHAR(191) NOT NULL,
    `targetVolume` INTEGER NULL DEFAULT 254,
    `totalTarger` VARCHAR(191) NULL,

    PRIMARY KEY (`idUraian`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `uraianTriwulan` (
    `idUraianTriwulan` INTEGER NOT NULL AUTO_INCREMENT,
    `idSubKegiatan` INTEGER NOT NULL,
    `idUraian` INTEGER NOT NULL,
    `triwulan` INTEGER NOT NULL,
    `targetVolume` INTEGER NULL DEFAULT 254,
    `targetPersen` VARCHAR(191) NULL,
    `capaianVolume` VARCHAR(191) NULL,
    `capaianPersen` VARCHAR(191) NULL,

    PRIMARY KEY (`idUraianTriwulan`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `uraianTotalTriwulan` (
    `idTotalUraian` INTEGER NOT NULL AUTO_INCREMENT,
    `idSubKegiatan` INTEGER NOT NULL,
    `triwulanSatu` VARCHAR(191) NULL,
    `triwulanDua` VARCHAR(191) NULL,
    `triwulanTiga` VARCHAR(191) NULL,
    `triwulanEmpat` VARCHAR(191) NULL,
    `totalPersenRp` VARCHAR(191) NULL,
    `totalKinerja` VARCHAR(191) NULL,

    PRIMARY KEY (`idTotalUraian`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `paguTriwulan` (
    `idPagu` INTEGER NOT NULL AUTO_INCREMENT,
    `tahun` INTEGER NOT NULL,
    `opd` INTEGER NOT NULL,
    `idSubKegiatan` INTEGER NOT NULL,
    `triwulan` INTEGER NOT NULL DEFAULT 254,
    `paguIndikatif` INTEGER NOT NULL DEFAULT 254,
    `persenPaguTriwulan` VARCHAR(191) NULL,

    PRIMARY KEY (`idPagu`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `totalPersenPagu` (
    `idTotalPagu` INTEGER NOT NULL AUTO_INCREMENT,
    `idSubKegiatan` INTEGER NOT NULL,
    `triwulanSatu` VARCHAR(191) NULL,
    `triwulanDua` VARCHAR(191) NULL,
    `triwulanTiga` VARCHAR(191) NULL,
    `triwulanEmpat` VARCHAR(191) NULL,
    `TotalPersen` VARCHAR(191) NULL,

    PRIMARY KEY (`idTotalPagu`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `capaianOutcome` (
    `idOutcome` INTEGER NOT NULL AUTO_INCREMENT,
    `idIndikator` INTEGER NOT NULL,
    `tahun` INTEGER NOT NULL,
    `opd` INTEGER NOT NULL,
    `type` VARCHAR(191) NULL,
    `capaianRealisasi` VARCHAR(191) NULL,
    `persenCapaianRealisasi` VARCHAR(191) NULL,

    PRIMARY KEY (`idOutcome`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `faktorRenja` (
    `idFaktor` INTEGER NOT NULL AUTO_INCREMENT,
    `idSubKegiatan` INTEGER NOT NULL,
    `tahun` INTEGER NOT NULL,
    `opd` INTEGER NOT NULL,
    `type` VARCHAR(191) NULL,
    `faktorPendorong` VARCHAR(191) NULL,
    `faktorPenghambat` VARCHAR(191) NULL,
    `tindakLanjut` VARCHAR(191) NULL,

    PRIMARY KEY (`idFaktor`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `historiRenja` (
    `idHistory` INTEGER NOT NULL AUTO_INCREMENT,
    `finalisasi` INTEGER NOT NULL,
    `cekPoint` INTEGER NOT NULL,
    `periode` INTEGER NOT NULL,
    `opd` INTEGER NOT NULL,

    PRIMARY KEY (`idHistory`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `revisiRenja` (
    `idRevisi` INTEGER NOT NULL AUTO_INCREMENT,
    `idSubKegiatan` INTEGER NOT NULL,
    `revisiBy` INTEGER NOT NULL,
    `roleId` INTEGER NOT NULL,
    `catatanRevisi` VARCHAR(191) NULL,
    `tahun` INTEGER NOT NULL,
    `opd` INTEGER NOT NULL,

    PRIMARY KEY (`idRevisi`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `riwayatRenja` (
    `idRiwayat` INTEGER NOT NULL AUTO_INCREMENT,
    `idHistory` INTEGER NOT NULL,
    `cekPoint` INTEGER NOT NULL,
    `tahun` INTEGER NOT NULL,
    `opd` INTEGER NOT NULL,

    PRIMARY KEY (`idRiwayat`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `approvalRenja` (
    `idApprov` INTEGER NOT NULL AUTO_INCREMENT,
    `idHistory` INTEGER NOT NULL,
    `userId` INTEGER NOT NULL,
    `cekPoint` INTEGER NOT NULL,
    `tahun` INTEGER NOT NULL,
    `opd` INTEGER NOT NULL,
    `operasi` INTEGER NOT NULL,
    `tanggal` DATETIME(3) NULL,

    PRIMARY KEY (`idApprov`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `kodeInstrumen` (
    `idKode` INTEGER NOT NULL AUTO_INCREMENT,
    `idIstrumen` INTEGER NOT NULL,
    `kode` VARCHAR(191) NULL,
    `type` VARCHAR(191) NOT NULL,

    PRIMARY KEY (`idKode`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `aksesmenu` (
    `idMenu` INTEGER NOT NULL AUTO_INCREMENT,
    `idUser` INTEGER NOT NULL,
    `idKegiatan` VARCHAR(191) NULL,

    PRIMARY KEY (`idMenu`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
