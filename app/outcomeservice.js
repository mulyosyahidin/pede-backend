import { PrismaClient } from "@prisma/client";
import { errorHandling } from '../middlewares/errorHandler.js'
import dates from "date-and-time";

const prisma = new PrismaClient()
process.env.TZ = 'Asia/Jakarta'

async function cekPriode(priode) {
    return await prisma.periode.findFirst({
        where : {
            idPeriode : parseInt(priode)
        }
    })
}

async function cekIndikator(idSub){
    return await prisma.indikatorInstrumen.findFirst({
        where:{
            idIndikator : parseInt(idSub)
        },
      	orderBy: {
            idIndikator: 'asc'
        }
    })
}

const getIndikator = async (id) => {
    const getIndikatorData = await prisma.indikatorInstrumen.findMany({
        where: {
            idInstrumen: parseInt(id),
        },
        orderBy: {
            idIndikator: 'asc'
        }
    })

    const indikatorData = []
    await Promise.all(getIndikatorData.map(async (item) => {
        const getOut = await prisma.capaianOutcome.findFirst({
            where: {
                idIndikator: item.idIndikator
            }
        })
        const indikat = {
            idindikator: item.idIndikator,
            NamaIndikator: item.indikator_kinerja,
            target: item.target,
            satuan: item.satuan,
            realisasiOutcome: getOut ? getOut.capaianRealisasi : 0,
            persenRealisasiOutcome: getOut ? getOut.persenCapaianRealisasi : 0,
        }
        indikatorData.push(indikat)
    }))

    return indikatorData;
}

const getChildInstrumen = async (type, parent) => {
    const getInstrumen = await prisma.instrumen.findMany({
        where: {
            parent: parseInt(parent),
            type: type
        },
        orderBy: {
            idInstrumen: 'asc'
        }
    });
    return getInstrumen;
}

const addOutcome = async (req)=>{
    const bodyAllow = Object.keys(req.body)
    bodyAllow.forEach(element => {
        if(element === 'idIndikator' || element === 'idPeriode'||element === 'realisasiOutcome'){
        }else{
            throw new errorHandling(401,'Parameter salah silahakn cek body request')
        }
    });
    const periodeExpired = await cekPriode(req.body.idPeriode)
    if(periodeExpired){
        if(periodeExpired.status < 1){
            throw new errorHandling(401,"Periode sudah berakhir tidak bisa edit")
        }
    }else{
        throw new errorHandling(404,"Periode tidak di temukan")
    }
    
    const subExist = await cekIndikator(req.body.idIndikator)
    if(!subExist){
        throw new errorHandling(404,"Data Kegiatan tidak di temukan")
    }

    const getInstrumen = await prisma.instrumen.findFirst({
        where : {
            idInstrumen : parseInt(subExist.idInstrumen)
        }
    })

    const totalCapaianPersen = parseInt(req.body.realisasiOutcome) / parseInt(subExist.target) *100

    const capaianExist = await prisma.capaianOutcome.findFirst({
        where : {
            idIndikator : parseInt(req.body.idIndikator),
            tahun : parseInt(periodeExpired.tahunPeriode),
            opd : parseInt(req.user.userId),
            type : getInstrumen.type.toString()
        }
    })

    const result = []
    if(!capaianExist){
        const insertCapaian = await prisma.capaianOutcome.create({
            data : {
                idIndikator : parseInt(req.body.idIndikator),
                tahun : parseInt(periodeExpired.tahunPeriode),
                opd : parseInt(req.user.userId),
                type : getInstrumen.type.toString(),
                capaianRealisasi : req.body.realisasiOutcome.toString(),
                persenCapaianRealisasi : totalCapaianPersen.toString()
            }
        })

        await Promise.all([insertCapaian]).then(async(item)=>{
            const parser = {
                idIndikator : parseInt(req.body.idIndikator),
                namaInstrumen : getInstrumen.tujuan,
                type : getInstrumen.type.toString(),
                totalTarget : getInstrumen.target,
                capaianRealisasi : req.body.realisasiOutcome.toString(),
                persenCapaianRealisasi : totalCapaianPersen.toString()
            }
            result.push(parser)
        })
    }else{
        const insertCapaian = await prisma.capaianOutcome.updateMany({
            data : {
                capaianRealisasi : req.body.realisasiOutcome.toString(),
                persenCapaianRealisasi : totalCapaianPersen.toString()
            },
            where :{
                idIndikator : parseInt(req.body.idIndikator),
                tahun : parseInt(periodeExpired.tahunPeriode),
                opd : parseInt(req.user.userId)
            }
        })
        await Promise.all([insertCapaian]).then(async(item)=>{
            const parser = {
                idIndikator : parseInt(req.body.idIndikator),
                namaInstrumen : getInstrumen.tujuan,
                type : getInstrumen.type.toString(),
                totalTarget : getInstrumen.target,
                capaianRealisasi : req.body.realisasiOutcome.toString(),
                persenCapaianRealisasi : totalCapaianPersen.toString()
            }
            result.push(parser)
        })
    }
    return result
}

const getAllDataTriwulan = async (idSub,triwulan,tahun,opd)=> {
    const dataTarget = await prisma.targetTriwulan.findFirst({
        where : {
            idSubKegiatan : parseInt(idSub),
            tahun : parseInt(tahun),
            opd : parseInt(opd)
        }
    })

    const getDataCapaian = await prisma.uraianTotalTriwulan.findFirst({
        where: {
            idSubKegiatan : parseInt(idSub)
        }
    })

    const getDataRealisasi = await prisma.paguTriwulan.findFirst({
        where : {
            idSubKegiatan : parseInt(idSub),
            tahun : parseInt(tahun),
            opd : parseInt(opd),
            triwulan : parseInt(triwulan)
        }
    })
    const dataTargetTriwulan =[]
    await Promise.all([dataTarget,getDataCapaian,getDataRealisasi]).then((item)=>{
        if(triwulan === 1){
            const dataTarget = {
                triwulan : 1,
                target : item[0] ? item[0].t1 : 0,
                realisasiKinerja : item[1] ? item[1].triwulanSatu : 0,
                rp : item[2] ? item[2].paguIndikatif : 0,
                persenRealisasi : item[2] ? item[2].persenPaguTriwulan : 0
            }
            dataTargetTriwulan.push(dataTarget)
        }else if (triwulan === 2){
            const dataTarget = {
                triwulan : 2,
                target : item[0] ? item[0].t2 : 0,
                realisasiKinerja : item[1] ? item[1].triwulanDua : 0,
                rp : item[2] ? item[2].paguIndikatif : 0,
                persenRealisasi : item[2] ? item[2].persenPaguTriwulan : 0
            }
            dataTargetTriwulan.push(dataTarget)
        }else if (triwulan === 3){
            const dataTarget = {
                triwulan : 3,
                target : item[0] ? item[0].t3 : 0,
                realisasiKinerja : item[1] ? item[1].triwulanTiga : 0,
                rp : item[2] ? item[2].paguIndikatif : 0,
                persenRealisasi : item[2] ? item[2].persenPaguTriwulan : 0
            }
            dataTargetTriwulan.push(dataTarget)
        }else if (triwulan === 4){
            const dataTarget = {
                triwulan : 4,
                target : item[0] ? item[0].t4 : 0,
                realisasiKinerja : item[1] ? item[1].triwulanEmpat : 0,
                rp : item[2] ? item[2].paguIndikatif : 0,
                persenRealisasi : item[2] ? item[2].persenPaguTriwulan : 0
            }
            dataTargetTriwulan.push(dataTarget)
        }
    })

    return dataTargetTriwulan[0]    
}

const fetchDataOutcome = async (req) => {
    const bodyAllow = Object.keys(req.params)
    bodyAllow.forEach(element => {
        if (element === 'tahun' || element === 'opd') {
        } else {
            throw new errorHandling(401, 'Parameter salah silahakn cek body request')
        }
    })

    const getDataTujuan = await prisma.instrumen.findMany({
        where: {
            periode: parseInt(req.params.tahun),
            opd: parseInt(req.params.opd),
            type: "tujuan"
        },
        orderBy: {
            idInstrumen: 'asc'
        }
    })

    const result = [];
    await Promise.all(getDataTujuan.map(async (tujuan) => {
        const getDataSasaran = await getChildInstrumen('sasaran', tujuan.idInstrumen);

        const dataSasaran = [];
        await Promise.all(getDataSasaran.map(async (sasaran) => {
            const getDataProgram = await getChildInstrumen('program', sasaran.idInstrumen);

            const dataProgram = [];
            await Promise.all(getDataProgram.map(async (program) => {
                const getDataKegiatan = await getChildInstrumen('kegiatan', program.idInstrumen);

                const dataKegiatan = [];
                await Promise.all(getDataKegiatan.map(async (kegiatan) => {
                    const getDataSub = await getChildInstrumen('subKegiatan', kegiatan.idInstrumen);

                    const dataSubKegiatan = [];
                    await Promise.all(getDataSub.map(async (sub) => {
                        const getTotalTarget = await prisma.uraianTotalTriwulan.findFirst({
                            where: {
                                idSubKegiatan: parseInt(sub.idInstrumen)
                            }
                        })

                        const getTotalRealisasi = await prisma.totalPersenPagu.findFirst({
                            where: {
                                idSubKegiatan: parseInt(sub.idInstrumen)
                            }
                        })
                        const dataTriwulanSatu = await getAllDataTriwulan(sub.idInstrumen, 1, req.params.tahun, req.params.opd)
                        const dataTriwulanDua = await getAllDataTriwulan(sub.idInstrumen, 2, req.params.tahun, req.params.opd)
                        const dataTriwulanTiga = await getAllDataTriwulan(sub.idInstrumen, 3, req.params.tahun, req.params.opd)
                        const dataTriwulanEmpat = await getAllDataTriwulan(sub.idInstrumen, 4, req.params.tahun, req.params.opd)
                        const dataTriwulan = []
                        await Promise.all([dataTriwulanSatu, dataTriwulanDua, dataTriwulanTiga, dataTriwulanEmpat]).then((i) => {
                            dataTriwulan.push(i)
                        })
                        const targetDanCapaian = {
                            TotalKinerja: getTotalTarget ? getTotalTarget.totalKinerja : 0,
                            TotalRealisasi: getTotalRealisasi ? getTotalRealisasi.TotalPersen : 0,
                            dataTriwulan
                        }

                        //const cekFinal = await prisma.finalisasirenja.findMany({
                            //where: {
                              //  idInstrumen: parseInt(sub.idInstrumen)
                            //}
                        //})
                        
                        const indikatorData = await getIndikator(sub.idInstrumen);
                        const resultSub = {
                            idSubKegiatan: sub.idInstrumen,
                            namaSubKegiatan: sub.tujuan,
                            idUrusan: sub.urusan,
                           // statusFinalisasi: cekFinal.length > 0 ? true : false,
                            idSubUrusan: sub.subUrusan,
                            targetPaguIndikatif: sub.paguIndikatif,
                            indikatorData,
                            targetDanCapaian
                        }
                        dataSubKegiatan.push(resultSub);
                    }))
                    const indikatorData = await getIndikator(kegiatan.idInstrumen);
                    const resultKegiatan = {
                        idKegiatan: kegiatan.idInstrumen,
                        namaKegiatan: kegiatan.tujuan,
                        idUrusan: kegiatan.urusan,
                        idSubUrusan: kegiatan.subUrusan,
                        targetPaguIndikatif: kegiatan.paguIndikatif,
                        indikatorData,
                        dataSubKegiatan
                    }
                    dataKegiatan.push(resultKegiatan);
                }))
                const indikatorData = await getIndikator(program.idInstrumen);
                const resultProgram = {
                    idProgram: program.idInstrumen,
                    namaProgram: program.tujuan,
                    idUrusan: program.urusan,
                    idSubUrusan: program.subUrusan,
                    targetPaguIndikatif: program.paguIndikatif,
                    indikatorData,
                    dataKegiatan
                }
                dataProgram.push(resultProgram);
            }))
            const indikatorData = await getIndikator(sasaran.idInstrumen);
            const resultSasaran = {
                idSasaran: sasaran.idInstrumen,
                namaSasaran: sasaran.tujuan,
                idUrusan: sasaran.urusan,
                idSubUrusan: sasaran.subUrusan,
                targetPaguIndikatif: sasaran.paguIndikatif,
                indikatorData,
                dataProgram
            }
            dataSasaran.push(resultSasaran);
        }))
        const indikatorData = await getIndikator(tujuan.idInstrumen);
        const dataTujuan = {
            idTujuan: tujuan.idInstrumen,
            namaTujuan: tujuan.tujuan,
            idUrusan: tujuan.urusan,
            idSubUrusan: tujuan.subUrusan,
            targetPaguIndikatif: tujuan.paguIndikatif,
            indikatorData,
            dataSasaran
        }
        result.push(dataTujuan)
    }))
    if (result.length < 1) {
        throw new errorHandling(404, "Data capaian belum ada")
    }

    return result
}

export {
    addOutcome,
    fetchDataOutcome
}