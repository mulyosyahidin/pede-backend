import { PrismaClient } from "@prisma/client";
import { errorHandling } from '../middlewares/errorHandler.js'
import dates from "date-and-time";
import { body } from "express-validator";

const prisma = new PrismaClient()

async function cekPriode(priode) {
    return await prisma.periode.findFirst({
        where : {
            idPeriode : parseInt(priode)
        }
    })
}

async function cekIdPeriode(id) {
    return await prisma.periode.findFirst({
        where : {
            idPeriode : parseInt(id)
        }
    })


}


async function cekIndikator(id) {
    return await prisma.indikatorRpjmd.findFirst({
        where : {
            idIndikator: parseInt(id)
        }
    })
}

const addRealisasi = async (req)=>{
    const bodyAllow = Object.keys(req.body)
    bodyAllow.forEach(element => {
        if(element === 'idIndikator' || element === 'tahun'||element === 'targetRpjmd'||element === 'idPeriode'||element === 'capaianRpjmd'){
        }else{
            throw new errorHandling(401,'Parameter salah silahakn cek body request')
        }

    });

    if(req.body.tahun > 5){
        throw new errorHandling(401,'Data tahun tidak boleh lebih dari 5 tahun')
    }
    const periodeExpired = await cekPriode(req.body.idPeriode)
    if(periodeExpired){
        if(periodeExpired.status < 1){
            throw new errorHandling(401,"Periode sudah berakhir tidak bisa edit")
        }
    }else{
        throw new errorHandling(404,"Periode tidak di temukan")
    }
    

    const indikatExist = await cekIndikator(req.body.idIndikator)
    if(!indikatExist){
        throw new errorHandling(404,"Data indikator tidak ditemukan")
    }

    const cekFinalisasi = await prisma.historiRpjmd.findFirst({
        where: {
            rpjmd : parseInt(indikatExist.idRpjmd),
            finalisasi : 1,
            periode : parseInt(req.body.idPeriode)
        }
    })

    if(cekFinalisasi){
        throw new errorHandling(401,"Data tujuan sudah finalisasi tidak bisa add")
    }

    const tahunExist = await prisma.tahunRpmjd.findFirst({
        where : {
            idPeriode : parseInt(req.body.idPeriode),
            opd : parseInt(req.user.odpId)
        }
    })

    if(!tahunExist){
        throw new errorHandling (404,"Data Tahun berlum di setting pada periode ini")
    }

    const getTarget = await prisma.targetRpmjd.findFirst({
        where : {
            indikator : parseInt(req.body.idIndikator),
            tahunKe : parseInt(req.body.tahun)
        }
    })

    if(getTarget){
        throw new errorHandling(404,"Data capaian untuk tahun ini sudah di isi silahkan update")
    }
    const tingkatCapaian = parseInt(req.body.capaianRpjmd) / parseInt(req.body.targetRpjmd) * 100;
    const addCapaian = await prisma.targetRpmjd.create({
        data: {
            tahunKe : parseInt(req.body.tahun),
            target_rpjmd : parseInt(req.body.targetRpjmd),
            capaian_rpjmd : parseInt(req.body.capaianRpjmd),
            indikator : parseInt(req.body.idIndikator),
            dateCreate : new Date(),
            idCreate : parseInt(req.user.userId),
            tingkatCapaianTarget : parseInt(tingkatCapaian),
        }
    })

    const getTargetAkhir = await prisma.indikatorRpjmd.findFirst({
        where : {
            idIndikator : parseInt(req.body.idIndikator)
        },
        select : {
            target_akhir_periode : true
        }
    })

    const result = []
    await Promise.all([addCapaian]).then(async (item) => {
        const indi = await prisma.indikatorRpjmd.findFirst({
            where : {
                idIndikator : parseInt(req.body.idIndikator)
            },
            select : {
                idIndikator : true,
                indikator_kinerja : true
            }
        })

        const sumCapaian = await prisma.targetRpmjd.aggregate({
            where : {
                indikator : parseInt(req.body.idIndikator)
            },
            _sum: {
                capaian_rpjmd: true
            }
        })

        const rasioCapaian = sumCapaian._sum.capaian_rpjmd/parseInt(getTargetAkhir.target_akhir_periode)*100
        await prisma.indikatorRpjmd.update({
            data : {
                data_capaian_akhir : sumCapaian._sum.capaian_rpjmd,
                rasio_capaian_akhir : rasioCapaian.toString()
            },
            where : {
                idIndikator : parseInt(req.body.idIndikator)
            }
        })
        const parser = {
            idindikator : indi.idIndikator,
            namaIndikator : indi.indikator_kinerja,
            idTargerRpjmd : item[0].idTargetRpjmd,
            tahunKe : item[0].tahunKe,
            targetRpjmd : item[0].target_rpjmd,
            capaianRpjmd : item[0].capaian_rpjmd,
            tingkatCapaianTarget : item[0].tingkatCapaianTarget,
            capaianAkhirTahun : sumCapaian._sum.capaian_rpjmd,
            rasioCapaian : rasioCapaian
        }

        result.push(parser)
    })

    return result
}

const updateCapaian = async (req) => {
    const bodyAllow = Object.keys(req.body)
    bodyAllow.forEach(element => {
        if(element === 'idIndikator' || element === 'tahun'||element === 'targetRpjmd'||element === 'idPeriode'||element === 'capaianRpjmd'){
        }else{
            throw new errorHandling(401,'Parameter salah silahakn cek body request')
        }

    });
    const periodeExpired = await cekPriode(req.body.idPeriode)
    if(periodeExpired){
        if(periodeExpired.status < 1){
            throw new errorHandling(401,"Periode sudah berakhir tidak bisa edit")
        }
    }else{
        throw new errorHandling(404,"Periode tidak di temukan")
    }
    
    const indikatExist = await cekIndikator(req.body.idIndikator)
    if(!indikatExist){
        throw new errorHandling(404,"Data indikator tidak ditemukan")
    }

    const cekFinalisasi = await prisma.historiRpjmd.findFirst({
        where: {
            rpjmd : parseInt(indikatExist.idRpjmd),
            finalisasi : 1,
            periode : parseInt(req.body.idPeriode)
        }
    })

    if(cekFinalisasi){
        throw new errorHandling(401,"Data tujuan sudah finalisasi tidak bisa update")
    }

    const tahunExist = await prisma.tahunRpmjd.findFirst({
        where : {
            idPeriode : parseInt(req.body.idPeriode),
            opd : parseInt(req.user.odpId)
        }
    })

    if(!tahunExist){
        throw new errorHandling (404,"Data Tahun belum di setting pada periode ini")
    }

    const getTarget = await prisma.targetRpmjd.findFirst({
        where : {
            indikator : parseInt(req.body.idIndikator),
            tahunKe : parseInt(req.body.tahun)
        }
    })

    if(!getTarget){
        throw new errorHandling(404,"Data capaian tidak di temukan")
    }
    const tingkatCapaian = parseInt(req.body.capaianRpjmd) / parseInt(req.body.targetRpjmd) * 100;
    const updateCapaian = await prisma.targetRpmjd.update({
        data: {
            tahunKe : parseInt(req.body.tahun),
            target_rpjmd : parseInt(req.body.targetRpjmd),
            capaian_rpjmd : parseInt(req.body.capaianRpjmd),
            tingkatCapaianTarget : parseInt(tingkatCapaian),

        },
        where : {
            idTargetRpjmd : parseInt(getTarget.idTargetRpjmd)
        }
    })

    const getTargetAkhir = await prisma.indikatorRpjmd.findFirst({
        where : {
            idIndikator : parseInt(req.body.idIndikator)
        },
        select : {
            target_akhir_periode : true
        }
    })

    const result = []
    await Promise.all([updateCapaian]).then(async (item) => {
        const indi = await prisma.indikatorRpjmd.findFirst({
            where : {
                idIndikator : parseInt(req.body.idIndikator)
            },
            select : {
                idIndikator : true,
                indikator_kinerja : true
            }
        })
        const sumCapaian = await prisma.targetRpmjd.aggregate({
            where : {
                indikator : parseInt(req.body.idIndikator)
            },
            _sum: {
                capaian_rpjmd: true
            }
        })

        const rasioCapaian = sumCapaian._sum.capaian_rpjmd/parseInt(getTargetAkhir.target_akhir_periode)*100
        await prisma.indikatorRpjmd.update({
            data : {
                data_capaian_akhir : sumCapaian._sum.capaian_rpjmd,
                rasio_capaian_akhir : rasioCapaian.toString()
            },
            where : {
                idIndikator : parseInt(req.body.idIndikator)
            }
        })
        const parser = {
            idindikator : indi.idIndikator,
            namaIndikator : indi.indikator_kinerja,
            idTargerRpjmd : item[0].idTargetRpjmd,
            tahunKe : item[0].tahunKe,
            targetRpjmd : item[0].target_rpjmd,
            capaianRpjmd : item[0].capaian_rpjmd,
            tingkatCapaianTarget : item[0].tingkatCapaianTarget,
            capaianAkhirTahun : sumCapaian._sum.capaian_rpjmd,
            rasioCapaian : rasioCapaian
        }

        result.push(parser)
    })

    return result

}

const listCapaian = async (req) => {
    const bodyAllow = Object.keys(req.params)
    bodyAllow.forEach(element => {
        if(element === 'tahun' ||element === 'idPeriode'){
        }else{
            throw new errorHandling(401,'Parameter salah silahakn cek body request')
        }

    });
    const periodeExpired = await cekPriode(req.params.idPeriode)
    if(!periodeExpired){
        throw new errorHandling(404,"Periode tidak di temukan")
    }

    const fetchTujuan = await prisma.eRPJMD.findMany({
        where : {
            type : 'tujuan',
            periode : parseInt(req.params.idPeriode)
        }
    })
    const result = []
    await Promise.all(fetchTujuan.map(async (item)=>{
        const indikator = await prisma.indikatorRpjmd.findMany({
            where : {
                idRpjmd : parseInt(item.idRpjmd)
            }
        })

        const indikators = []

        await Promise.all(indikator.map(async (isn)=>{
            const capaianTarget = await prisma.targetRpmjd.findFirst({
                where : {
                    indikator : parseInt(isn.idIndikator),
                    tahunKe : parseInt(req.params.tahun)
                },
                select : {
                    idTargetRpjmd : true,
                    tahunKe : true,
                    target_rpjmd : true,
                    capaian_rpjmd : true,
                    tingkatCapaianTarget : true
                }
            })

            const tahuns = capaianTarget ? capaianTarget.tahunKe : null;
            const targetRpjmds = capaianTarget ? capaianTarget.target_rpjmd : null;
            const capaianTargets = capaianTarget ? capaianTarget.capaian_rpjmd : null;
            const tingkatCapaianTargets = capaianTarget ? capaianTarget.tingkatCapaianTarget : null;


            const capaianParse = {
                idIndikator : isn.idIndikator,
                indikator_kinerja : isn.indikator_kinerja,
                data_capaian_awal : isn.data_capaian_awal,
                target_akhir_periode : isn.target_akhir_periode,
                satuan : isn.satuan,
                tahunKe : tahuns,
                targetRpjmd : targetRpjmds,
                capaianTarget : capaianTargets,
                tingkatCapaianTarget : tingkatCapaianTargets,
                data_capaian_akhir : isn.data_capaian_akhir,
                rasio_capaian_akhir : isn.rasio_capaian_akhir
            }

            indikators.push(capaianParse)
        }))
        const sasaran = await prisma.eRPJMD.findMany({
            where : {
                parent : parseInt(item.idRpjmd),
                type : 'sasaran'
            }
        })

        const program = await prisma.eRPJMD.findMany({
            where : {
                parent : parseInt(item.idRpjmd),
                type : 'program'
            }
        })
        const sasaranRpjmd = []
        await Promise.all(sasaran.map(async (ite) =>{
            const indikatorSasarans = await prisma.indikatorRpjmd.findMany({
                where : {
                    idRpjmd : parseInt(ite.idRpjmd)
                },
                select : {
                    idIndikator : true,
                    indikator_kinerja : true,
                    data_capaian_awal : true,
                    target_akhir_periode : true,
                    satuan : true,
                    data_capaian_akhir : true,
                    rasio_capaian_akhir : true
                }
            })

            const indikatorSasaran =[]

            await Promise.all(indikatorSasarans.map(async (capai) => {
                const capaianTarget = await prisma.targetRpmjd.findFirst({
                    where : {
                        indikator : parseInt(capai.idIndikator),
                        tahunKe : parseInt(req.params.tahun)
                    },
                    select : {
                        idTargetRpjmd : true,
                        tahunKe : true,
                        target_rpjmd : true,
                        capaian_rpjmd : true,
                        tingkatCapaianTarget : true
                    }
                })

                const tahuns = capaianTarget ? capaianTarget.tahunKe : null;
                const targetRpjmds = capaianTarget ? capaianTarget.target_rpjmd : null;
                const capaianTargets = capaianTarget ? capaianTarget.capaian_rpjmd : null;
                const tingkatCapaianTargets = capaianTarget ? capaianTarget.tingkatCapaianTarget : null;


                const capaianParse = {
                    idIndikator : capai.idIndikator,
                    indikator_kinerja : capai.indikator_kinerja,
                    data_capaian_awal : capai.data_capaian_awal,
                    target_akhir_periode : capai.target_akhir_periode,
                    satuan : capai.satuan,
                    tahunKe : tahuns,
                    targetRpjmd : targetRpjmds,
                    capaianTarget : capaianTargets,
                    tingkatCapaianTarget : tingkatCapaianTargets,
                    data_capaian_akhir : capai.data_capaian_akhir,
                    rasio_capaian_akhir : capai.rasio_capaian_akhir
                }

                indikatorSasaran.push(capaianParse)
            }))
            const sasaranR = {
                idRpjmd : ite.idRpjmd,
                parent : ite.parent,
                sasaran : ite.tujuan,
                indikatorSasaran
            }
            return sasaranR
        })).then((itek) => {
            sasaranRpjmd.push(itek)
        })

        const programRpjmd = []
        await Promise.all(program.map(async (ites) =>{
            const indikatorPrograms = await prisma.indikatorRpjmd.findMany({
                where : {
                    idRpjmd : parseInt(ites.idRpjmd)
                },
                select : {
                    idIndikator : true,
                    indikator_kinerja : true,
                    data_capaian_awal : true,
                    target_akhir_periode : true,
                    satuan : true,
                    data_capaian_akhir : true,
                    rasio_capaian_akhir : true
                }
            })

            const indikatorProgram =[]

            await Promise.all(indikatorPrograms.map(async (capai) => {
                const capaianTarget = await prisma.targetRpmjd.findFirst({
                    where : {
                        indikator : parseInt(capai.idIndikator),
                        tahunKe : parseInt(req.params.tahun)
                    },
                    select : {
                        idTargetRpjmd : true,
                        tahunKe : true,
                        target_rpjmd : true,
                        capaian_rpjmd : true,
                        tingkatCapaianTarget : true
                    }
                })

                const tahuns = capaianTarget ? capaianTarget.tahunKe : null;
                const targetRpjmds = capaianTarget ? capaianTarget.target_rpjmd : null;
                const capaianTargets = capaianTarget ? capaianTarget.capaian_rpjmd : null;
                const tingkatCapaianTargets = capaianTarget ? capaianTarget.tingkatCapaianTarget : null;


                const capaianParse = {
                    idIndikator : capai.idIndikator,
                    indikator_kinerja : capai.indikator_kinerja,
                    data_capaian_awal : capai.data_capaian_awal,
                    target_akhir_periode : capai.target_akhir_periode,
                    satuan : capai.satuan,
                    tahunKe : tahuns,
                    targetRpjmd : targetRpjmds,
                    capaianTarget : capaianTargets,
                    tingkatCapaianTarget : tingkatCapaianTargets,
                    data_capaian_akhir : capai.data_capaian_akhir,
                    rasio_capaian_akhir : capai.rasio_capaian_akhir
                }

                indikatorProgram.push(capaianParse)
            }))
            const programR = {
                idRpjmd : ites.idRpjmd,
                parent : ites.parent,
                program : ites.tujuan,
                indikatorProgram
            }
            return programR
        })).then((iteks) => {
            programRpjmd.push(iteks)
        })

        const parser = {
            idRpjmd : item.idRpjmd,
            periode : item.periode,
            tujuan : item.tujuan,
            indikators,
            sasaranRpjmd,
            programRpjmd
        }
        result.push(parser)
    }))
    return result

    
}

const addFaktor = async (req) => {
    const bodyAllow = Object.keys(req.body)
    bodyAllow.forEach(element => {
        if(element === 'idIndikator' || element === 'tahun'||element === 'faktorPenghambat'||element === 'idPeriode'||element === 'faktorPendorong'||element === 'rekomTindakLanjut'){
        }else{
            throw new errorHandling(401,'Parameter salah silahakn cek body request')
        }

    });

    if(req.body.tahun > 5){
        throw new errorHandling(401,'Data tahun tidak boleh lebih dari 5 tahun')
    }
    const periodeExpired = await cekPriode(req.body.idPeriode)
    if(periodeExpired){
        if(periodeExpired.status < 1){
            throw new errorHandling(401,"Periode sudah berakhir tidak bisa edit")
        }
    }else{
        throw new errorHandling(404,"Periode tidak di temukan")
    }
    

    const indikatExist = await cekIndikator(req.body.idIndikator)
    if(!indikatExist){
        throw new errorHandling(404,"Data indikator tidak ditemukan")
    }

    const cekFinalisasi = await prisma.historiRpjmd.findFirst({
        where: {
            rpjmd : parseInt(indikatExist.idRpjmd),
            finalisasi : 1,
            periode : parseInt(req.body.idPeriode)
        }
    })

    if(cekFinalisasi){
        throw new errorHandling(401,"Data tujuan sudah finalisasi tidak bisa ditambah")
    }

    const tahunExist = await prisma.tahunRpmjd.findFirst({
        where : {
            idPeriode : parseInt(req.body.idPeriode),
            opd : parseInt(req.user.odpId)
        }
    })

    if(!tahunExist){
        throw new errorHandling (404,"Data Tahun berlum di setting pada periode ini")
    }

    const getFaktor = await prisma.faktorRpjmd.findFirst({
        where : {
            indikator : parseInt(req.body.idIndikator),
            tahunKe : parseInt(req.body.tahun)
        }
    })

    if(getFaktor){
        throw new errorHandling(404,"Data faktor untuk tahun ini sudah di isi silahkan update")
    }

    const createFaktor = await prisma.faktorRpjmd.create({
        data : {
            tahunKe : parseInt(req.body.tahun),
            faktor_penghambat : req.body.faktorPenghambat.toString(),
            faktor_pendorong : req.body.faktorPendorong.toString(),
            rekomendasi_tl : req.body.rekomTindakLanjut.toString(),
            dateCreate : new Date(),
            idCreate : parseInt(req.user.userId),
            indikator : parseInt(req.body.idIndikator)
        }
    })

    const result = {
        "idindikator": req.body.idIndikator,
        "namaIndikator": indikatExist.indikator_kinerja,
        "idFaktorRpjmd": createFaktor.idFaktorRpjmd,
        "tahunKe": req.body.tahun,
        "faktorPenghambat": createFaktor.faktor_penghambat,
        "faktorPendorong": createFaktor.faktor_pendorong,
        "rekomendasiTindakLanjur": createFaktor.rekomendasi_tl
    }

    return result
}

const updateFaktor = async (req)=>{
    const bodyAllow = Object.keys(req.body)
    bodyAllow.forEach(element => {
        if(element === 'idIndikator' || element === 'tahun'||element === 'faktorPenghambat'||element === 'idPeriode'||element === 'faktorPendorong'||element === 'rekomTindakLanjut'){
        }else{
            throw new errorHandling(401,'Parameter salah silahakn cek body request')
        }

    });

    if(req.body.tahun > 5){
        throw new errorHandling(401,'Data tahun tidak boleh lebih dari 5 tahun')
    }
    const periodeExpired = await cekPriode(req.body.idPeriode)
    if(periodeExpired){
        if(periodeExpired.status < 1){
            throw new errorHandling(401,"Periode sudah berakhir tidak bisa edit")
        }
    }else{
        throw new errorHandling(404,"Periode tidak di temukan")
    }

    const indikatExist = await cekIndikator(req.body.idIndikator)
    if(!indikatExist){
        throw new errorHandling(404,"Data indikator tidak ditemukan")
    }

    const cekFinalisasi = await prisma.historiRpjmd.findFirst({
        where: {
            rpjmd : parseInt(indikatExist.idRpjmd),
            finalisasi : 1,
            periode : parseInt(req.body.idPeriode)
        }
    })

    if(cekFinalisasi){
        throw new errorHandling(401,"Data tujuan sudah finalisasi tidak bisa update")
    }

    const tahunExist = await prisma.tahunRpmjd.findFirst({
        where : {
            idPeriode : parseInt(req.body.idPeriode),
            opd : parseInt(req.user.odpId)
        }
    })

    if(!tahunExist){
        throw new errorHandling (404,"Data Tahun berlum di setting pada periode ini")
    }

    const getFaktor = await prisma.faktorRpjmd.findFirst({
        where : {
            indikator : parseInt(req.body.idIndikator),
            tahunKe : parseInt(req.body.tahun)
        }
    })

    if(!getFaktor){
        throw new errorHandling(404,"Data faktor tidak di temukan silahkan add lebih dahulu")
    }

    const createFaktor = await prisma.faktorRpjmd.update({
        data : {
            faktor_penghambat : req.body.faktorPenghambat.toString(),
            faktor_pendorong : req.body.faktorPendorong.toString(),
            rekomendasi_tl : req.body.rekomTindakLanjut.toString(),
        },
        where : {
            idFaktorRpjmd : parseInt(getFaktor.idFaktorRpjmd)
        }
    })

    const result = {
        "idindikator": req.body.idIndikator,
        "namaIndikator": indikatExist.indikator_kinerja,
        "idFaktorRpjmd": createFaktor.idFaktorRpjmd,
        "tahunKe": req.body.tahun,
        "faktorPenghambat": createFaktor.faktor_penghambat,
        "faktorPendorong": createFaktor.faktor_pendorong,
        "rekomendasiTindakLanjur": createFaktor.rekomendasi_tl
    }

    return result
}

const listFaktor = async (req)=>{
    const bodyAllow = Object.keys(req.params)
    bodyAllow.forEach(element => {
        if(element === 'tahun' ||element === 'idPeriode'){
        }else{
            throw new errorHandling(401,'Parameter salah silahakn cek body request')
        }

    });
    const periodeExpired = await cekPriode(req.params.idPeriode)
    if(!periodeExpired){
        throw new errorHandling(404,"Periode tidak di temukan")
    }

    const fetchTujuan = await prisma.eRPJMD.findMany({
        where : {
            type : 'tujuan',
            periode : parseInt(req.params.idPeriode)
        }
    })
    const result = []
    await Promise.all(fetchTujuan.map(async (item)=>{
        const indikator = await prisma.indikatorRpjmd.findMany({
            where : {
                idRpjmd : parseInt(item.idRpjmd)
            }
        })

        const indikators = []

        await Promise.all(indikator.map(async (isn)=>{
            const capaianTarget = await prisma.faktorRpjmd.findFirst({
                where : {
                    indikator : parseInt(isn.idIndikator),
                    tahunKe : parseInt(req.params.tahun)
                },
                select : {
                    idFaktorRpjmd : true,
                    tahunKe : true,
                    faktor_penghambat : true,
                    faktor_pendorong : true,
                    rekomendasi_tl : true
                }
            })

            const faktorPenghambats = capaianTarget ? capaianTarget.faktor_penghambat : null;
            const faktorPendorongs = capaianTarget ? capaianTarget.faktor_pendorong : null;
            const rekomendasitlTargets = capaianTarget ? capaianTarget.rekomendasi_tl : null;


            const capaianParse = {
                idIndikator : isn.idIndikator,
                indikator_kinerja : isn.indikator_kinerja,
                tahunKe : req.params.tahun,
                faktorPenghambat : faktorPenghambats,
                faktorpendorong : faktorPendorongs,
                rekomendasiTindakLanjut : rekomendasitlTargets
            }

            indikators.push(capaianParse)
        }))
        const sasaran = await prisma.eRPJMD.findMany({
            where : {
                parent : parseInt(item.idRpjmd),
                type : 'sasaran'
            }
        })

        const program = await prisma.eRPJMD.findMany({
            where : {
                parent : parseInt(item.idRpjmd),
                type : 'program'
            }
        })
        const sasaranRpjmd = []
        await Promise.all(sasaran.map(async (ite) =>{
            const indikatorSasarans = await prisma.indikatorRpjmd.findMany({
                where : {
                    idRpjmd : parseInt(ite.idRpjmd)
                },
                select : {
                    idIndikator : true,
                    indikator_kinerja : true,
                    data_capaian_awal : true,
                    target_akhir_periode : true,
                    satuan : true,
                    data_capaian_akhir : true,
                    rasio_capaian_akhir : true
                }
            })

            const indikatorSasaran =[]

            await Promise.all(indikatorSasarans.map(async (capai) => {
                const capaianTarget = await prisma.faktorRpjmd.findFirst({
                    where : {
                        indikator : parseInt(capai.idIndikator),
                        tahunKe : parseInt(req.params.tahun)
                    },
                    select : {
                        idFaktorRpjmd : true,
                        tahunKe : true,
                        faktor_penghambat : true,
                        faktor_pendorong : true,
                        rekomendasi_tl : true
                    }
                })
    
                const faktorPenghambats = capaianTarget ? capaianTarget.faktor_penghambat : null;
                const faktorPendorongs = capaianTarget ? capaianTarget.faktor_pendorong : null;
                const rekomendasitlTargets = capaianTarget ? capaianTarget.rekomendasi_tl : null;
    
    
                const capaianParse = {
                    idIndikator : capai.idIndikator,
                    indikator_kinerja : capai.indikator_kinerja,
                    tahunKe : req.params.tahun,
                    faktorPenghambat : faktorPenghambats,
                    faktorpendorong : faktorPendorongs,
                    rekomendasiTindakLanjut : rekomendasitlTargets
                }
    
                indikatorSasaran.push(capaianParse)
            }))
            const sasaranR = {
                idRpjmd : ite.idRpjmd,
                parent : ite.parent,
                sasaran : ite.tujuan,
                indikatorSasaran
            }
            return sasaranR
        })).then((itek) => {
            sasaranRpjmd.push(itek)
        })

        const programRpjmd = []
        await Promise.all(program.map(async (ites) =>{
            const indikatorPrograms = await prisma.indikatorRpjmd.findMany({
                where : {
                    idRpjmd : parseInt(ites.idRpjmd)
                },
                select : {
                    idIndikator : true,
                    indikator_kinerja : true,
                    data_capaian_awal : true,
                    target_akhir_periode : true,
                    satuan : true,
                    data_capaian_akhir : true,
                    rasio_capaian_akhir : true
                }
            })

            const indikatorProgram =[]

            await Promise.all(indikatorPrograms.map(async (capai) => {
                const capaianTarget = await prisma.faktorRpjmd.findFirst({
                    where : {
                        indikator : parseInt(capai.idIndikator),
                        tahunKe : parseInt(req.params.tahun)
                    },
                    select : {
                        idFaktorRpjmd : true,
                        tahunKe : true,
                        faktor_penghambat : true,
                        faktor_pendorong : true,
                        rekomendasi_tl : true
                    }
                })
    
                const faktorPenghambats = capaianTarget ? capaianTarget.faktor_penghambat : null;
                const faktorPendorongs = capaianTarget ? capaianTarget.faktor_pendorong : null;
                const rekomendasitlTargets = capaianTarget ? capaianTarget.rekomendasi_tl : null;
    
    
                const capaianParse = {
                    idIndikator : capai.idIndikator,
                    indikator_kinerja : capai.indikator_kinerja,
                    tahunKe : req.params.tahun,
                    faktorPenghambat : faktorPenghambats,
                    faktorpendorong : faktorPendorongs,
                    rekomendasiTindakLanjut : rekomendasitlTargets
                }
    
                indikatorProgram.push(capaianParse)
            }))
            const programR = {
                idRpjmd : ites.idRpjmd,
                parent : ites.parent,
                program : ites.tujuan,
                indikatorProgram
            }
            return programR
        })).then((iteks) => {
            programRpjmd.push(iteks)
        })

        const statusFinalisasi = await prisma.historiRpjmd.findFirst({
            where : {
                rpjmd : item.idRpjmd,
                periode : parseInt(req.params.idPeriode),
                odp : parseInt(req.user.odpId)
            }
        })

        const parser = {
            idRpjmd : item.idRpjmd,
            periode : item.periode,
            finalisasi : statusFinalisasi ? statusFinalisasi.finalisasi : 0,
            tujuan : item.tujuan,
            indikators,
            sasaranRpjmd,
            programRpjmd
        }
        result.push(parser)
    }))
    return result
}

const getReport = async (req) => {
    const bodyAllow = Object.keys(req.params)
    bodyAllow.forEach(element => {
        if(element === 'idPeriode'){
        }else{
            throw new errorHandling(401,'Parameter salah silahakn cek body request')
        }

    });
    const periodeExpired = await cekPriode(req.params.idPeriode)
    if(!periodeExpired){
        throw new errorHandling(404,"Periode tidak di temukan")
    }

    const fetchTujuan = await prisma.eRPJMD.findMany({
        where : {
            type : 'tujuan',
            periode : parseInt(req.params.idPeriode)
        }
    })
    const result = []
    await Promise.all(fetchTujuan.map(async (item)=>{
        const indikator = await prisma.indikatorRpjmd.findMany({
            where : {
                idRpjmd : parseInt(item.idRpjmd)
            }
        })

        const indikators = []

        await Promise.all(indikator.map(async (isn)=>{
            const faktorPendorongPenghambat = await prisma.faktorRpjmd.findMany({
                where : {
                    indikator : parseInt(isn.idIndikator),
                },
                select : {
                    idFaktorRpjmd : false,
                    tahunKe : true,
                    faktor_penghambat : true,
                    faktor_pendorong : true,
                    rekomendasi_tl : true
                }
            })

            const capaianTarget = await prisma.targetRpmjd.findMany({
                where : {
                    indikator : parseInt(isn.idIndikator)
                },
                select : {
                    idTargetRpjmd : false,
                    tahunKe : true,
                    target_rpjmd : true,
                    capaian_rpjmd : true,
                    tingkatCapaianTarget : true
                }
            })


            const capaianParse = {
                idIndikator : isn.idIndikator,
                indikator_kinerja : isn.indikator_kinerja,
                dataCapaianAwal : isn.data_capaian_akhir,
                targetAkhirPeriode : isn.target_akhir_periode,
                satuan : isn.satuan,
                dataCapaianAkhir : isn.data_capaian_akhir,
                rasioCapaianAkhir : isn.rasio_capaian_akhir,
                capaianTarget,
                faktorPendorongPenghambat
            }
            indikators.push(capaianParse)
        }))
        const sasaran = await prisma.eRPJMD.findMany({
            where : {
                parent : parseInt(item.idRpjmd),
                type : 'sasaran'
            }
        })

        const program = await prisma.eRPJMD.findMany({
            where : {
                parent : parseInt(item.idRpjmd),
                type : 'program'
            }
        })
        const sasaranRpjmd = []
        await Promise.all(sasaran.map(async (ite) =>{
            const indikatorSasarans = await prisma.indikatorRpjmd.findMany({
                where : {
                    idRpjmd : parseInt(ite.idRpjmd)
                },
                select : {
                    idIndikator : true,
                    indikator_kinerja : true,
                    data_capaian_awal : true,
                    target_akhir_periode : true,
                    satuan : true,
                    data_capaian_akhir : true,
                    rasio_capaian_akhir : true
                }
            })

            const indikatorSasaran =[]

            await Promise.all(indikatorSasarans.map(async (capai) => {
                const faktorPendorongPenghambat = await prisma.faktorRpjmd.findMany({
                    where : {
                        indikator : parseInt(capai.idIndikator),
                    },
                    select : {
                        idFaktorRpjmd : false,
                        tahunKe : true,
                        faktor_penghambat : true,
                        faktor_pendorong : true,
                        rekomendasi_tl : true
                    }
                })
    
                const capaianTarget = await prisma.targetRpmjd.findMany({
                    where : {
                        indikator : parseInt(capai.idIndikator)
                    },
                    select : {
                        idTargetRpjmd : false,
                        tahunKe : true,
                        target_rpjmd : true,
                        capaian_rpjmd : true,
                        tingkatCapaianTarget : true
                    }
                })
    
    
                const capaianParse = {
                    idIndikator : capai.idIndikator,
                    indikator_kinerja : capai.indikator_kinerja,
                    dataCapaianAwal : capai.data_capaian_akhir,
                    targetAkhirPeriode : capai.target_akhir_periode,
                    satuan : capai.satuan,
                    dataCapaianAkhir : capai.data_capaian_akhir,
                    rasioCapaianAkhir : capai.rasio_capaian_akhir,
                    capaianTarget,
                    faktorPendorongPenghambat
                }
    
                indikatorSasaran.push(capaianParse)
            }))
            const sasaranR = {
                idRpjmd : ite.idRpjmd,
                parent : ite.parent,
                sasaran : ite.tujuan,
                indikatorSasaran
            }
            return sasaranR
        })).then((itek) => {
            sasaranRpjmd.push(itek)
        })

        const programRpjmd = []
        await Promise.all(program.map(async (ites) =>{
            const indikatorPrograms = await prisma.indikatorRpjmd.findMany({
                where : {
                    idRpjmd : parseInt(ites.idRpjmd)
                },
                select : {
                    idIndikator : true,
                    indikator_kinerja : true,
                    data_capaian_awal : true,
                    target_akhir_periode : true,
                    satuan : true,
                    data_capaian_akhir : true,
                    rasio_capaian_akhir : true
                }
            })

            const indikatorProgram =[]

            await Promise.all(indikatorPrograms.map(async (capai) => {
                const faktorPendorongPenghambat = await prisma.faktorRpjmd.findMany({
                    where : {
                        indikator : parseInt(capai.idIndikator),
                    },
                    select : {
                        idFaktorRpjmd : false,
                        tahunKe : true,
                        faktor_penghambat : true,
                        faktor_pendorong : true,
                        rekomendasi_tl : true
                    }
                })
    
                const capaianTarget = await prisma.targetRpmjd.findMany({
                    where : {
                        indikator : parseInt(capai.idIndikator)
                    },
                    select : {
                        idTargetRpjmd : false,
                        tahunKe : true,
                        target_rpjmd : true,
                        capaian_rpjmd : true,
                        tingkatCapaianTarget : true
                    }
                })
    
    
                const capaianParse = {
                    idIndikator : capai.idIndikator,
                    indikator_kinerja : capai.indikator_kinerja,
                    dataCapaianAwal : capai.data_capaian_akhir,
                    targetAkhirPeriode : capai.target_akhir_periode,
                    satuan : capai.satuan,
                    dataCapaianAkhir : capai.data_capaian_akhir,
                    rasioCapaianAkhir : capai.rasio_capaian_akhir,
                    capaianTarget,
                    faktorPendorongPenghambat
                }
    
                indikatorProgram.push(capaianParse)
            }))
            const programR = {
                idRpjmd : ites.idRpjmd,
                parent : ites.parent,
                program : ites.tujuan,
                indikatorProgram
            }
            return programR
        })).then((iteks) => {
            programRpjmd.push(iteks)
        })
        const statusFinalisasi = await prisma.historiRpjmd.findFirst({
            where : {
                rpjmd : item.idRpjmd,
                periode : parseInt(req.params.idPeriode),
                odp : parseInt(req.user.odpId)
            }
        })
        const parser = {
            idRpjmd : item.idRpjmd,
            periode : item.periode,
            tujuan : item.tujuan,
            idUrusan : item.urusan,
            subUrusan : item.subUrusan,
            finalisasi : statusFinalisasi ? statusFinalisasi.finalisasi : 0,
            opd: item.opd,
            indikators,
            sasaranRpjmd,
            programRpjmd
        }
        result.push(parser)
    }))
    return result
}

const finalisasiRpjmd = async (req)=>{
    const bodyAllow = Object.keys(req.body)
    bodyAllow.forEach(element => {
        if(element === 'idPeriode' || element === 'idRpjmd'){
        }else{
            throw new errorHandling(401,'Parameter salah silahakn cek body request')
        }

    });
    const periodeExpired = await cekPriode(req.body.idPeriode)
    if(periodeExpired){
        if(periodeExpired.status < 1){
            throw new errorHandling(401,"Periode sudah berakhir tidak bisa edit")
        }
    }else{
        throw new errorHandling(404,"Periode tidak di temukan")
    }

    const cekHistory = await prisma.historiRpjmd.findMany({
        where : {
            rpjmd : {
                in : req.body.idRpjmd
            },
            periode : parseInt(req.body.idPeriode),
            odp : parseInt(req.user.odpId)
        }
    })
    if(cekHistory.length > 0){
        throw new errorHandling(401,"Data Rpjmd sudah di finalisas, periksa id rpjmd")
    }

    const datas = req.body.idRpjmd.map((item) => {
        const parser = {
            rpjmd : item,
            periode : parseInt(req.body.idPeriode),
            odp : parseInt(req.user.odpId),
            cekPoint : 3,
            finalisasi : 1
        }
        return parser
    })

    

    const insertFinal = await prisma.historiRpjmd.createMany({
        data : datas
    })
    const result = []
    await Promise.all([insertFinal]).then(async ()=>{
        const cekdata = await prisma.historiRpjmd.findMany({
            where : {
                rpjmd : {
                    in : req.body.idRpjmd
                },
                periode : parseInt(req.body.idPeriode),
                odp : parseInt(req.user.odpId)
            }
        })

        const dataApprov = cekdata.map((item)=>{
            const dataApprovs = {
                histori : parseInt(item.idHisRpmjd),
                idUser : parseInt(req.user.userId),
                roleId : parseInt(req.user.roleId),
                dateCreate : new Date()
            }

            return dataApprovs
        })

        await prisma.approvalRpmjd.createMany({
            data : dataApprov
        })

        result.push(cekdata)
    })

    return result
}

const listApproval =  async (req)=>{
    const bodyAllow = Object.keys(req.params)
    bodyAllow.forEach(element => {
        if(element === 'idPeriode'){
        }else{
            throw new errorHandling(401,'Parameter salah silahakn cek body request')
        }

    });
    const periodeExpired = await cekPriode(req.params.idPeriode)
    if(!periodeExpired){
        throw new errorHandling(404,"Periode tidak di temukan")
    }

    const idRpjmd = await prisma.historiRpjmd.findMany({
        where : {
            cekPoint : parseInt(req.user.roleId),
            finalisasi : 1,
            periode : parseInt(req.params.idPeriode)
        }
    })

    const dataId = idRpjmd.map((item)=>{
        return item.rpjmd
    })

    const fetchTujuan = await prisma.eRPJMD.findMany({
        where : {
            type : 'tujuan',
            periode : parseInt(req.params.idPeriode),
            idRpjmd : {
                in : dataId
            }
        }
    })
    const result = []
    await Promise.all(fetchTujuan.map(async (item)=>{
        const indikator = await prisma.indikatorRpjmd.findMany({
            where : {
                idRpjmd : parseInt(item.idRpjmd)
            }
        })

        const indikators = []

        await Promise.all(indikator.map(async (isn)=>{
            const faktorPendorongPenghambat = await prisma.faktorRpjmd.findMany({
                where : {
                    indikator : parseInt(isn.idIndikator),
                },
                select : {
                    idFaktorRpjmd : false,
                    tahunKe : true,
                    faktor_penghambat : true,
                    faktor_pendorong : true,
                    rekomendasi_tl : true
                }
            })

            const capaianTarget = await prisma.targetRpmjd.findMany({
                where : {
                    indikator : parseInt(isn.idIndikator)
                },
                select : {
                    idTargetRpjmd : false,
                    tahunKe : true,
                    target_rpjmd : true,
                    capaian_rpjmd : true,
                    tingkatCapaianTarget : true
                }
            })


            const capaianParse = {
                idIndikator : isn.idIndikator,
                indikator_kinerja : isn.indikator_kinerja,
                dataCapaianAwal : isn.data_capaian_akhir,
                targetAkhirPeriode : isn.target_akhir_periode,
                satuan : isn.satuan,
                dataCapaianAkhir : isn.data_capaian_akhir,
                rasioCapaianAkhir : isn.rasio_capaian_akhir,
                capaianTarget,
                faktorPendorongPenghambat
            }
            indikators.push(capaianParse)
        }))
        const sasaran = await prisma.eRPJMD.findMany({
            where : {
                parent : parseInt(item.idRpjmd),
                type : 'sasaran'
            }
        })

        const program = await prisma.eRPJMD.findMany({
            where : {
                parent : parseInt(item.idRpjmd),
                type : 'program'
            }
        })
        const sasaranRpjmd = []
        await Promise.all(sasaran.map(async (ite) =>{
            const indikatorSasarans = await prisma.indikatorRpjmd.findMany({
                where : {
                    idRpjmd : parseInt(ite.idRpjmd)
                },
                select : {
                    idIndikator : true,
                    indikator_kinerja : true,
                    data_capaian_awal : true,
                    target_akhir_periode : true,
                    satuan : true,
                    data_capaian_akhir : true,
                    rasio_capaian_akhir : true
                }
            })

            const indikatorSasaran =[]

            await Promise.all(indikatorSasarans.map(async (capai) => {
                const faktorPendorongPenghambat = await prisma.faktorRpjmd.findMany({
                    where : {
                        indikator : parseInt(capai.idIndikator),
                    },
                    select : {
                        idFaktorRpjmd : false,
                        tahunKe : true,
                        faktor_penghambat : true,
                        faktor_pendorong : true,
                        rekomendasi_tl : true
                    }
                })
    
                const capaianTarget = await prisma.targetRpmjd.findMany({
                    where : {
                        indikator : parseInt(capai.idIndikator)
                    },
                    select : {
                        idTargetRpjmd : false,
                        tahunKe : true,
                        target_rpjmd : true,
                        capaian_rpjmd : true,
                        tingkatCapaianTarget : true
                    }
                })
    
    
                const capaianParse = {
                    idIndikator : capai.idIndikator,
                    indikator_kinerja : capai.indikator_kinerja,
                    dataCapaianAwal : capai.data_capaian_akhir,
                    targetAkhirPeriode : capai.target_akhir_periode,
                    satuan : capai.satuan,
                    dataCapaianAkhir : capai.data_capaian_akhir,
                    rasioCapaianAkhir : capai.rasio_capaian_akhir,
                    capaianTarget,
                    faktorPendorongPenghambat
                }
    
                indikatorSasaran.push(capaianParse)
            }))
            const sasaranR = {
                idRpjmd : ite.idRpjmd,
                parent : ite.parent,
                sasaran : ite.tujuan,
                indikatorSasaran
            }
            return sasaranR
        })).then((itek) => {
            sasaranRpjmd.push(itek)
        })

        const programRpjmd = []
        await Promise.all(program.map(async (ites) =>{
            const indikatorPrograms = await prisma.indikatorRpjmd.findMany({
                where : {
                    idRpjmd : parseInt(ites.idRpjmd)
                },
                select : {
                    idIndikator : true,
                    indikator_kinerja : true,
                    data_capaian_awal : true,
                    target_akhir_periode : true,
                    satuan : true,
                    data_capaian_akhir : true,
                    rasio_capaian_akhir : true
                }
            })

            const indikatorProgram =[]

            await Promise.all(indikatorPrograms.map(async (capai) => {
                const faktorPendorongPenghambat = await prisma.faktorRpjmd.findMany({
                    where : {
                        indikator : parseInt(capai.idIndikator),
                    },
                    select : {
                        idFaktorRpjmd : false,
                        tahunKe : true,
                        faktor_penghambat : true,
                        faktor_pendorong : true,
                        rekomendasi_tl : true
                    }
                })
    
                const capaianTarget = await prisma.targetRpmjd.findMany({
                    where : {
                        indikator : parseInt(capai.idIndikator)
                    },
                    select : {
                        idTargetRpjmd : false,
                        tahunKe : true,
                        target_rpjmd : true,
                        capaian_rpjmd : true,
                        tingkatCapaianTarget : true
                    }
                })
    
    
                const capaianParse = {
                    idIndikator : capai.idIndikator,
                    indikator_kinerja : capai.indikator_kinerja,
                    dataCapaianAwal : capai.data_capaian_akhir,
                    targetAkhirPeriode : capai.target_akhir_periode,
                    satuan : capai.satuan,
                    dataCapaianAkhir : capai.data_capaian_akhir,
                    rasioCapaianAkhir : capai.rasio_capaian_akhir,
                    capaianTarget,
                    faktorPendorongPenghambat
                }
    
                indikatorProgram.push(capaianParse)
            }))
            const programR = {
                idRpjmd : ites.idRpjmd,
                parent : ites.parent,
                program : ites.tujuan,
                indikatorProgram
            }
            return programR
        })).then((iteks) => {
            programRpjmd.push(iteks)
        })
        const statusFinalisasi = await prisma.historiRpjmd.findFirst({
            where : {
                rpjmd : item.idRpjmd,
                periode : parseInt(req.params.idPeriode),
                odp : parseInt(req.user.odpId)
            }
        })
        const parser = {
            idRpjmd : item.idRpjmd,
            periode : item.periode,
            tujuan : item.tujuan,
            idUrusan : item.urusan,
            subUrusan : item.subUrusan,
            finalisasi : statusFinalisasi ? statusFinalisasi.finalisasi : 0,
            opd: item.opd,
            indikators,
            sasaranRpjmd,
            programRpjmd
        }
        result.push(parser)
    }))
    return result
}

const approval = async (req)=>{
    const bodyAllow = Object.keys(req.body)
    bodyAllow.forEach(element => {
        if(element === 'status' || element === 'idRpjmd'||element === 'idPeriode'||element === 'catatan'){
        }else{
            throw new errorHandling(401,'Parameter salah silahakn cek body request')
        }

    });
    const statusPeriode = await cekIdPeriode(req.body.idPeriode)
    if(statusPeriode){
        if(statusPeriode.status < '1'){
            throw new errorHandling(401,'Periode ini sudah berakhir')
        }
    }else{
        throw new errorHandling(401,'Periode tidak ditemukan')
    }

    const getApprov = await prisma.historiRpjmd.findMany({
        where :{
            rpjmd : {
                in: req.body.idRpjmd
            },
            periode : parseInt(req.body.idPeriode),
            cekPoint : parseInt(req.user.roleId),
            finalisasi : 1
        }
    })

    const dataId = getApprov.map((item)=>{
        return item.idHisRpmjd
    })

    if(req.body.status === 1){
        const cekPoint = parseInt(req.user.roleId)+1
        const updates = await prisma.historiRpjmd.updateMany({
            data:{
                cekPoint: parseInt(cekPoint),
            },
            where:{
                idHisRpmjd : {
                    in: dataId
                }
            }
        })

    
        await Promise.all([updates]).then(async ()=>{
            const dataParser = getApprov.map((items)=>{
                const parser = {
                    histori: items.idHisRpmjd,
                    idUser : parseInt(req.user.userId),
                    roleId : parseInt(req.user.roleId),
                    dateCreate : new Date()
                }
                return parser
            })

            await prisma.approvalRpmjd.createMany({
                data :dataParser
            })
        })

        return
        
    }else{
        const cekPoint = parseInt(req.user.roleId)-1
        const updates = await prisma.historiRpjmd.updateMany({
            data:{
                cekPoint: parseInt(cekPoint),
                finalisasi : 2
            },
            where:{
                idHisRpmjd : {
                    in: dataId
                }
            }
        })
        await Promise.all([updates,getApprov]).then(async (item)=>{
            const dataParser = getApprov.map((items)=>{
                const parser = {
                    histori: items.idHisRpmjd,
                    idUser : parseInt(req.user.userId),
                    roleId : parseInt(req.user.roleId),
                    dateCreate : new Date()
                }
                return parser
            })

            const dataParsers = getApprov.map((items)=>{
                const parser = {
                    histori: items.idHisRpmjd,
                    idUser : parseInt(req.user.userId),
                    catatan : req.body.catatan.toString()
                }
                return parser
            })

            await prisma.approvalRpmjd.createMany({
                data :dataParser
            })

            await prisma.catatanRpmjd.createMany({
                data : dataParsers
            })
        })
        return 
    }
}

const getRevisi = async (req) => {
    const bodyAllow = Object.keys(req.params)
    bodyAllow.forEach(element => {
        if(element === 'idPeriode'){
        }else{
            throw new errorHandling(401,'Parameter salah silahakn cek body request')
        }

    });
    const periodeExpired = await cekPriode(req.params.idPeriode)
    if(!periodeExpired){
        throw new errorHandling(404,"Periode tidak di temukan")
    }

    const idRpjmd = await prisma.historiRpjmd.findMany({
        where : {
            cekPoint : parseInt(req.user.roleId),
            finalisasi : 2,
            periode : parseInt(req.params.idPeriode)
        }
    })

    const dataId = idRpjmd.map((item)=>{
        return item.rpjmd
    })

    const fetchTujuan = await prisma.eRPJMD.findMany({
        where : {
            type : 'tujuan',
            periode : parseInt(req.params.idPeriode),
            idRpjmd : {
                in : dataId
            }
        }
    })
    const result = []
    await Promise.all(fetchTujuan.map(async (item)=>{
        const indikator = await prisma.indikatorRpjmd.findMany({
            where : {
                idRpjmd : parseInt(item.idRpjmd)
            }
        })

        const indikators = []

        await Promise.all(indikator.map(async (isn)=>{
            const faktorPendorongPenghambat = await prisma.faktorRpjmd.findMany({
                where : {
                    indikator : parseInt(isn.idIndikator),
                },
                select : {
                    idFaktorRpjmd : false,
                    tahunKe : true,
                    faktor_penghambat : true,
                    faktor_pendorong : true,
                    rekomendasi_tl : true
                }
            })

            const capaianTarget = await prisma.targetRpmjd.findMany({
                where : {
                    indikator : parseInt(isn.idIndikator)
                },
                select : {
                    idTargetRpjmd : false,
                    tahunKe : true,
                    target_rpjmd : true,
                    capaian_rpjmd : true,
                    tingkatCapaianTarget : true
                }
            })


            const capaianParse = {
                idIndikator : isn.idIndikator,
                indikator_kinerja : isn.indikator_kinerja,
                dataCapaianAwal : isn.data_capaian_akhir,
                targetAkhirPeriode : isn.target_akhir_periode,
                satuan : isn.satuan,
                dataCapaianAkhir : isn.data_capaian_akhir,
                rasioCapaianAkhir : isn.rasio_capaian_akhir,
                capaianTarget,
                faktorPendorongPenghambat
            }
            indikators.push(capaianParse)
        }))
        const sasaran = await prisma.eRPJMD.findMany({
            where : {
                parent : parseInt(item.idRpjmd),
                type : 'sasaran'
            }
        })

        const program = await prisma.eRPJMD.findMany({
            where : {
                parent : parseInt(item.idRpjmd),
                type : 'program'
            }
        })
        const sasaranRpjmd = []
        await Promise.all(sasaran.map(async (ite) =>{
            const indikatorSasarans = await prisma.indikatorRpjmd.findMany({
                where : {
                    idRpjmd : parseInt(ite.idRpjmd)
                },
                select : {
                    idIndikator : true,
                    indikator_kinerja : true,
                    data_capaian_awal : true,
                    target_akhir_periode : true,
                    satuan : true,
                    data_capaian_akhir : true,
                    rasio_capaian_akhir : true
                }
            })

            const indikatorSasaran =[]

            await Promise.all(indikatorSasarans.map(async (capai) => {
                const faktorPendorongPenghambat = await prisma.faktorRpjmd.findMany({
                    where : {
                        indikator : parseInt(capai.idIndikator),
                    },
                    select : {
                        idFaktorRpjmd : false,
                        tahunKe : true,
                        faktor_penghambat : true,
                        faktor_pendorong : true,
                        rekomendasi_tl : true
                    }
                })
    
                const capaianTarget = await prisma.targetRpmjd.findMany({
                    where : {
                        indikator : parseInt(capai.idIndikator)
                    },
                    select : {
                        idTargetRpjmd : false,
                        tahunKe : true,
                        target_rpjmd : true,
                        capaian_rpjmd : true,
                        tingkatCapaianTarget : true
                    }
                })
    
    
                const capaianParse = {
                    idIndikator : capai.idIndikator,
                    indikator_kinerja : capai.indikator_kinerja,
                    dataCapaianAwal : capai.data_capaian_akhir,
                    targetAkhirPeriode : capai.target_akhir_periode,
                    satuan : capai.satuan,
                    dataCapaianAkhir : capai.data_capaian_akhir,
                    rasioCapaianAkhir : capai.rasio_capaian_akhir,
                    capaianTarget,
                    faktorPendorongPenghambat
                }
    
                indikatorSasaran.push(capaianParse)
            }))
            const sasaranR = {
                idRpjmd : ite.idRpjmd,
                parent : ite.parent,
                sasaran : ite.tujuan,
                indikatorSasaran
            }
            return sasaranR
        })).then((itek) => {
            sasaranRpjmd.push(itek)
        })

        const programRpjmd = []
        await Promise.all(program.map(async (ites) =>{
            const indikatorPrograms = await prisma.indikatorRpjmd.findMany({
                where : {
                    idRpjmd : parseInt(ites.idRpjmd)
                },
                select : {
                    idIndikator : true,
                    indikator_kinerja : true,
                    data_capaian_awal : true,
                    target_akhir_periode : true,
                    satuan : true,
                    data_capaian_akhir : true,
                    rasio_capaian_akhir : true
                }
            })

            const indikatorProgram =[]

            await Promise.all(indikatorPrograms.map(async (capai) => {
                const faktorPendorongPenghambat = await prisma.faktorRpjmd.findMany({
                    where : {
                        indikator : parseInt(capai.idIndikator),
                    },
                    select : {
                        idFaktorRpjmd : false,
                        tahunKe : true,
                        faktor_penghambat : true,
                        faktor_pendorong : true,
                        rekomendasi_tl : true
                    }
                })
    
                const capaianTarget = await prisma.targetRpmjd.findMany({
                    where : {
                        indikator : parseInt(capai.idIndikator)
                    },
                    select : {
                        idTargetRpjmd : false,
                        tahunKe : true,
                        target_rpjmd : true,
                        capaian_rpjmd : true,
                        tingkatCapaianTarget : true
                    }
                })
    
    
                const capaianParse = {
                    idIndikator : capai.idIndikator,
                    indikator_kinerja : capai.indikator_kinerja,
                    dataCapaianAwal : capai.data_capaian_akhir,
                    targetAkhirPeriode : capai.target_akhir_periode,
                    satuan : capai.satuan,
                    dataCapaianAkhir : capai.data_capaian_akhir,
                    rasioCapaianAkhir : capai.rasio_capaian_akhir,
                    capaianTarget,
                    faktorPendorongPenghambat
                }
    
                indikatorProgram.push(capaianParse)
            }))
            const programR = {
                idRpjmd : ites.idRpjmd,
                parent : ites.parent,
                program : ites.tujuan,
                indikatorProgram
            }
            return programR
        })).then((iteks) => {
            programRpjmd.push(iteks)
        })
        const statusFinalisasi = await prisma.historiRpjmd.findFirst({
            where : {
                rpjmd : item.idRpjmd,
                periode : parseInt(req.params.idPeriode),
                odp : parseInt(req.user.odpId)
            }
        })
        const parser = {
            idRpjmd : item.idRpjmd,
            periode : item.periode,
            tujuan : item.tujuan,
            idUrusan : item.urusan,
            subUrusan : item.subUrusan,
            finalisasi : statusFinalisasi ? statusFinalisasi.finalisasi :0,
            opd: item.opd,
            indikators,
            sasaranRpjmd,
            programRpjmd
        }
        result.push(parser)
    }))
    return result
}

const getCatatan = async (req) => {
    const bodyAllow = Object.keys(req.body)
    bodyAllow.forEach(element => {
        if(element === 'idRpjmd' || element === 'idPeriode'){
        }else{
            throw new errorHandling(401,'Parameter salah silahakn cek body request')
        }

    });

    const getHistori = await prisma.historiRpjmd.findFirst({
        where : {
            rpjmd : parseInt(req.body.idRpjmd),
            periode : parseInt(req.body.idPeriode),
            odp : parseInt(req.user.odpId),
            finalisasi : 2
        }
    })

    if(!getHistori){
        throw new errorHandling(404,'Belum ada catatan apapun untuk id rpjmd berikut')
    }

    const catatan = await prisma.catatanRpmjd.findMany({
        where : {
            histori : getHistori.idHisRpmjd
        }
    })
    return await Promise.all(catatan.map(async (catat) =>{
        const namaUser = await prisma.namaUser.findFirst({
            where : {
                userId : catat.idUser.toString()
            },
            select : {
                namaUser : true
            }
        })
        
        const dataParser = {
            idRpjmd : getHistori.rpjmd,
            namaUsers : namaUser.namaUser,
            catatan : catat.catatan
        }

        return dataParser
    }))
}

export {
    addRealisasi,
    updateCapaian,
    listCapaian,
    addFaktor,
    updateFaktor,
    listFaktor,
    getReport,
    finalisasiRpjmd,
    listApproval,
    approval,
    getRevisi,
    getCatatan
}