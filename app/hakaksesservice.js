import { PrismaClient } from "@prisma/client";
import { errorHandling } from '../middlewares/errorHandler.js'
import { utimes } from "fs";


const prisma = new PrismaClient()

async function cekUser(idUser) {
    const cekUser = await prisma.users.findFirst({
        where :{
            userId : parseInt(idUser)
        }
    })

    const result = cekUser ? cekUser : null
    return result
}

const addHakAkses = async (req)=>{
    const bodyAllow = Object.keys(req.body)
    bodyAllow.forEach(element => {
        if(element === 'idUser' || element === 'menu' ){
        }else{
            throw new errorHandling(401,'Parameter salah silahakn cek body request')
        }
    });

    const cekUsers = await cekUser(req.body.idUser)
    if(!cekUsers){
        throw new errorHandling(404,'Data User tidak di temukan')
    }

    const cekHakAkses = await prisma.aksesmenu.findFirst({
        where : {
            idUser : parseInt(req.body.idUser)
        }
    }) 
    if(cekHakAkses){
        await prisma.aksesmenu.updateMany({
            data : {
                idKegiatan : req.body.menu ? JSON.stringify(req.body.menu) : null
            },
            where :{
                idUser : parseInt(req.body.idUser)
            }
        })
    }else{
        await prisma.aksesmenu.create({
            data :{
                idUser : parseInt(req.body.idUser),
                idKegiatan : req.body.menu ? JSON.stringify(req.body.menu) : null
            }
        })
    }
    return
}


const getAksesMenu = async (id)=>{
    const cekUsers = await cekUser(id)
    if(!cekUsers){
        throw new errorHandling(404,'Data User tidak di temukan')
    }

    const fetchMenu = await prisma.aksesmenu.findMany({
        where : {
            idUser : parseInt(id)
        }
    })

    const result = []
    await Promise.all(fetchMenu.map(async (item)=>{
        const idKegiatan = JSON.parse(item.idKegiatan)
        const getSubKegiatan = [] 
        await Promise.all(idKegiatan.map(async(item)=>{
            const idSubParse = await prisma.instrumen.findFirst({
                where : {
                    idInstrumen : parseInt(item),
                    type : 'subKegiatan'
                }
            })
            const parser = {
                idSubKegiatan : item,
                namaSubKegiatan : idSubParse ? idSubParse.tujuan : '' 
            }
            getSubKegiatan.push(parser)
        }))
        return getSubKegiatan
    })).then((item)=>{
        result.push(item[0])
    })
    return result[0]
}

export{
    addHakAkses,
    getAksesMenu
}