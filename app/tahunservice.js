import { PrismaClient } from "@prisma/client";
import { errorHandling } from '../middlewares/errorHandler.js'

const prisma = new PrismaClient()

async function cekTahun(idTahun){
    return await prisma.tahunRpmjd.findFirst({
        where : {
            idTh : parseInt(idTahun)
        }
    })
}

async function tahunStatus(){
    return await prisma.tahunRpmjd.findFirst({
        where : {
            status : 1 
        }
    })
}

const addTahun = async (req) => {
    const statusTahun = await tahunStatus()
    if(statusTahun){
        throw new errorHandling(401,"Ada data tahun yang masi aktif, gagal menambahkan")
    }
    req.body.status = 1
    return await prisma.tahunRpmjd.create({
        data : req.body
    })
}

const updateTahun = async (req) => {
    const cekTahuns = await cekTahun(req.body.idTh)
    if(!cekTahuns) {
        throw new errorHandling(404,"Data tahun tidak di temukan")
    }
    return await prisma.tahunRpmjd.update({
        data : req.body.data,
        where : {
            idTh : parseInt(req.body.idTh)
        },
        select : {
            idTh : true,
            tahunSatu : true,
            tahunDua : true,
            tahunTiga : true,
            tahunEmpat : true,
            tahunLima : true
        }
    })
}

const listTahun = async (req) => {
    return await prisma.tahunRpmjd.findFirst({
        where : {
            status : 1,
        },
        select : {
            idTh : true,
            tahunSatu : true,
            tahunDua : true,
            tahunTiga : true,
            tahunEmpat : true,
            tahunLima : true
        }
    })
}

export {
    addTahun,
    updateTahun,
    listTahun
}