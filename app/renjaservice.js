import {PrismaClient} from "@prisma/client";
import {errorHandling} from '../middlewares/errorHandler.js'
import dates from "date-and-time";
import prismaClient from "../src/config/database.js";

const prisma = new PrismaClient()
process.env.TZ = 'Asia/Jakarta'

async function cekPriode(priode) {
    return await prisma.periode.findFirst({
        where: {
            idPeriode: parseInt(priode)
        }
    })
}

async function cekUraian(id) {
    return await prisma.uraianRenja.findFirst({
        where: {
            idUraian: parseInt(id)
        }
    })
}

const getIndikator = async (id) => {
    const getIndikatorData = await prisma.indikatorInstrumen.findMany({
        where: {
            idInstrumen: parseInt(id),
        },
        orderBy: {
            idIndikator: 'asc'
        }
    })

    const indikatorData = []
    await Promise.all([getIndikatorData]).then((indikator) => {
        const indi = indikator[0].map((ite) => {
            const indis = {
                idindikator: ite.idIndikator,
                NamaIndikator: ite.indikator_kinerja,
                target: ite.target,
                satuan: ite.satuan
            }
            return indis
        })
        indikatorData.push(indi[0])
    })

    return indikatorData;
}

const getChildInstrumen = async (type, parent) => {
    const getInstrumen = await prisma.instrumen.findMany({
        where: {
            parent: parseInt(parent),
            type: type
        },
        orderBy: {
            idInstrumen: 'asc'
        }
    });
    return getInstrumen;
}

const getCapaianPersen = async (idSub, triwulan) => {
    const getNilaiSUb = await prisma.uraianTriwulan.findMany({
        where: {
            idSubKegiatan: parseInt(idSub),
            triwulan: parseInt(triwulan)
        }
    })
    const totalCapaians = getNilaiSUb.reduce((total, item) => {
        if (item.capaianPersen !== null && item.capaianPersen !== undefined) {
            total += parseInt(item.capaianPersen, 10);
        }
        return total;
    }, 0);

    // Menghitung jumlah elemen yang tidak null
    const countNonNull = getNilaiSUb.reduce((count, item) => {
        if (item.capaianPersen !== null && item.capaianPersen !== undefined) {
            count += 1;
        }
        return count;
    }, 0);

    // Menghitung rata-rata
    const averageCapaian = countNonNull > 0 ? totalCapaians / countNonNull : 0;

    if (triwulan === 1) {
        const data = {
            t1: averageCapaian
        }
        return data
    } else if (triwulan === 2) {
        const data = {
            t2: averageCapaian
        }
        return data
    } else if (triwulan === 3) {
        const data = {
            t3: averageCapaian
        }
        return data
    } else if (triwulan === 4) {
        const data = {
            t4: averageCapaian
        }
        return data
    }
}


const inputCapaian = async (req) => {
    const bodyAllow = Object.keys(req.body)
    bodyAllow.forEach(element => {
        if (element === 'idUraian' || element === 'idPeriode' || element === 'dataCapaian') {
        } else {
            throw new errorHandling(401, 'Parameter salah silahakn cek body request')
        }
    });
    if (req.body.triwulan > 4) {
        throw new errorHandling(401, 'Data triwulan tidak boleh lebih dari 4')
    }

    const periodeExpired = await cekPriode(req.body.idPeriode)
    if (periodeExpired) {
        if (periodeExpired.status < 1) {
            throw new errorHandling(401, "Periode sudah berakhir tidak bisa edit")
        }
    } else {
        throw new errorHandling(404, "Periode tidak di temukan")
    }
    const indikatExist = await cekUraian(req.body.idUraian)
    if (!indikatExist) {
        throw new errorHandling(404, "Data Uraian tidak ditemukan")
    }

    const capaianInput = []

    await Promise.all(req.body.dataCapaian.map(async (item) => {
        const capaianExist = await prisma.uraianTriwulan.findFirst({
            where: {
                idUraian: parseInt(req.body.idUraian),
                triwulan: parseInt(item.triwulan)
            }
        })

        if (!capaianExist) {
            throw new errorHandling(404, "Data target capaian tidak di temukan, hubungi admin Bappeda")
        }
        const hasilTarget = parseInt(item.capaian) / parseInt(indikatExist.targetVolume) * 100
        const result = await prisma.uraianTriwulan.updateMany({
            data: {
                capaianVolume: item.capaian.toString(),
                capaianPersen: hasilTarget.toString()
            },
            where: {
                idUraian: parseInt(req.body.idUraian),
                triwulan: parseInt(item.triwulan)
            }
        })

        return result
    })).then(async () => {
        const totalUraian = await prisma.uraianTriwulan.findMany({
            where: {
                idUraian: parseInt(req.body.idUraian)
            }
        })
        //total persen per uaraian dan update
        const totalCapaian = totalUraian.reduce((total, item) => {
            // Pemeriksaan untuk memastikan nilai bukan null atau undefined
            if (item.capaianPersen !== null && item.capaianPersen !== undefined) {
                total += parseInt(item.capaianPersen, 10);
            }

            return total;
        }, 0);

        await prisma.uraianRenja.update({
            data: {
                totalTarger: totalCapaian.toString()
            },
            where: {
                idUraian: parseInt(req.body.idUraian)
            }
        })

        // total all subkegiatan dan update
        const totalExist = await prisma.uraianTotalTriwulan.findFirst({
            where: {
                idSubKegiatan: parseInt(indikatExist.idSubKegiatan)
            }
        })

        const averageCapaianTw1 = await getCapaianPersen(indikatExist.idSubKegiatan, 1)
        const averageCapaianTw2 = await getCapaianPersen(indikatExist.idSubKegiatan, 2)
        const averageCapaianTw3 = await getCapaianPersen(indikatExist.idSubKegiatan, 3)
        const averageCapaianTw4 = await getCapaianPersen(indikatExist.idSubKegiatan, 4)

        await Promise.all([averageCapaianTw1, averageCapaianTw2, averageCapaianTw3, averageCapaianTw4]).then(async (capai) => {
            if (totalExist) {
                const TotalAkhir = parseInt(capai[0].t1) + parseInt(capai[1].t2) + parseInt(capai[2].t3) + parseInt(capai[3].t4)
                await prisma.uraianTotalTriwulan.updateMany({
                    data: {
                        triwulanSatu: capai[0].t1 ? capai[0].t1.toString() : '0',
                        triwulanDua: capai[1].t2 ? capai[1].t2.toString() : '0',
                        triwulanTiga: capai[2].t3 ? capai[2].t3.toString() : '0',
                        triwulanEmpat: capai[3].t4 ? capai[3].t4.toString() : '0',
                        totalKinerja: TotalAkhir.toString()
                    },
                    where: {
                        idSubKegiatan: parseInt(indikatExist.idSubKegiatan)
                    }
                })
            } else {
                const TotalAkhir = parseInt(capai[0].t1) + parseInt(capai[1].t2) + parseInt(capai[2].t3) + parseInt(capai[3].t4)
                await prisma.uraianTotalTriwulan.create({
                    data: {
                        idSubKegiatan: parseInt(indikatExist.idSubKegiatan),
                        triwulanSatu: capai[0].t1 ? capai[0].t1.toString() : '0',
                        triwulanDua: capai[1].t2 ? capai[1].t2.toString() : '0',
                        triwulanTiga: capai[2].t3 ? capai[2].t3.toString() : '0',
                        triwulanEmpat: capai[3].t4 ? capai[3].t4.toString() : '0',
                        totalKinerja: TotalAkhir.toString()
                    }
                })
            }
        })
    }).finally(async () => {
        const data = await prisma.uraianRenja.findFirst({
            where: {
                idUraian: parseInt(req.body.idUraian)
            }
        })
        const capaianTargetTriwulan = []
        await Promise.all(req.body.dataCapaian.map(async (par) => {
            const capaianData = await prisma.uraianTriwulan.findFirst({
                where: {
                    idUraian: parseInt(data.idUraian),
                    triwulan: parseInt(par.triwulan)
                }
            })

            const capaianTarget = {
                triwulan: par.triwulan,
                targetPertriwulan: capaianData ? capaianData.targetVolume : null,
                persenPertriwulan: capaianData ? capaianData.targetPersen : null,
                capaianVolumePertriwulan: capaianData ? capaianData.capaianVolume : null,
                capaianPersenPertriwulan: capaianData ? capaianData.capaianPersen : null,
            }
            capaianTargetTriwulan.push(capaianTarget)
        }))
        const parser = {
            idUraian: data.idUraian,
            namaUraian: data.namaUraian,
            targetVolumeTotal: data.targetVolume,
            targetPersenTotal: data.totalTarger,
            capaianTargetTriwulan

        }
        capaianInput.push(parser)
    })
    await prisma.logRenja.create({
        data: {
            userId: parseInt(req.user.userId),
            data: parseInt(req.body.idUraian),
            dateCreate: new Date(),
            aktiviti: "input capaian target"
        }
    })
    return capaianInput
}

const listCapaian = async (req) => {
    const bodyAllow = Object.keys(req.params)
    bodyAllow.forEach(element => {
        if (element === 'idPeriode') {
        } else {
            throw new errorHandling(401, 'Parameter salah silahakn cek body request')
        }
    });

    const periodeExpired = await cekPriode(req.params.idPeriode)
    if (!periodeExpired) {
        throw new errorHandling(404, "Periode tidak di temukan")
    }
    const subKegiatan = await prisma.instrumen.findMany({
        where: {
            periode: parseInt(periodeExpired.tahunPeriode),
            opd: parseInt(req.user.odpId),
            type: "subKegiatan"
        }
    })
    const result = []
    await Promise.all(subKegiatan.map(async (item) => {
        const getUraian = await prisma.uraianRenja.findMany({
            where: {
                idSubKegiatan: parseInt(item.idInstrumen),
                opd: parseInt(req.user.odpId),
                tahun: parseInt(periodeExpired.tahunPeriode)
            }
        })

        const dataUraian = []
        await Promise.all(getUraian.map(async (item) => {
            const capaianGet = await prisma.uraianTriwulan.findMany({
                where: {
                    idUraian: parseInt(item.idUraian),
                    triwulan: {
                        in: [1, 2, 3, 4]
                    }
                }
            })

            const dataTriwulan = []
            await Promise.all(capaianGet.map((capai) => {
                const dataCap = {
                    triwulan: capai.triwulan,
                    targetTriwulanVolume: capai.targetVolume ? capai.targetVolume : null,
                    targetTriwulanPersen: capai.targetPersen ? capai.targetPersen : null,
                    capaianTriwulanVolume: capai.capaianVolume ? capai.capaianVolume : null,
                    capaianTriwulanPersen: capai.capaianPersen ? capai.capaianPersen : null
                }

                dataTriwulan.push(dataCap)
            }))

            const parse = {
                idUraian: item.idUraian,
                namaUraian: item.namaUraian,
                satuan: item.satuan,
                targetTotalVolume: item.targetVolume,
                capaianTotalPersentw1untiltw4: item.totalTarger ? item.totalTarger : null,
                dataTriwulan
            }

            dataUraian.push(parse)
        }))
        const totalTri = await prisma.uraianTotalTriwulan.findFirst({
            where: {
                idSubKegiatan: parseInt(item.idInstrumen)
            }
        })

        //const cekFinal = await prisma.finalisasirenja.findMany({
        // where : {
        //    idInstrumen : parseInt(item.idInstrumen)
        // }
        //})

        let kodeInstrumen = await prismaClient.kodeInstrumen.findFirst({
            where: {
                idIstrumen: item.idInstrumen,
            }
        });

        const parser = {
            idSubKegiatan: item.idInstrumen,
            namaSubKegiatan: item.tujuan,
            kode: kodeInstrumen.kode,
            tahun: item.periode,
            // statusFinalisasi : cekFinal ? true : false,
            opd: item.opd,
            capaianPersenSub: [
                {
                    triwulanSatu: totalTri ? totalTri.triwulanSatu : 0
                },
                {
                    triwulanDua: totalTri ? totalTri.triwulanDua : 0
                },
                {
                    triwulanTiga: totalTri ? totalTri.triwulanTiga : 0
                },
                {
                    triwulanEmplat: totalTri ? totalTri.triwulanEmpat : 0
                },
                {
                    totalAkhir: totalTri ? totalTri.totalKinerja : 0
                }
            ],
            dataUraian
        }
        result.push(parser)
    }))

    return result

}

const cekSub = async (idSub) => {
    return await prisma.instrumen.findFirst({
        where: {
            idInstrumen: parseInt(idSub),
            type: "subKegiatan"
        }
    })
}

const totalPersenPagu = async (idsub, tahun, req) => {
    const getTotal = await prisma.paguTriwulan.findMany({
        where: {
            idSubKegiatan: parseInt(idsub),
            tahun: parseInt(tahun),
            opd: parseInt(req.user.odpId),
            triwulan: {
                in: [1, 2, 3, 4]
            }
        },
        select: {
            persenPaguTriwulan: true
        }
    })

    const totalCapaian = getTotal.reduce((total, item) => {
        // Pemeriksaan untuk memastikan nilai bukan null atau undefined
        if (item.persenPaguTriwulan !== null && item.persenPaguTriwulan !== undefined) {
            total += parseInt(item.persenPaguTriwulan, 10);
        }
        return total;
    }, 0);

    return totalCapaian

}

const totalAveragePagu = async (idSub, triwulan, tahun, req) => {
    const getAllTriwulan = await prisma.paguTriwulan.findMany({
        where: {
            idSubKegiatan: parseInt(idSub),
            tahun: parseInt(tahun),
            opd: parseInt(req.user.odpId),
            triwulan: parseInt(triwulan)
        }
    })
    const totalCapaians = getAllTriwulan.reduce((total, item) => {
        if (item.persenPaguTriwulan !== null && item.persenPaguTriwulan !== undefined) {
            total += parseInt(item.persenPaguTriwulan, 10);
        }
        return total;
    }, 0);

    // Menghitung jumlah elemen yang tidak null
    const countNonNull = getAllTriwulan.reduce((count, item) => {
        if (item.persenPaguTriwulan !== null && item.persenPaguTriwulan !== undefined) {
            count += 1;
        }
        return count;
    }, 0);

    // Menghitung rata-rata
    const averageTarget = countNonNull > 0 ? totalCapaians / countNonNull : 0;
    if (triwulan === 1) {
        const data = {
            t1: averageTarget
        }
        return data
    } else if (triwulan === 2) {
        const data = {
            t2: averageTarget
        }
        return data
    } else if (triwulan === 3) {
        const data = {
            t3: averageTarget
        }
        return data
    } else if (triwulan === 4) {
        const data = {
            t4: averageTarget
        }
        return data
    }
}
const inputRupiah = async (req) => {
    const bodyAllow = Object.keys(req.body)
    bodyAllow.forEach(element => {
        if (element === 'idSubKegiatan' || element === 'idPeriode' || element === 'dataPagu') {
        } else {
            throw new errorHandling(401, 'Parameter salah silahakn cek body request')
        }
    });
    if (req.body.triwulan > 4) {
        throw new errorHandling(401, 'Data triwulan tidak boleh lebih dari 4')
    }

    const periodeExpired = await cekPriode(req.body.idPeriode)
    if (periodeExpired) {
        if (periodeExpired.status < 1) {
            throw new errorHandling(401, "Periode sudah berakhir tidak bisa edit")
        }
    } else {
        throw new errorHandling(404, "Periode tidak di temukan")
    }
    const indikatExist = await cekSub(req.body.idSubKegiatan)
    if (!indikatExist) {
        throw new errorHandling(404, "Data subKegiatan tidak ditemukan")
    }

    const cekPagu = await prisma.paguTriwulan.findFirst({
        where: {
            idSubKegiatan: parseInt(req.body.idSubKegiatan),
            tahun: parseInt(periodeExpired.tahunPeriode),
            opd: parseInt(req.user.odpId)
        }
    })
    await Promise.all(req.body.dataPagu.map(async (item) => {
        if (cekPagu) {
            const persenTarget = parseInt(item.rupiah) / parseInt(indikatExist.paguIndikatif) * 100

            return await prisma.paguTriwulan.updateMany({
                data: {
                    paguIndikatif: parseInt(item.rupiah),
                    persenPaguTriwulan: persenTarget.toString()
                },
                where: {
                    idSubKegiatan: parseInt(req.body.idSubKegiatan),
                    triwulan: parseInt(item.triwulan)
                }
            })
        } else {
            const persenTarget = parseInt(item.rupiah) / parseInt(indikatExist.paguIndikatif) * 100
            return await prisma.paguTriwulan.createMany({
                data: {
                    idSubKegiatan: parseInt(req.body.idSubKegiatan),
                    triwulan: parseInt(item.triwulan),
                    tahun: parseInt(periodeExpired.tahunPeriode),
                    opd: parseInt(req.user.odpId),
                    paguIndikatif: parseInt(item.rupiah),
                    persenPaguTriwulan: persenTarget.toString()
                }
            })
        }

    })).then(async () => {
        const totalPersenPagus = await totalPersenPagu(req.body.idSubKegiatan, periodeExpired.tahunPeriode, req)
        const avgPaguPersenTw1 = await totalAveragePagu(req.body.idSubKegiatan, 1, periodeExpired.tahunPeriode, req)
        const avgPaguPersenTw2 = await totalAveragePagu(req.body.idSubKegiatan, 2, periodeExpired.tahunPeriode, req)
        const avgPaguPersenTw3 = await totalAveragePagu(req.body.idSubKegiatan, 3, periodeExpired.tahunPeriode, req)
        const avgPaguPersenTw4 = await totalAveragePagu(req.body.idSubKegiatan, 4, periodeExpired.tahunPeriode, req)

        await Promise.all([totalPersenPagus, avgPaguPersenTw1, avgPaguPersenTw2, avgPaguPersenTw3, avgPaguPersenTw4]).then(async (avg) => {
            const totalPaguExist = await prisma.totalPersenPagu.findFirst({
                where: {
                    idSubKegiatan: parseInt(req.body.idSubKegiatan),
                }
            })

            const triwulanSatu = avg[1].t1 ? avg[1].t1 : 0
            const triwulanDua = avg[2].t2 ? avg[2].t2 : 0
            const triwulanTiga = avg[3].t3 ? avg[3].t3 : 0
            const triwulanEmpat = avg[4].t4 ? avg[4].t4 : 0
            const TotalPersen = avg[0] ? avg[0] : 0

            if (totalPaguExist) {
                await prisma.totalPersenPagu.updateMany({
                    data: {
                        triwulanSatu: triwulanSatu.toString(),
                        triwulanDua: triwulanDua.toString(),
                        triwulanTiga: triwulanTiga.toString(),
                        triwulanEmpat: triwulanEmpat.toString(),
                        TotalPersen: TotalPersen.toString()
                    },
                    where: {
                        idSubKegiatan: parseInt(req.body.idSubKegiatan)
                    }
                })
            } else {
                await prisma.totalPersenPagu.createMany({
                    data: {
                        idSubKegiatan: parseInt(req.body.idSubKegiatan),
                        triwulanSatu: triwulanSatu.toString(),
                        triwulanDua: triwulanDua.toString(),
                        triwulanTiga: triwulanTiga.toString(),
                        triwulanEmpat: triwulanEmpat.toString(),
                        TotalPersen: TotalPersen.toString()
                    }
                })
            }
        })
    })


    const result = {
        targetPaguIndikatif: indikatExist.paguIndikatif,
        status: 'dataSukses'
    }

    return result

}

const getAllDataTriwulan = async (idSub, triwulan, tahun, opd) => {
    const dataTarget = await prisma.targetTriwulan.findFirst({
        where: {
            idSubKegiatan: parseInt(idSub),
            tahun: parseInt(tahun),
            opd: parseInt(opd)
        }
    })

    const getDataCapaian = await prisma.uraianTotalTriwulan.findFirst({
        where: {
            idSubKegiatan: parseInt(idSub)
        }
    })

    const getDataRealisasi = await prisma.paguTriwulan.findFirst({
        where: {
            idSubKegiatan: parseInt(idSub),
            tahun: parseInt(tahun),
            opd: parseInt(opd),
            triwulan: parseInt(triwulan)
        }
    })
    const dataTargetTriwulan = []
    await Promise.all([dataTarget, getDataCapaian, getDataRealisasi]).then((item) => {
        if (triwulan === 1) {
            const dataTarget = {
                triwulan: 1,
                target: item[0] ? item[0].t1 : 0,
                realisasiKinerja: item[1] ? item[1].triwulanSatu : 0,
                rp: item[2] ? item[2].paguIndikatif : 0,
                persenRealisasi: item[2] ? item[2].persenPaguTriwulan : 0
            }
            dataTargetTriwulan.push(dataTarget)
        } else if (triwulan === 2) {
            const dataTarget = {
                triwulan: 2,
                target: item[0] ? item[0].t2 : 0,
                realisasiKinerja: item[1] ? item[1].triwulanDua : 0,
                rp: item[2] ? item[2].paguIndikatif : 0,
                persenRealisasi: item[2] ? item[2].persenPaguTriwulan : 0
            }
            dataTargetTriwulan.push(dataTarget)
        } else if (triwulan === 3) {
            const dataTarget = {
                triwulan: 3,
                target: item[0] ? item[0].t3 : 0,
                realisasiKinerja: item[1] ? item[1].triwulanTiga : 0,
                rp: item[2] ? item[2].paguIndikatif : 0,
                persenRealisasi: item[2] ? item[2].persenPaguTriwulan : 0
            }
            dataTargetTriwulan.push(dataTarget)
        } else if (triwulan === 4) {
            const dataTarget = {
                triwulan: 4,
                target: item[0] ? item[0].t4 : 0,
                realisasiKinerja: item[1] ? item[1].triwulanEmpat : 0,
                rp: item[2] ? item[2].paguIndikatif : 0,
                persenRealisasi: item[2] ? item[2].persenPaguTriwulan : 0
            }
            dataTargetTriwulan.push(dataTarget)
        }
    })

    return dataTargetTriwulan[0]
}

const listCapaianRupiah = async (req) => {
    const bodyAllow = Object.keys(req.params)
    bodyAllow.forEach(element => {
        if (element === 'idPeriode') {
        } else {
            throw new errorHandling(401, 'Parameter salah silahakn cek body request')
        }
    });

    const periodeExpired = await cekPriode(req.params.idPeriode)
    if (!periodeExpired) {
        throw new errorHandling(404, "Periode tidak di temukan")
    }

    const getDataTujuan = await prisma.instrumen.findMany({
        where: {
            periode: parseInt(periodeExpired.tahunPeriode),
            opd: parseInt(req.user.odpId),
            type: "tujuan"
        },
        orderBy: {
            idInstrumen: 'asc'
        }
    })

    const result = []
    await Promise.all(getDataTujuan.map(async (tujuan) => {
        const getDataSasaran = await getChildInstrumen('sasaran', tujuan.idInstrumen);

        const dataSasaran = [];
        await Promise.all(getDataSasaran.map(async (sasaran) => {
            const getDataProgram = await getChildInstrumen('program', sasaran.idInstrumen);

            const dataProgram = [];
            await Promise.all(getDataProgram.map(async (program) => {
                const getDataKegiatan = await getChildInstrumen('kegiatan', program.idInstrumen);

                const dataKegiatan = [];
                await Promise.all(getDataKegiatan.map(async (kegiatan) => {
                    const getDataSubKegiatan = await getChildInstrumen('subKegiatan', kegiatan.idInstrumen);
                    const dataSubKegiatan = [];
                    await Promise.all(getDataSubKegiatan.map(async (sub) => {

                        const getTotalTarget = await prisma.uraianTotalTriwulan.findFirst({
                            where: {
                                idSubKegiatan: parseInt(sub.idInstrumen)
                            }
                        })

                        const getTotalRealisasi = await prisma.totalPersenPagu.findFirst({
                            where: {
                                idSubKegiatan: parseInt(sub.idInstrumen)
                            }
                        });

                        const dataTriwulanSatu = await getAllDataTriwulan(sub.idInstrumen, 1, periodeExpired.tahunPeriode, req.user.odpId)
                        const dataTriwulanDua = await getAllDataTriwulan(sub.idInstrumen, 2, periodeExpired.tahunPeriode, req.user.odpId)
                        const dataTriwulanTiga = await getAllDataTriwulan(sub.idInstrumen, 3, periodeExpired.tahunPeriode, req.user.odpId)
                        const dataTriwulanEmpat = await getAllDataTriwulan(sub.idInstrumen, 4, periodeExpired.tahunPeriode, req.user.odpId)
                        const dataTriwulan = []
                        await Promise.all([dataTriwulanSatu, dataTriwulanDua, dataTriwulanTiga, dataTriwulanEmpat]).then((i) => {
                            dataTriwulan.push(i)
                        })

                        const targetDanCapaian = {
                            TotalKinerja: getTotalTarget ? getTotalTarget.totalKinerja : 0,
                            TotalRealisasi: getTotalRealisasi ? getTotalRealisasi.TotalPersen : 0,
                            dataTriwulan
                        }


                        const indikatorData = await getIndikator(sub.idInstrumen);
                        const resultSub = {
                            idSubKegiatan: sub.idInstrumen,
                            namaSubKegiatan: sub.tujuan,
                            idUrusan: sub.urusan,
                            idSubUrusan: sub.subUrusan,
                            targetPaguIndikatif: sub.paguIndikatif,
                            indikatorData,
                            targetDanCapaian
                        }
                        dataSubKegiatan.push(resultSub);
                    }))
                    const indikatorData = await getIndikator(kegiatan.idInstrumen);
                    const resultKegiatan = {
                        idKegiatan: kegiatan.idInstrumen,
                        namaKegiatan: kegiatan.tujuan,
                        idUrusan: kegiatan.urusan,
                        idSubUrusan: kegiatan.subUrusan,
                        targetPaguIndikatif: kegiatan.paguIndikatif,
                        indikatorData,
                        dataSubKegiatan
                    }
                    dataKegiatan.push(resultKegiatan);
                }))
                const indikatorData = await getIndikator(program.idInstrumen);
                const resultProgram = {
                    idProgram: program.idInstrumen,
                    namaProgram: program.tujuan,
                    idUrusan: program.urusan,
                    idSubUrusan: program.subUrusan,
                    targetPaguIndikatif: program.paguIndikatif,
                    indikatorData,
                    dataKegiatan
                }
                dataProgram.push(resultProgram);
            }))
            const indikatorData = await getIndikator(sasaran.idInstrumen);
            const resultSasaran = {
                idSasaran: sasaran.idInstrumen,
                namaSasaran: sasaran.tujuan,
                idUrusan: sasaran.urusan,
                idSubUrusan: sasaran.subUrusan,
                targetPaguIndikatif: sasaran.paguIndikatif,
                indikatorData,
                dataProgram
            }
            dataSasaran.push(resultSasaran);
        }))
        const indikatorData = await getIndikator(tujuan.idInstrumen);
        const dataTujuan = {
            idTujuan: tujuan.idInstrumen,
            namaTujuan: tujuan.tujuan,
            idUrusan: tujuan.urusan,
            idSubUrusan: tujuan.subUrusan,
            targetPaguIndikatif: tujuan.paguIndikatif,
            indikatorData,
            dataSasaran
        }

        result.push(dataTujuan)
    }))

    return result;
}

const renjaListFinalisasi = async (req) => {
    try {
        const userId = req.user.userId;
        const tahun = parseInt(req.params.tahun);

        // Mendapatkan daftar idSubKegiatan dari aksesmenu
        const subList = await prismaClient.aksesmenu.findFirst({
            where: {
                idUser: userId,
            }
        });

        let idKegiatan = subList.idKegiatan.replace('[', '').replace(']', ''); // Menghapus tanda kurung siku
        let _idSubKegiatans = idKegiatan.split(',').map(id => parseInt(id.trim())); // Mendapatkan array idSubKegiatan

        // Mengambil semua uraianTriwulan berdasarkan _idSubKegiatans dalam satu query
        let uraianTriwulan = await prismaClient.uraianTriwulan.findMany({
            where: {
                idSubKegiatan: {
                    in: _idSubKegiatans,
                }
            },
            select: {
                idUraian: true,
                idSubKegiatan: true,
                capaianVolume: true,
            }
        });

        // Mengelompokkan uraian berdasarkan idSubKegiatan
        let subKegiatanMap = {};
        for (const uraian of uraianTriwulan) {
            let _idSubKegiatan = uraian.idSubKegiatan;
            let subKegiatanInstrumen = await getInstrumen(_idSubKegiatan);

            if (!subKegiatanMap[_idSubKegiatan]) {
                subKegiatanMap[_idSubKegiatan] = {
                    idSubKegiatan: _idSubKegiatan,
                    nama: subKegiatanInstrumen.tujuan,
                    uraian: [],
                };
            }

            subKegiatanMap[_idSubKegiatan].uraian.push(uraian);
        }

        // Mengumpulkan hasil uji capaian untuk setiap idUraian unik
        let uraianIds = [...new Set(uraianTriwulan.map(u => u.idUraian))];
        let allUraian = await prismaClient.uraianTriwulan.findMany({
            where: {
                idUraian: {
                    in: uraianIds,
                },
                capaianVolume: {
                    gt: String(0),
                }
            },
            select: {
                idUraian: true,
            }
        });

        let idUraianWithVolume = new Set(allUraian.map(u => u.idUraian));

        // Memproses setiap subKegiatan dalam subKegiatanMap
        let data = [];
        for (const key in subKegiatanMap) {
            let _data = subKegiatanMap[key];
            let _uraianIds = _data.uraian.map(u => u.idUraian);

            _data.uraian = !_uraianIds.some(id => !idUraianWithVolume.has(id));

            // Mengambil informasi PAGU TRIWULAN, OUTCOME, dan faktor renja
            let [allPaguTriwulan, capaianOutcome, faktorRenja] = await Promise.all([
                prismaClient.totalPersenPagu.findFirst({
                    select: {
                        TotalPersen: true,
                    },
                    where: {
                        idSubKegiatan: parseInt(key)
                    }
                }),
                cekCapaianOutcome(parseInt(key), tahun),
                prismaClient.faktorRenja.findFirst({
                    where: {
                        idSubKegiatan: parseInt(key),
                    }
                })
            ]);

            _data.paguTriwulan = allPaguTriwulan.TotalPersen !== 0;
            _data.capaianOutcome = capaianOutcome != null;
            _data.faktorRenja = faktorRenja != null;
            _data.finish = _data.uraian && _data.paguTriwulan && _data.capaianOutcome && _data.faktorRenja;

            // Mendapatkan idKegiatan dari subKegiatan
            let subKegiatanKegiatan = await prismaClient.instrumen.findFirst({
                select: {
                    parent: true,
                },
                where: {
                    idInstrumen: parseInt(key)
                }
            });

            _data.idKegiatan = subKegiatanKegiatan.parent;
            data.push(_data);
        }

        // Mengelompokkan data kegiatan berdasarkan idKegiatan
        let kegiatanMap = {};
        for (const item of data) {
            let idKegiatan = item.idKegiatan;
            let kegiatanInstrumen = await getInstrumen(idKegiatan);

            if (!kegiatanMap[idKegiatan]) {
                kegiatanMap[idKegiatan] = {
                    idKegiatan: idKegiatan,
                    nama: kegiatanInstrumen.tujuan,
                    capaianOutcome: cekCapaianOutcome(idKegiatan, tahun) !== null,
                    finish: null,
                    subKegiatan: [],
                };
            }

            kegiatanMap[idKegiatan].subKegiatan.push(item);
        }

        // Mengubah kegiatanMap menjadi array hasil
        let hasil = Object.values(kegiatanMap).map(kegiatan => ({
            ...kegiatan,
            finish: kegiatan.subKegiatan.every(s => s.finish),
        }));

        // Mengelompokkan kegiatan berdasarkan idProgram
        let programMap = {};
        for (const item of hasil) {
            let kegiatanInstrumen = await getInstrumen(item.idKegiatan);
            let getSasaranInstrumen = await getInstrumen(kegiatanInstrumen.parent);

            let idProgram = item.idProgram;
            let getProgramInstrumen = await getInstrumen(kegiatanInstrumen.parent);

            if (!programMap[idProgram]) {
                programMap[idProgram] = {
                    idProgram: kegiatanInstrumen.parent,
                    nama: getProgramInstrumen.tujuan,
                    capaianOutcome: item.capaianOutcome,
                    finish: null,
                    kegiatan: [],
                    idSasaran: getSasaranInstrumen.parent,
                };
            }

            programMap[idProgram].kegiatan.push(item);
        }

        // Menghitung nilai finish untuk setiap program
        let hasilAkhir = Object.values(programMap).map(program => ({
            ...program,
            finish: program.kegiatan.every(k => k.finish),
        }));

        let sasaranMap = {};

        for (const item of hasilAkhir) {
            let sasaranInstrumen = await getInstrumen(item.idSasaran);
            let idSasaran = item.idSasaran;
            if (!sasaranMap[idSasaran]) {
                sasaranMap[idSasaran] = {
                    idSasaran: idSasaran,
                    nama: sasaranInstrumen.tujuan,
                    capaianOutcome: item.capaianOutcome,
                    finish: null,
                    program: [],
                    idTujuan: sasaranInstrumen.parent,
                };
            }

            sasaranMap[idSasaran].program.push(item);
        }

        let hasilAkhirSasaran = Object.values(sasaranMap).map(sasaran => ({
            ...sasaran,
            finish: sasaran.program.every(p => p.finish),
        }));

        let tujuanMap = {};

        for (const item of hasilAkhirSasaran) {
            let tujuanInstrumen = await getInstrumen(item.idTujuan);
            let idTujuan = item.idTujuan;
            if (!tujuanMap[idTujuan]) {
                tujuanMap[idTujuan] = {
                    idTujuan: idTujuan,
                    nama: tujuanInstrumen.tujuan,
                    capaianOutcome: item.capaianOutcome,
                    finish: null,
                    sasaran: [],
                };
            }

            tujuanMap[idTujuan].sasaran.push(item);
        }

        let hasilAkhirTujuan = Object.values(tujuanMap).map(tujuan => ({
            ...tujuan,
            finish: tujuan.sasaran.every(s => s.finish),
        }));

        // Mengembalikan hasil akhir
        return hasilAkhirTujuan;

    } catch (e) {
        console.error(e);
        return e;
    }

}

const cekCapaianOutcome = async (idIndikator, tahun) => {
    let indikatorInstrumen = await prismaClient.indikatorInstrumen.findFirst({
        where: {
            idInstrumen: parseInt(idIndikator),
        }
    });

    // capaian outcome berdasarkan indikator instrumen
    let capaianOutcome = await prismaClient.capaianOutcome.findFirst({
        where: {
            idIndikator: indikatorInstrumen.idIndikator,
            tahun: tahun,
        },
    });

    return capaianOutcome;
}

const getInstrumen = async (idInstrumen) => {
    return prisma.instrumen.findFirst({
        where: {
            idInstrumen: parseInt(idInstrumen),
        }
    });

}

export {
    inputCapaian,
    listCapaian,
    inputRupiah,
    listCapaianRupiah,
    renjaListFinalisasi
}