import {PrismaClient} from "@prisma/client";
import {errorHandling} from '../middlewares/errorHandler.js'
import dates from "date-and-time";
import Joi from "joi";


const prisma = new PrismaClient()
process.env.TZ = 'Asia/Jakarta'

const cekIdPeriode = async (id) => {
    const result = await prisma.periode.findFirst({
        where: {
            idPeriode: id
        }
    })

    return result
}

const cekOpd = async (id) => {
    return prisma.opdUser.findFirst({
        where: {
            opdId: parseInt(id)
        }
    });
}

const addPeriode = async (req) => {
    const bodyAllow = Object.keys(req.body)
    bodyAllow.forEach(element => {
        if (element === 'namaPeriode' || element === 'waktuMulai' || element === 'waktuAkhir' || element === 'opd' || element === 'tahunPeriode') {
        } else {
            throw new errorHandling(401, 'Parameter salah silahakn cek body request')
        }
    });

    const addPriod = await prisma.periode.create({
        data: {
            tahunPeriode: parseInt(req.body.tahunPeriode),
            namaPeriode: req.body.namaPeriode.toString(),
            waktuMulai: new Date(req.body.waktuMulai),
            waktuAkhir: new Date(req.body.waktuAkhir),
            status: 1
        }
    })

    req.body.opd.every(async el => {
        const cek = await cekOpd(el.opdId)
        if (!cek) {
            return false
        } else {
            const cekExist = await prisma.akumulasiPeriode.findFirst({
                where: {
                    opd: el.opdId,
                    namaPeriode: req.body.namaPeriode.toString(),
                    status: 1
                }
            })
            if (!cekExist) {
                await prisma.akumulasiPeriode.create({
                    data: {
                        periode: parseInt(addPriod.idPeriode),
                        namaPeriode: addPriod.namaPeriode,
                        opd: parseInt(cek.opdId),
                        status: 1
                    }
                })
            }
        }
        return true
    });

    delete req.body['opd']

    req.body.status = addPriod.status
    req.body.idPeriode = addPriod.idPeriode

    return req.body
}

const updatePeriode = async (req) => {
    const validation = Joi.object({
        idPeriode: Joi.number().required(),
        dataUpdate: Joi.object({
            waktuMulai: Joi.date().required(),
            waktuAkhir: Joi.date().required(),
            namaPeriode: Joi.string().required(),
            tahunPeriode: Joi.number().required(),
        }),
        opd: Joi.required(),
        status: Joi.required(),
        semua: Joi.required(),
    });

    // validasi input yang di terima
    const {error} = validation.validate(req.body);

    if (error) {
        throw new errorHandling(422, error);
    }

    const {idPeriode, dataUpdate, opd, status, semua} = req.body;

    // Cek apakah ada periode dengan id tersebut
    const cekPeriode = await prisma.periode.findFirst({
        where: {
            idPeriode: idPeriode,
        }
    });

    if (!cekPeriode) {
        throw new errorHandling(404, 'Data Periode tidak di temukan');
    }

    // ambil data periode
    const dataPeriode = await prisma.periode.findFirst({
        where: {
            idPeriode: idPeriode,
        }
    });

    const {waktuMulai, waktuAkhir, namaPeriode, tahunPeriode} = dataUpdate;

    // Update data periode
    await prisma.periode.update({
        where: {
            idPeriode: idPeriode,
        },
        data: {
            waktuMulai: waktuMulai ? new Date(waktuMulai) : dataPeriode.waktuMulai,
            waktuAkhir: waktuAkhir ? new Date(waktuAkhir) : dataPeriode.waktuAkhir,
            namaPeriode: namaPeriode,
            tahunPeriode: tahunPeriode,
            status: status,
        }
    });

    let _opdBaru = [];

    if (semua) {
        let allOpd = await prisma.opdUser.findMany();

        allOpd.forEach((item) => {
            _opdBaru.push(item.opdId);
        });
    } else {
        opd.forEach((item) => {
            _opdBaru.push(item.opdId);
        });
    }

    // hapus duplikat dari opdBaru
    _opdBaru = [...new Set(_opdBaru)];

    // jika suatu opd sudah ada di akumulasiPeriode, maka hapus opd tersebut dari opdBaru
    let opdBaru = [];
    let dataAkumulasi = await prisma.akumulasiPeriode.findMany({
        where: {
            periode: parseInt(idPeriode),
        }
    });

    for (const item of _opdBaru) {
        for (const akumulasi of dataAkumulasi) {
            const checkIsExist = await prisma.akumulasiPeriode.findFirst({
                where: {
                    idAkumulasi: {
                        not: akumulasi.idAkumulasi,
                    },
                    namaPeriode: dataPeriode.namaPeriode,
                    opd: parseInt(item),
                },
            });

            if (!checkIsExist) {
                opdBaru.push(item);
            }
        }
    }

    // hapus semua data akumulasi periode yang ada sebelum memasukkan data yang baru
    await prisma.akumulasiPeriode.deleteMany({
        where: {
            periode: parseInt(idPeriode),
        }
    });

    // hapus duplikat dari opdBaru jika mungkin ada
    opdBaru = [...new Set(opdBaru)];

    // tambahkan data akumulasi periode yang baru
    for (const item of opdBaru) {
        await prisma.akumulasiPeriode.create({
            data: {
                periode: parseInt(idPeriode),
                namaPeriode: dataPeriode.namaPeriode,
                opd: parseInt(item),
                status: status,
            },
        });
    }

    return;
}

async function getAlldata(id) {
    return new Promise(async nilai => {
        const result = await prisma.akumulasiPeriode.findMany({
            where: {
                periode: parseInt(id)
            },
            select: {
                opd: true,
                idAkumulasi: false,
                periode: false
            }
        })
        nilai(result)
    })
}

const listData = async () => {
    const result = await prisma.periode.findMany()
    const datas = []
    await Promise.all(result.map(async (item) => {
        const timeAwal = new Date(item.waktuMulai)
        const dateAwal = dates.format(timeAwal, 'YYYY-MM-DD')
        const timeAkhir = new Date(item.waktuAkhir)
        const dateAkhir = dates.format(timeAkhir, 'YYYY-MM-DD')
        const opdList = await getAlldata(item.idPeriode);
        const dataList = {
            idPeriode: item.idPeriode,
            tahunPeriode: item.tahunPeriode,
            namaPeriode: item.namaPeriode,
            waktuMulai: dateAwal,
            waktuAkhir: dateAkhir,
            status: item.status,
            opdList
        }
        return dataList
    })).then((items) => {
        // script dari chatgpt bangke bingung bagian ini haha
        const groupedData = items.reduce((acc, item) => {
            const tahunPeriode = item.tahunPeriode;
            if (!acc[tahunPeriode]) {
                acc[tahunPeriode] = [];
            }
            acc[tahunPeriode].push(item);
            return acc;
        }, {});
        datas.push(groupedData)
    });
    return datas
}

const hapus = async (req) => {
    const idPeriode = req.params.id;

    // periksa apakah ada periode dengan id tersebut
    const cekData = await prisma.periode.findFirst({
        where: {
            idPeriode: parseInt(idPeriode)
        }
    });

    if (!cekData) {
        throw new errorHandling(404, 'Data Periode tidak di temukan')
    }

    // hapus dulu semua child periode, yaitu data akumulasi periode di tabel `akumulasiPeriode`
    await prisma.akumulasiPeriode.deleteMany({
        where: {
            periode: parseInt(idPeriode),
        }
    });

    // kemudian hapus data periode
    await prisma.periode.delete({
        where: {
            idPeriode: parseInt(idPeriode)
        }
    })

    // const cekData = await cekIdPeriode(parseInt(req.params.id))
    //
    // if(!cekData){
    //     throw new errorHandling(404,'Data Periode tidak di temukan')
    // }

    // const hapusPeriode = await prisma.periode.delete({
    //     where : {
    //         idPeriode : parseInt(req.params.id)
    //     },
    //     select : {
    //         idPeriode : true
    //     }
    // })
    //
    // const cekIdakumulasi = await prisma.akumulasiPeriode.findFirst({
    //     where : {
    //         periode : parseInt(hapusPeriode.idPeriode)
    //     },
    //     select :{
    //         idAkumulasi : true
    //     }
    // })
    //
    // await Promise.all([hapusPeriode,cekIdakumulasi]).then( async (item) => {
    //     await prisma.akumulasiPeriode.delete({
    //         where : {
    //             idAkumulasi : parseInt(item[1].idAkumulasi)
    //         }
    //     })
    // })

    return
}

const getSingleData = async (req) => {
    const cekData = await cekIdPeriode(parseInt(req.params.id))
    if (!cekData) {
        throw new errorHandling(404, 'Data Periode tidak di temukan')
    }
    const result = await prisma.periode.findMany({
        where: {
            idPeriode: parseInt(req.params.id)
        }
    })
    const datas = []
    await Promise.all(result.map(async (item) => {
        const timeAwal = new Date(item.waktuMulai)
        const dateAwal = dates.format(timeAwal, 'YYYY-MM-DD')
        const timeAkhir = new Date(item.waktuAkhir)
        const dateAkhir = dates.format(timeAkhir, 'YYYY-MM-DD')
        const opdList = await getAlldata(item.idPeriode);
        const dataList = {
            idPeriode: item.idPeriode,
            tahunPeriode: item.tahunPeriode,
            namaPeriode: item.namaPeriode,
            waktuMulai: dateAwal,
            waktuAkhir: dateAkhir,
            status: item.status,
            opdList
        }
        datas.push(dataList)
    }))
    return datas
}

const getSingleTahun = async (req) => {
    const cekData = await prisma.periode.findMany({
        where: {
            tahunPeriode: parseInt(req.params.tahun)
        }
    })

    if (!cekData) {
        throw new errorHandling(404, 'Data Periode tidak di temukan')
    }
    const datas = []
    await Promise.all(cekData.map(async (item) => {
        const timeAwal = new Date(item.waktuMulai)
        const dateAwal = dates.format(timeAwal, 'YYYY-MM-DD')
        const timeAkhir = new Date(item.waktuAkhir)
        const dateAkhir = dates.format(timeAkhir, 'YYYY-MM-DD')
        const opdList = await getAlldata(item.idPeriode);
        const dataList = {
            idPeriode: item.idPeriode,
            tahunPeriode: item.tahunPeriode,
            namaPeriode: item.namaPeriode,
            waktuMulai: dateAwal,
            waktuAkhir: dateAkhir,
            status: item.status,
            opdList
        }
        datas.push(dataList)
    }))
    return datas
}

const priodGetUser = async (req) => {
    const dataUser = req.user
    if (dataUser.roleId === '2' || dataUser.roleId === '3' || dataUser.roleId === '4') {
        const getAkumulasi = await prisma.akumulasiPeriode.findMany({
            where: {
                opd: parseInt(dataUser.odpId)
            }
        })
        const result = []
        await Promise.all(getAkumulasi.map(async (item) => {
            const getPriod = await prisma.periode.findFirst({
                where: {
                    idPeriode: parseInt(item.periode)
                }
            })
            const timeAwal = new Date(getPriod.waktuMulai)
            const dateAwal = dates.format(timeAwal, 'YYYY-MM-DD')
            const timeAkhir = new Date(getPriod.waktuAkhir)
            const dateAkhir = dates.format(timeAkhir, 'YYYY-MM-DD')
            const parse = {
                idPeriode: getPriod.idPeriode,
                tahunPeriode: getPriod.tahunPeriode,
                namaPeriode: getPriod.tahunPeriode,
                waktuMulai: dateAwal,
                waktuAkhir: dateAkhir,
                status: item.status
            }
            return parse
        })).then((periode) => {
            const priode = {
                periode
            }
            const parser = {
                evaluasi: [
                    {
                        RPJMD: 1
                    },
                    {
                        RPKD: 0
                    },
                    {
                        RENJA: 0
                    },
                    {
                        RENSTRA: 0
                    }
                ]
            }
            result.push(priode)
            result.push(parser)
        })

        return result
    } else {
        const getAkumulasi = await prisma.periode.findMany({})
        const uniqueTahunPeriode = [...new Set(getAkumulasi.map(item => item.tahunPeriode))];
        const groupedData = uniqueTahunPeriode.map(tahunPeriode => {
            return {
                tahunPeriode: tahunPeriode
            };
        })
        return groupedData
    }
}

export {
    addPeriode,
    updatePeriode,
    listData,
    hapus,
    getSingleData,
    getSingleTahun,
    priodGetUser
}
