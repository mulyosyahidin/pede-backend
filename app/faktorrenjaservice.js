import { PrismaClient } from "@prisma/client";
import { errorHandling } from '../middlewares/errorHandler.js'
import dates from "date-and-time";
import laporanService from "../src/services/laporan-service.js";

const prisma = new PrismaClient()
process.env.TZ = 'Asia/Jakarta'

async function cekPriode(priode) {
    return await prisma.periode.findFirst({
        where : {
            idPeriode : parseInt(priode)
        }
    })
}

async function cekSubKegiatan(id){
    return await prisma.instrumen.findFirst({
        where : {
            idInstrumen : parseInt(id),
            type : 'subKegiatan'
        }
    })
}

async function cekIndikator(id){
    return await prisma.indikatorInstrumen.findFirst({
        where : {
            idIndikator : parseInt(id)
        }
    })
}


const addFaktor = async(req)=>{
    const bodyAllow = Object.keys(req.body)
    bodyAllow.forEach(element => {
        if(element === 'idIndikator' || element === 'idPeriode'||element === 'faktorPendorong'||element === 'faktorPenghambat'||element === 'tindakLanjut'){
        }else{
            throw new errorHandling(401,'Parameter salah silahakn cek body request')
        }
    });
    const periodeExpired = await cekPriode(req.body.idPeriode)
    if(periodeExpired){
        if(periodeExpired.status < 1){
            throw new errorHandling(401,"Periode sudah berakhir tidak bisa edit")
        }
    }else{
        throw new errorHandling(404,"Periode tidak di temukan")
    }
    
    const indikatExist = await cekIndikator(req.body.idIndikator)
    if(!indikatExist){
        throw new errorHandling(404,"Data indikator tidak di temukan")
    }

    const cekFaktorExist = await prisma.faktorRenja.findFirst({
        where : {
            idSubKegiatan : parseInt(req.body.idIndikator),
            tahun : parseInt(periodeExpired.tahunPeriode),
            opd : parseInt(req.user.odpId)
        }
    })
    const result = []
    if(!cekFaktorExist){
        const input = await prisma.faktorRenja.create({
            data : {
                idSubKegiatan : parseInt(req.body.idIndikator),
                tahun : parseInt(periodeExpired.tahunPeriode),
                opd : parseInt(req.user.odpId),
                type : 'indikator',
                faktorPendorong : req.body.faktorPendorong ? req.body.faktorPendorong.toString() : '',
                faktorPenghambat : req.body.faktorPenghambat ? req.body.faktorPenghambat.toString() : '',
                tindakLanjut : req.body.tindakLanjut ? req.body.tindakLanjut.toString() : ''
            }
        })
        const dataPush = []
        await Promise.all([input]).then(()=>{
            const parser = {
                idIndikator : indikatExist.idIndikator,
                type : 'indikator',
                faktorPendorong : req.body.faktorPendorong ? req.body.faktorPendorong : '',
                faktorPenghambat : req.body.faktorPenghambat ? req.body.faktorPenghambat : '',
                tindakLanjut : req.body.tindakLanjut ? req.body.tindakLanjut : ''
            }
            dataPush.push(parser)
        })
        result.push(dataPush)
    }else{
        const input = await prisma.faktorRenja.updateMany({
            data : {
                faktorPendorong : req.body.faktorPendorong ? req.body.faktorPendorong.toString() : '',
                faktorPenghambat : req.body.faktorPenghambat ? req.body.faktorPenghambat.toString() : '',
                tindakLanjut : req.body.tindakLanjut ? req.body.tindakLanjut.toString() : ''
            },
            where : {
                idSubKegiatan : parseInt(req.body.idIndikator),
                tahun : parseInt(periodeExpired.tahunPeriode),
                opd : parseInt(req.user.odpId)
            }
        })
        const dataPush = []
        await Promise.all([input]).then(()=>{
            const parser = {
                idIndikator : indikatExist.idIndikator,
                type : 'indikator',
                faktorPendorong : req.body.faktorPendorong ? req.body.faktorPendorong : '',
                faktorPenghambat : req.body.faktorPenghambat ? req.body.faktorPenghambat : '',
                tindakLanjut : req.body.tindakLanjut ? req.body.tindakLanjut : ''
            }
            dataPush.push(parser)
        })
        result.push(dataPush)
    }

    return result
}

const getAllDataTriwulan = async (idSub,triwulan,tahun,opd)=> {
    const dataTarget = await prisma.targetTriwulan.findFirst({
        where : {
            idSubKegiatan : parseInt(idSub),
            tahun : parseInt(tahun),
            opd : parseInt(opd)
        }
    })

    const getDataCapaian = await prisma.uraianTotalTriwulan.findFirst({
        where: {
            idSubKegiatan : parseInt(idSub)
        }
    })

    const getDataRealisasi = await prisma.paguTriwulan.findFirst({
        where : {
            idSubKegiatan : parseInt(idSub),
            tahun : parseInt(tahun),
            opd : parseInt(opd),
            triwulan : parseInt(triwulan)
        }
    })
    const dataTargetTriwulan =[]
    await Promise.all([dataTarget,getDataCapaian,getDataRealisasi]).then((item)=>{
        if(triwulan === 1){
            const dataTarget = {
                triwulan : 1,
                target : item[0] ? item[0].t1 : 0,
                realisasiKinerja : item[1] ? item[1].triwulanSatu : 0,
                rp : item[2] ? item[2].paguIndikatif : 0,
                persenRealisasi : item[2] ? item[2].persenPaguTriwulan : 0
            }
            dataTargetTriwulan.push(dataTarget)
        }else if (triwulan === 2){
            const dataTarget = {
                triwulan : 2,
                target : item[0] ? item[0].t2 : 0,
                realisasiKinerja : item[1] ? item[1].triwulanDua : 0,
                rp : item[2] ? item[2].paguIndikatif : 0,
                persenRealisasi : item[2] ? item[2].persenPaguTriwulan : 0
            }
            dataTargetTriwulan.push(dataTarget)
        }else if (triwulan === 3){
            const dataTarget = {
                triwulan : 3,
                target : item[0] ? item[0].t3 : 0,
                realisasiKinerja : item[1] ? item[1].triwulanTiga : 0,
                rp : item[2] ? item[2].paguIndikatif : 0,
                persenRealisasi : item[2] ? item[2].persenPaguTriwulan : 0
            }
            dataTargetTriwulan.push(dataTarget)
        }else if (triwulan === 4){
            const dataTarget = {
                triwulan : 4,
                target : item[0] ? item[0].t4 : 0,
                realisasiKinerja : item[1] ? item[1].triwulanEmpat : 0,
                rp : item[2] ? item[2].paguIndikatif : 0,
                persenRealisasi : item[2] ? item[2].persenPaguTriwulan : 0
            }
            dataTargetTriwulan.push(dataTarget)
        }
    })

    return dataTargetTriwulan[0]    
}

const getIndikator = async (id) => {
    const getIndikatorData = await prisma.indikatorInstrumen.findMany({
        where: {
            idInstrumen: parseInt(id),
        },
        orderBy: {
            idIndikator: 'asc'
        }
    })

    const indikatorData = []
    await Promise.all(getIndikatorData.map(async (item) => {
        const getOut = await prisma.capaianOutcome.findFirst({
            where: {
                idIndikator: item.idIndikator
            }
        })
        const indikat = {
            idindikator: item.idIndikator,
            NamaIndikator: item.indikator_kinerja,
            target: item.target,
            satuan: item.satuan,
            realisasiOutcome: getOut ? getOut.capaianRealisasi : 0,
            persenRealisasiOutcome: getOut ? getOut.persenCapaianRealisasi : 0,
            targetAkhirPeriode: item.target_akhir_periode,
        }
        indikatorData.push(indikat)
    }))

    return indikatorData;
}

const getChildInstrumen = async (type, parent) => {
    const getInstrumen = await prisma.instrumen.findMany({
        where: {
            parent: parseInt(parent),
            type: type
        },
        orderBy: {
            idInstrumen: 'asc'
        }
    });
    return getInstrumen;
}

async function getFaktor (id,tahun,opd){
	const fetch = await prisma.faktorRenja.findFirst({
    	where: {
              idSubKegiatan: id,
              tahun: parseInt(tahun),
              opd: parseInt(opd)
        }
    })
    
    const result = {
    	faktorPendorong: fetch ? fetch.faktorPendorong : '',
        faktorPenghambat: fetch ? fetch.faktorPenghambat : '',
        tindakLanjut: fetch ? fetch.tindakLanjut : '',
    }
    
    return result
}

const listLaporan = async (req) => {
    const bodyAllow = Object.keys(req.params)
    bodyAllow.forEach(element => {
        if (element === 'tahun' || element === 'opd') {
        } else {
            throw new errorHandling(401, 'Parameter salah silahakn cek body request')
        }
    });

    const getDataTujuan = await prisma.instrumen.findMany({
        where: {
            periode: parseInt(req.params.tahun),
            opd: parseInt(req.params.opd),
            type: "tujuan"
        },
        orderBy: {
            idInstrumen: 'asc'
        }
    })
    const result = [];
    await Promise.all(getDataTujuan.map(async (tujuan) => {
        const getDataSasaran = await getChildInstrumen('sasaran', tujuan.idInstrumen);

        const dataSasaran = [];
        await Promise.all(getDataSasaran.map(async (sasaran) => {
            const getDataProgram = await getChildInstrumen('program', sasaran.idInstrumen);

            const dataProgram = [];
            await Promise.all(getDataProgram.map(async (program) => {
                const getDataKegiatan = await getChildInstrumen('kegiatan', program.idInstrumen);

                const dataKegiatan = [];
                await Promise.all(getDataKegiatan.map(async (kegiatan) => {
                    const getDataSubKegiatan = await getChildInstrumen('subKegiatan', kegiatan.idInstrumen);

                    const dataSubKegiatan = [];
                    await Promise.all(getDataSubKegiatan.map(async (sub) => {
                        const getIndikatorData = await getIndikator(sub.idInstrumen);
                        const indikatorData = [];

                        await Promise.all([getIndikatorData]).then(async(indikatorSub)=>{
                         	await Promise.all(indikatorSub[0].map(async(item)=>{
                              const faktorPendorongs = await getFaktor(item.idindikator,req.params.tahun,req.params.opd)
                              const faktorPenghambats = await getFaktor(item.idindikator,req.params.tahun,req.params.opd)
                              const tindakLanjuts = await getFaktor(item.idindikator,req.params.tahun,req.params.opd)
                              const resultIndikatorSub =  {...item,
                                 faktorPendorong: faktorPendorongs.faktorPendorong,
                                 faktorPenghambat: faktorPenghambats.faktorPenghambat,
                                 tindakLanjut: tindakLanjuts.tindakLanjut,
                              }
                              return resultIndikatorSub
                        	})).then((is)=>{
                            	indikatorData.push(is[0])
                            })
                        })
                        const getTotalTarget = await prisma.uraianTotalTriwulan.findFirst({
                            where: {
                                idSubKegiatan: parseInt(sub.idInstrumen)
                            }
                        })

                        const getTotalRealisasi = await prisma.totalPersenPagu.findFirst({
                            where: {
                                idSubKegiatan: parseInt(sub.idInstrumen)
                            }
                        });

                        const dataTriwulanSatu = await getAllDataTriwulan(sub.idInstrumen, 1, req.params.tahun, req.params.opd)
                        const dataTriwulanDua = await getAllDataTriwulan(sub.idInstrumen, 2, req.params.tahun, req.params.opd)
                        const dataTriwulanTiga = await getAllDataTriwulan(sub.idInstrumen, 3, req.params.tahun, req.params.opd)
                        const dataTriwulanEmpat = await getAllDataTriwulan(sub.idInstrumen, 4, req.params.tahun, req.params.opd)
                        const dataTriwulan = []
                        await Promise.all([dataTriwulanSatu, dataTriwulanDua, dataTriwulanTiga, dataTriwulanEmpat]).then((i) => {
                            dataTriwulan.push(i)
                        })
                        const targetDanCapaian = {
                            TotalKinerja: getTotalTarget ? getTotalTarget.totalKinerja : 0,

                            TotalRealisasi: getTotalRealisasi ? getTotalRealisasi.TotalPersen : 0,
                            dataTriwulan
                        }

                       // const cekFinal = await prisma.finalisasirenja.findMany({
                           // where: {
                           //     idInstrumen: parseInt(sub.idInstrumen)
                           // }
                       // })

                        const resultSub = {
                            idSubKegiatan: sub.idInstrumen,
                            namaSubKegiatan: sub.tujuan,
                           // statusFinalisasi: cekFinal.length > 0 ? true : false,
                            idUrusan: sub.urusan,
                            idSubUrusan: sub.subUrusan,
                            targetPaguIndikatif: sub.paguIndikatif,
                            indikatorData,
                            targetDanCapaian
                        }
                        dataSubKegiatan.push(resultSub);
                    }))
                    const indikatorData = await getIndikator(kegiatan.idInstrumen);
                    const resultKegiatan = {
                        idKegiatan: kegiatan.idInstrumen,
                        namaKegiatan: kegiatan.tujuan,
                        idUrusan: kegiatan.urusan,
                        idSubUrusan: kegiatan.subUrusan,
                        targetPaguIndikatif: kegiatan.paguIndikatif,
                        indikatorData,
                        dataSubKegiatan
                    }
                    dataKegiatan.push(resultKegiatan)
                }))
                const indikatorData = await getIndikator(program.idInstrumen);
                const resultProgram = {
                    idProgram: program.idInstrumen,
                    namaProgram: program.tujuan,
                    idUrusan: program.urusan,
                    idSubUrusan: program.subUrusan,
                    targetPaguIndikatif: program.paguIndikatif,
                    indikatorData,
                    dataKegiatan
                }
                dataProgram.push(resultProgram);
            }))
            const indikatorData = await getIndikator(sasaran.idInstrumen);
            const resultSasaran = {
                idSasaran: sasaran.idInstrumen,
                namaSasaran: sasaran.tujuan,
                idUrusan: sasaran.urusan,
                idSubUrusan: sasaran.subUrusan,
                targetPaguIndikatif: sasaran.paguIndikatif,
                indikatorData,
                dataProgram
            }
            dataSasaran.push(resultSasaran);
        }))
        const indikatorData = await getIndikator(tujuan.idInstrumen);
        const dataTujuan = {
            idTujuan: tujuan.idInstrumen,
            namaTujuan: tujuan.tujuan,
            idUrusan: tujuan.urusan,
            idSubUrusan: tujuan.subUrusan,
            targetPaguIndikatif: tujuan.paguIndikatif,
            indikatorData,
            dataSasaran
        }
        result.push(dataTujuan);
    }))

    if (result.length < 1) {
        throw new errorHandling(404, "Data capaian belum ada")
    }

    return result
}

const excelLaporanF = async (req) => {
    const data = await listLaporan(req);

    return await laporanService.renja(req, data);
}

export {
    addFaktor,
    listLaporan,
    excelLaporanF,
    getAllDataTriwulan,
    getChildInstrumen,
    getIndikator,
}