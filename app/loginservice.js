import { PrismaClient } from '@prisma/client'
import {v4 as uuid} from "uuid"
import bycrypt from "bcrypt"
import dates from "date-and-time"
import { errorHandling } from '../middlewares/errorHandler.js'

const prisma = new PrismaClient()
process.env.TZ = 'Asia/Jakarta'

const countAuth = async (req) => {
    const user = await prisma.users.findMany({
        where : {
            username : req.body.username
        },
        select : {
            password : true,
            userId : true
        }
    })    

    // console.log(await bycrypt.hash('admin12345', 10))


    if (Object.keys(user).length === 0) {
        throw new errorHandling(400,'Username atau password salah')
    }
    const cekPass =  await bycrypt.compare(req.body.password, user[0].password);
    if(!cekPass){
        throw new errorHandling(400,'Username atau password salah')
    }
    const token = uuid();
    const date = new Date();
    date.setHours(24,0,0);
    const result = await prisma.users.update({
            data :{
                token : token,
                waktuBerlaku : date
            },
            where : {
                userId : user[0].userId
            },
            select : {
                username : true,
                email : true,
                userId : true,
                roleId : true,
                odpId : true,
                token : true
            }
        })
    
        const times = dates.format(date,'YYYY-MM-DD HH:MM:SS')
        const respon = {
            "token" : result.token,
            "date" : times,
            "roleId" : result.roleId,
            "odpId" : result.odpId,
            "username" : result.username
        }
        return respon
}

export {countAuth}
