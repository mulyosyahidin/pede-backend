import { PrismaClient } from '@prisma/client'
import {v4 as uuid} from "uuid"
import bycrypt from "bcrypt"
import { errorHandling } from '../middlewares/errorHandler.js'
import { getAksesMenu } from "../app/hakaksesservice.js"

const prisma = new PrismaClient()
process.env.TZ = 'Asia/Jakarta'

const validasiUsername = async (req) => {
    const result = await prisma.users.findFirst({
        where : {
            username : req.body.username
        }
    })

    return result
}

const validasiId = async (req) => {
    const results = await prisma.users.findUnique({
        where : {
            userId : req
        }
    })

    return results
}

const addUser = async (req) => {
    const cekValid = await validasiUsername(req);
    if(cekValid) {
        throw new errorHandling(401,'Username sudah ada, silahkan ganti username lain')
    }
    
    const passwordHas = await bycrypt.hash(req.body.password,10)
    const token = uuid()
    const idAdmin = req.user.userId
    const result = await prisma.users.create({
        data : {
            username : req.body.username,
            email : req.body.email,
            password : passwordHas,
            odpId :  req.body.kodeOdp.toString(),
            roleId : req.body.kodeRole.toString(),
            idCreate :idAdmin.toString(),
            token : token
        }
    })

    await prisma.namaUser.create({
        data : {
            userId: result.userId.toString(),
            namaUser : req.body.nama
        }
    })
    
    const returns = {
        idUser : result.userId,
        username : result.username,
        roleId : result.roleId
    }

    return returns
}

const updateUser = async (req) => {
    const cekValid = await validasiId(parseInt(req.body.id));
    if(!cekValid) {
        throw new errorHandling(401,'User tidak terdaftar di sistem')
    }

    if (req.body.data.username){
        throw new errorHandling(401,'Username tidak dapat di ubah')
    }

    if(req.body.data.password){
        req.body.data.password = await bycrypt.hash(req.body.data.password,10)
    }

    if(req.body.data.odpId){
        req.body.data.odpId = req.body.data.odpId.toString()
    }

    if(req.body.data.roleId){
        req.body.data.roleId = req.body.data.roleId.toString()
    }

    if (req.body.data.namaUser){
        req.body.name = req.body.data.namaUser
        delete req.body.data['namaUser'];
    }


    const updates = await prisma.users.update({
        data : req.body.data,
        where : {
            userId : req.body.id
        },
        select : {
            username : true
        }
    })

    if (req.body.name){
        const getNama = await prisma.namaUser.findFirst({
            where : {
                userId : req.body.id.toString()
            }
        })
        const rest = await prisma.namaUser.update({
            data : {
                namaUser : req.body.name
            },
            where : {
                namaId : getNama.namaId
            }
        })
    }

    return updates
}

const hapusUser = async (req) => {
    const cekId = await validasiId(parseInt(req.params.id))
    
    if(!cekId) {
        throw new errorHandling(401,'User tidak terdaftar di sistem')
    }

    const hapusUser = await prisma.users.delete({
        where : {
            userId : parseInt(req.params.id)
        }
    })

    const getNama = await prisma.namaUser.findFirst({
        where : {
            userId : req.params.id.toString()
        }
    })

    const hapusNama = await prisma.namaUser.delete({
        where : {
            namaId : getNama.namaId
        }
    })

    return

}

const listAllUser = async (req) => {
    const bodyAllow = Object.keys(req.params)
    bodyAllow.forEach(element => {
        if(element === 'page'){
        }else{
            throw new errorHandling(401,'Parameter salah silahakn cek body request')
        }

    });



    const countData = await prisma.users.aggregate({
        _count: {
            userId : true
        }
    })
    const JumlahPage = Math.ceil(parseInt(countData._count.userId)/20)
    if(req.params.page > JumlahPage) {
        throw new errorHandling(401,'Tidak ada data pada page yang di pilih')
    }
    const skipUserList = (req.params.page == 1) ? 0 : (req.params.page - 1) * 20

    const getUserData = await prisma.users.findMany({
        skip : parseInt(skipUserList),
        take : 20 ,
        orderBy: {
            userId: 'desc',
        }
    })
    const dataUser = []
    await Promise.all(getUserData.map(async(item)=>{
        const nameUser = await prisma.namaUser.findFirst({
            where : {
                userId : item.userId.toString()
            }
        })

        const roleUser = await prisma.roleUser.findFirst({
            where : {
                roleId : parseInt(item.roleId)
            }
        })

        const opdIds = item.odpId ? item.odpId : 0

        const opdUser = await prisma.opdUser.findFirst({
            where : {
                opdId : parseInt(opdIds)
            }
        })

        const result = []
        await Promise.all([nameUser,roleUser,opdUser]).then(async(it)=>{
            const hak = await getAksesMenu(item.userId)
            const dataNama = {
                userId : item.userId,
                namaUser : it[0] ? it[0].namaUser : null ,
                email : item.email,
                username : item.username,
                roleId : item.roleId,
                nameRole : it[1] ? it[1].namaRole : null,
                opdId : item.odpId,
                nameOPD : it[2] ? it[2].namaOpd : null,
                hakAkses : hak ? hak : [null]
            }
            result.push(dataNama)
        })

        dataUser.push(result[0])
    }))
    const dataAkhir = {
        jumlahPage : JumlahPage,
        jumlahData : countData._count.userId,
        statusPage : req.params.page,
        dataUser
    }
    return dataAkhir
}


const listUserKeyword = async (req) => {
    const bodyAllow = Object.keys(req.params)
    bodyAllow.forEach(element => {
        if(element === 'page' || element === 'keyword'){
        }else{
            throw new errorHandling(401,'Parameter salah silahakn cek body request')
        }

    });

    const namaUser = await prisma.namaUser.findMany({
        where : {
            namaUser : {
                contains : req.params.keyword.toString()
            }
        },
        select : {
            userId : true
        }
    })


    const getUsername = await prisma.users.findMany({
        where : {
            username : {
                contains : req.params.keyword.toString()
            }
        },
        select : {
            userId : true
        }
    })

    const getEmail = await prisma.users.findMany({
        where : {
            email : {
                contains : req.params.keyword.toString()
            }
        },
        select : {
            userId : true
        }
    })

    const getOpd = async()=>{
        const opd = await prisma.opdUser.findMany({
            where : {
                namaOpd : {
                    contains : req.params.keyword.toString()
                }
            },
            select : {
                opdId : true
            }
        })
        const result = []
        await Promise.all([opd]).then(async(item)=>{
            const data = (item[0].length > 0) ? item[0] : [{opdId:0}]
            const stringData = data.map(items => items.opdId.toString());
            const getIduser = await prisma.users.findMany({
                where : {
                    odpId : {
                       in : stringData
                    }
                },
                select : {
                    userId : true
                }
            })
            result.push(getIduser)
        })
        return result[0]
    }
    const dataId = []
    await Promise.all([namaUser,getUsername,getEmail,await getOpd()]).then(async(user)=>{
        const flatArray = user.flat();
        // Menggunakan map dan reduce untuk menggabungkan nilai-nilai yang sama dan mengubah userId menjadi integer
        const resultArray = flatArray
        .map(item => ({ userId: parseInt(item.userId, 10) }))
        .reduce((acc, { userId }) => {
            // Jika userId belum ada di dalam array acc, tambahkan
            if (!acc.find(item => item.userId === userId)) {
            acc.push({ userId });
            }
            return acc;
        }, []);
        const parserData = resultArray.map(items => parseInt(items.userId));
        dataId.push(parserData)
    })
    const countData = await prisma.users.aggregate({
        _count: {
            userId : true
        },
        where : {
            userId : {
                in : dataId[0] ? dataId[0] : dataId
            }
        }
    })
    const JumlahPage = Math.ceil(parseInt(countData._count.userId)/20)
    if(req.params.page > JumlahPage) {
        throw new errorHandling(401,'Tidak ada data pada page yang di pilih')
    }
    const skipUserList = (req.params.page == 1) ? 0 : (req.params.page - 1) * 20

    const getUserData = await prisma.users.findMany({
        skip : parseInt(skipUserList),
        take : 20 ,
        orderBy: {
            userId: 'desc',
        },
        where : {
            userId : {
                in : dataId[0] ? dataId[0] : dataId
            }
        }
    })
    const dataUser = []
    await Promise.all(getUserData.map(async(item)=>{
        const nameUser = await prisma.namaUser.findFirst({
            where : {
                userId : item.userId.toString()
            }
        })

        const roleUser = await prisma.roleUser.findFirst({
            where : {
                roleId : parseInt(item.roleId)
            }
        })

        const opdUser = await prisma.opdUser.findFirst({
            where : {
                opdId : parseInt(item.odpId)
            }
        })

        const result = []
        await Promise.all([nameUser,roleUser,opdUser]).then(async(it)=>{
            const hak = await getAksesMenu(item.userId)
            const dataNama = {
                userId : item.userId,
                namaUser : it[0] ? it[0].namaUser : null ,
                email : item.email,
                username : item.username,
                roleId : item.roleId,
                nameRole : it[1] ? it[1].namaRole : null,
                opdId : item.odpId,
                nameOPD : it[2] ? it[2].namaOpd : null,
                hakAkses : hak ? hak : [null]
            }
            result.push(dataNama)
        })

        dataUser.push(result[0])
    }))
    const dataAkhir = {
        jumlahPage : JumlahPage,
        jumlahData : countData._count.userId,
        statusPage : req.params.page,
        dataUser
    }
    return dataAkhir
}
const getUser = async (req) => {
    const cekValid = await validasiId(parseInt(req.params.id));
    if(!cekValid) {
        throw new errorHandling(401,'User tidak terdaftar di sistem')
    }
    try{
        const getUsers = await prisma.$queryRaw`
          SELECT *
          FROM users
          LEFT JOIN namaUser ON users.userId = namaUser.userId
          LEFT JOIN roleUser ON users.roleId = roleUser.roleId
          LEFT JOIN opdUser ON users.odpId = opdUser.opdId
          WHERE users.userId = ${req.params.id}
        `;
        const hak = await getAksesMenu(getUsers[0].userId)
        const result = {
            "userId"   : getUsers[0].userId,
            "namaUser" : getUsers[0].namaUser,
            "email"    : getUsers[0].email,
            "username" : getUsers[0].username,
            "roleId"   : getUsers[0].roleId,
            "nameRole" : getUsers[0].namaRole,
            "odpId"    : getUsers[0].odpId,
            "namaOPD"  : getUsers[0].namaOpd,
            "hakAkses" : hak ? hak : [null]
        }
        return result
    }finally{
        await prisma.$disconnect()
    }
}

const getUserAll = async (req) => {
    const cekValid = await validasiId(parseInt(req.user.userId));
    if(!cekValid) {
        throw new errorHandling(401,'User tidak terdaftar di sistem')
    }
    try{
        const getUsers = await prisma.$queryRaw`
          SELECT *
          FROM users
          LEFT JOIN namaUser ON users.userId = namaUser.userId
          LEFT JOIN roleUser ON users.roleId = roleUser.roleId
          LEFT JOIN opdUser ON users.odpId = opdUser.opdId
          WHERE users.userId = ${req.user.userId}
        `;
        const hak = await getAksesMenu(getUsers[0].userId)
        const result = {
            "userId"   : getUsers[0].userId,
            "namaUser" : getUsers[0].namaUser,
            "email"    : getUsers[0].email,
            "username" : getUsers[0].username,
            "roleId"   : getUsers[0].roleId,
            "nameRole" : getUsers[0].namaRole,
            "odpId"    : getUsers[0].odpId,
            "namaOPD"  : getUsers[0].namaOpd,
            "hakUser" : hak ? hak : [null]
        }
        return result
    }finally{
        await prisma.$disconnect()
    }
}

const removeToken = async (req) => {
    const cekValid = await validasiId(parseInt(req.user.userId));
    if(!cekValid) {
        throw new errorHandling(401,'User tidak terdaftar di sistem')
    }

    return await prisma.users.update({
        data : {
            waktuBerlaku : null
        },
        where : {
            userId : parseInt(req.user.userId)
        }
    })
}

const updateSingleUser = async (req) =>{
    const bodyAllow = Object.keys(req.body.data)
    bodyAllow.forEach(element => {
        if(element === 'namaUser' || element === 'email' || element === 'password'){
        }else{
            throw new errorHandling(401,'Parameter salah silahakn cek body request')
        }

    });
    const cekValid = await validasiId(parseInt(req.user.userId));
    if(!cekValid) {
        throw new errorHandling(401,'User tidak terdaftar di sistem')
    }
    
    if(req.body.data.password){
        req.body.data.password = await bycrypt.hash(req.body.data.password,10)
    }

    
    if (req.body.data.namaUser){
        req.body.name = req.body.data.namaUser
        delete req.body.data['namaUser'];
    }
    const updates = await prisma.users.update({
        data : req.body.data,
        where : {
            userId : parseInt(req.user.userId)
        },
        select : {
            username : true
        }
    })

    

    if (req.body.name){
        const getNama = await prisma.namaUser.findFirst({
            where : {
                userId : req.user.userId.toString()
            }
        })
        const rest = await prisma.namaUser.update({
            data : {
                namaUser : req.body.name
            },
            where : {
                namaId : getNama.namaId
            }
        })
    }

    return updates
}

const userRole = async () => {
    const result = await prisma.roleUser.findMany()
    return result
}


export {
    addUser,
    updateUser,
    hapusUser,
    listAllUser,
    getUser,
    getUserAll,
    removeToken,
    updateSingleUser,
    userRole,
    listUserKeyword
}
