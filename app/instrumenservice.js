import { PrismaClient } from "@prisma/client";
import { errorHandling } from '../middlewares/errorHandler.js'
import dates from "date-and-time";

const prisma = new PrismaClient()
process.env.TZ = 'Asia/Jakarta'


async function cekSuburusan(idSubs) {
    return await prisma.subUrusan.findFirst({
        where : {
            idSub : parseInt(idSubs)
        }
    })
}

async function cekTujuanSubmit(idtujuan,type) {
    return await prisma.eRPJMD.findFirst({
        where : {
            idRpmjd : parseInt(idtujuan),
            type : type.toString()
        }
    })
}

async function getKode(id,type) {
    const result = await prisma.kodeInstrumen.findMany({
        where:{
            idIstrumen : parseInt(id),
            type : type.toString()
        }
    })

    const retur = result[0] ? result[0].kode : null
    return retur
}


const inputEval = async (req) =>{
    const bodyAllow = Object.keys(req.body)
    bodyAllow.forEach(element => {
        if(element === 'type' || element === 'kode' || element === 'idSubUrusan'||element === 'idInstrumen'||element === 'dataDesc'||element === 'tahun'||element === 'opd'||element === 'paguIndikatif'){
        }else{
            throw new errorHandling(401,'Parameter salah silahakn cek body request')
        }

    });
    const subExist = await cekSuburusan(req.body.idSubUrusan)
    if(!subExist){
        throw new errorHandling(404,"Data Sub Urusan tidak di temukan")
    }
    if(!req.body.idInstrumen === null){
        if(!await cekTujuanSubmit(req.body.idRpjmd,req.body.type)){
            throw new errorHandling(404,"Data parent tidak ditemukan")
        }
    }

    const create = await prisma.instrumen.create({
            data : {
                urusan : parseInt(subExist.urusan),
                subUrusan : parseInt(req.body.idSubUrusan),
                periode : req.body.tahun ? parseInt(req.body.tahun):null,
                tujuan : req.body.dataDesc.nama.toString(),
                dateCreate : new Date(),
                type : req.body.type.toString(),
                opd : req.body.opd ? parseInt(req.body.opd):null,
                idCreate : parseInt(req.user.userId),
                parent : req.body.idInstrumen ? parseInt(req.body.idInstrumen) : null,
                paguIndikatif : req.body.paguIndikatif ? parseInt(req.body.paguIndikatif) : null
            }
        })

    const indikator = []
    await Promise.all([create]).then(async (items)=>{
        const cekKode = await prisma.kodeInstrumen.findMany({
            where : {
                idIstrumen : parseInt(items[0].idInstrumen),
                type : req.body.type.toString()
            }
        })
        if(cekKode.length > 0){
            await prisma.kodeInstrumen.updateMany({
                data : {
                    kode : req.body.kode ? req.body.kode.toString() : null 
                },
                where : {
                    idIstrumen : parseInt(items[0].idInstrumen),
                    type : req.body.type.toString()
                }
            })
        }else{
            await prisma.kodeInstrumen.create({
                data : {
                    idIstrumen : parseInt(items[0].idInstrumen),
                    type : req.body.type.toString(),
                    kode : req.body.kode ? req.body.kode.toString() : null 
                }
            })
        }
        if(req.body.dataDesc.indikator){
            const aa = req.body.dataDesc.indikator.map( async (item) => {
            return await prisma.indikatorInstrumen.create({
                    data : {
                        idInstrumen : parseInt(items[0].idInstrumen),
                        indikator_kinerja : item.namaIndikator.toString(),
                        data_capaian_awal : item.DataCapaianAwal,
                        target_akhir_periode : item.TargetAkhirPeriode,
                        satuan : item.satuan.toString(),
                        target : parseInt(item.target)
                    }
                })
            })
            await Promise.all(aa).then((ite) => {
                indikator.push(ite)
            })
        }
    })
    const indikators = indikator[0] ? indikator[0] : indikator 
    const dataAkhir = {
        "type" : create.type,
        "periode" : create.periode,
        "idUrusan" : create.urusan,
        "idSubUrusan"  : create.subUrusan,
        "idparent" : create.parent,
        "idInstrumen" : create.idInstrumen,
        "kode" : req.body.kode,
        "pagiIndikatif" : create.paguIndikatif, 
        "dataDesc" :{
            "nama" : create.tujuan,
             indikators
        }
    }

    await prisma.logAktivitas.create({
        data : {
            userId : parseInt(req.user.userId),
            data : parseInt(create.idInstrumen),
            dateCreate : new Date(),
            aktiviti : "tambah"
        }
    })

    return dataAkhir
    
}

const listEval = async (req)=> {
    const fetchTujuan = await prisma.instrumen.findMany({
        where : {
            type : 'tujuan',
            periode : parseInt(req.params.tahun),
            opd :  parseInt(req.params.opd)
        }
    })
    if(fetchTujuan.length < 1){
        throw new errorHandling(404,"Data yang di cari tidak ada")
    }
    const result = []
    await Promise.all(fetchTujuan.map(async (item)=>{
        const indikator = await prisma.indikatorInstrumen.findMany({
            where : {
                idInstrumen : parseInt(item.idInstrumen)
            }
        })
        const sasaran = await prisma.instrumen.findMany({
            where : {
                parent : parseInt(item.idInstrumen),
                type : 'sasaran'
            }
        })
        const sasaranInstrumen = []
        await Promise.all(sasaran.map(async (ite) =>{
            const indikatorSasaran = await prisma.indikatorInstrumen.findMany({
                where : {
                    idInstrumen : parseInt(ite.idInstrumen)
                },
                select : {
                    idIndikator : true,
                    indikator_kinerja : true,
                    data_capaian_awal : true,
                    target_akhir_periode : true,
                    satuan : true,
                    target : true,
                    data_capaian_akhir : true,
                    rasio_capaian_akhir : true
                }
            })
            const program = await prisma.instrumen.findMany({
                where : {
                    parent : ite.idInstrumen,
                    type : 'program'
                }
            })

            const programInstrumen = []
            await Promise.all(program.map(async (ites) =>{
                const indikatorProgram = await prisma.indikatorInstrumen.findMany({
                    where : {
                        idInstrumen : parseInt(ites.idInstrumen)
                    },
                    select : {
                        idIndikator : true,
                        indikator_kinerja : true,
                        data_capaian_awal : true,
                        target_akhir_periode : true,
                        satuan : true,
                        target : true,
                        data_capaian_akhir : true,
                        rasio_capaian_akhir : true
                    }
                })
                const kegiatan = await prisma.instrumen.findMany({
                    where : {
                        parent : parseInt(ites.idInstrumen),
                        type : 'kegiatan'
                    }
                })
                const kegiatanData = []
                await Promise.all(kegiatan.map(async(item)=>{
                    const indikatorKagiatan = await prisma.indikatorInstrumen.findMany({
                        where : {
                            idInstrumen : parseInt(item.idInstrumen)
                        },
                        select : {
                            idIndikator : true,
                            indikator_kinerja : true,
                            data_capaian_awal : true,
                            target_akhir_periode : true,
                            satuan : true,
                            target : true,
                            data_capaian_akhir : true,
                            rasio_capaian_akhir : true
                        }
                    })

                    const subKegiatan = await prisma.instrumen.findMany({
                        where : {
                            parent : parseInt(item.idInstrumen),
                            type : 'subKegiatan'
                        }
                    })

                    const subKegiatanData = []

                    await Promise.all(subKegiatan.map( async(item)=>{
                        const indikatorSubKegiatan = await prisma.indikatorInstrumen.findMany({
                            where : {
                                idInstrumen : parseInt(item.idInstrumen)
                            },
                            select : {
                                idIndikator : true,
                                indikator_kinerja : true,
                                data_capaian_awal : true,
                                target_akhir_periode : true,
                                satuan : true,
                                target : true,
                                data_capaian_akhir : true,
                                rasio_capaian_akhir : true
                            }
                        })

                        const subkegiatans = {
                            idInstrumen : item.idInstrumen,
                            parent : item.parent,
                            subKegiatan : item.tujuan,
                            kode : await getKode(item.idInstrumen,'subKegiatan'),
                            paguIndikatifSubKegiatan : item.paguIndikatif,
                            indikatorSubKegiatan,
                        }

                        subKegiatanData.push(subkegiatans)
                    }))

                    const kegiatansub = {
                        idInstrumen : item.idInstrumen,
                        parent : item.parent,
                        kegiatan : item.tujuan,
                        kode : await getKode(item.idInstrumen,'kegiatan'),
                        paguIndikatifKegiatan : item.paguIndikatif,
                        indikatorKagiatan,
                        subKegiatanData
                    }

                    kegiatanData.push(kegiatansub)
                }))
                const programR = {
                    idInstrumen : ites.idInstrumen,
                    parent : ites.parent,
                    program : ites.tujuan,
                    kode : await getKode(ites.idInstrumen,'program'),
                    paguIndikatifProgram : ites.paguIndikatif,
                    indikatorProgram,
                    kegiatanData
                }
                return programR
            })).then((iteks) => {
                programInstrumen.push(iteks)
            })

            const programInstrumens = programInstrumen[0]
            const sasaranR = {
                idInstrumen : ite.idInstrumen,
                parent : ite.parent,
                sasaran : ite.tujuan,
                kode : await getKode(ite.idInstrumen,'sasaran'),
                indikatorSasaran,
                programInstrumens
            }
            return sasaranR
        })).then((itek) => {
            sasaranInstrumen.push(itek)
        })
        const sasaranInstrumens = sasaranInstrumen[0]
        const parser = {
            idInstrumen : item.idInstrumen,
            tahun : item.periode,
            tujuan : item.tujuan,
            kode : await getKode(item.idInstrumen,'tujuan'),
            indikator,
            sasaranInstrumens
        }
        result.push(parser)
    }))
    return result

}

const listTujuan = async (req)=> {
    const tujuan = await prisma.instrumen.findMany({
        where : {
            periode: parseInt(req.params.tahun),
            type : 'tujuan',
            opd : parseInt(req.params.opd)
        },
        select : {
            idInstrumen : true,
            tujuan : true
        }
    })
    if(tujuan.length < 1){
        throw new errorHandling(404,"Data yang di cari tidak ada")
    }

    return tujuan
}

const updateTujuan = async (req) => {
    const bodyAllow = Object.keys(req.body)
    bodyAllow.forEach(element => {
        if(element === 'nama' ||element === 'kode' ||element === 'type' || element === 'indikator'||element === 'idInstrumen'||element === 'tahun'||element === 'opd'||element === 'paguIndikatif'){
        }else{
            throw new errorHandling(401,'Parameter salah silahakn cek body request')
        }

    });
    const dataExist = await prisma.indikatorInstrumen.findFirst({
        where : {
            idInstrumen : parseInt(req.body.idInstrumen)
        }
     })

    if(!dataExist){
        throw new errorHandling(404,"Data tidak di temukan pada sistem")
    }
    const getIndikator = await prisma.indikatorInstrumen.findMany({
        where : {
            idInstrumen : parseInt(req.body.idInstrumen)
        }
    })

    await Promise.all(getIndikator.map(async (item)=>{
        await prisma.indikatorInstrumen.delete({
            where : {
                idIndikator : parseInt(item.idIndikator)
            }
        })
    })).then(()=>
        req.body.indikator.map(async (indi) => {
            await prisma.indikatorInstrumen.create({
                data : {
                    idInstrumen : parseInt(req.body.idInstrumen),
                    indikator_kinerja : indi.namaIndikator.toString(),
                    data_capaian_awal : indi.DataCapaianAwal.toString(),
                    target_akhir_periode : indi.TargetAkhirPeriode.toString(),
                    satuan : indi.satuan.toString(),
                    target : parseInt(indi.target),
                }
            })
        })
    )

    const update = await prisma.instrumen.update({
        data : {
            tujuan : req.body.nama.toString(),
            opd : req.body.opd ? parseInt(req.body.opd) : null,
            paguIndikatif : req.body.paguIndikatif ? req.body.paguIndikatif : null
        },
        where : {
            idInstrumen : parseInt(req.body.idInstrumen)
        }
    })

    const result = []
    await Promise.all([update]).then(async (data)=>{
        const cekKode = await prisma.kodeInstrumen.findMany({
            where : {
                idIstrumen : parseInt(req.body.idInstrumen),
                type : req.body.type.toString()
            }
        })
        if(cekKode.length > 0){
            await prisma.kodeInstrumen.updateMany({
                data : {
                    kode : req.body.kode ? req.body.kode.toString() : null 
                },
                where : {
                    idIstrumen : parseInt(data[0].idInstrumen),
                    type : req.body.type.toString()
                }
            })
        }else{
            await prisma.kodeInstrumen.create({
                data : {
                    idIstrumen : parseInt(data[0].idInstrumen),
                    type : req.body.type.toString(),
                    kode : req.body.kode ? req.body.kode.toString() : null 
                }
            })
        }
        const indikator = await prisma.indikatorInstrumen.findMany({
            where : {
                idInstrumen : parseInt(data[0].idInstrumen)
            },
            select : {
                idIndikator : true,
                indikator_kinerja : true,
                data_capaian_awal : true,
                target_akhir_periode : true,
                satuan : true,
                target : true
            }
        })
        const parser = {
            idInsrumen : data[0].idInstrumen,
            Nama : data[0].tujuan,
            type : data[0].type,
            kode : req.body.kode,
            indikator
        }

        result.push(parser)
    })

    await prisma.logAktivitas.create({
        data : {
            userId : parseInt(req.user.userId),
            data : parseInt(update.idInstrumen),
            dateCreate : new Date(),
            aktiviti : "edit"
        }
    })

    return result
}

const getname = async (id)=>{
    const name = await prisma.namaUser.findFirst({
        where : {
            userId : id.toString()
        },
        select : {
            namaUser : true
        }
    })

    if(!name){
        return "NoName"
    }else{
        return name.namaUser
    }
}

const getTujuanByIdSubUrusan = async (req)=>{
    const bodyAllow = Object.keys(req.params)
    bodyAllow.forEach(element => {
        if(element === 'opd' || element === 'tahun' ){
        }else{
            throw new errorHandling(401,'Parameter salah silahakn cek body request')
        }

    });
    const fetchTujuan = await prisma.instrumen.findMany({
        where : {
            type : 'tujuan',
            opd : parseInt(req.params.opd),
            periode : parseInt(req.params.tahun)
        }
    })
    if(fetchTujuan.length < 1){
        throw new errorHandling(404,"Data yang di cari tidak ada")
    }

    const result = []
    await Promise.all(fetchTujuan.map(async (item)=>{
        const parse = {
            idSubUrusan : item.subUrusan,
            kodeSubUrusan : await getKode(item.subUrusan,'subUrusan'),
            idInstrumen : item.idInstrumen,
            kodeTujuan : await getKode(item.idInstrumen,'tujuan'),
            namaTujuan : item.tujuan
        }
        return parse
    })).then((items)=>{
        result.push(items)
    })

    return result[0]


}

async function getParentId(id,type) {
    const result = await prisma.instrumen.findFirst({
        where : {
            idInstrumen : parseInt(id),
            type : type.toString()
        }
    })

    return result ? result.parent : 0 
}

const getSasaranByIdTujuan = async (req)=>{
    const bodyAllow = Object.keys(req.params)
    bodyAllow.forEach(element => {
        if(element === 'opd' || element === 'tahun'){
        }else{
            throw new errorHandling(401,'Parameter salah silahakn cek body request')
        }

    });
    const fetchDataSasaran = await prisma.instrumen.findMany({
        where :{
            type : 'sasaran',
            opd : parseInt(req.params.opd),
            periode : parseInt(req.params.tahun)
        }
    })

    if(fetchDataSasaran.length < 1){
        throw new errorHandling(404,"Data yang di cari tidak ada")
    }
    const result = []
    await Promise.all(fetchDataSasaran.map(async (item)=>{
        const parse = {
            kodeUrusan: await getKode(item.urusan,'urusan'),
            idSubUrusan : item.subUrusan,
            kodeSubUrusan : await getKode(item.subUrusan,'subUrusan'),
            idTujuan : item.parent,
            kodeTujuan : await getKode(item.parent,'tujuan'),
            idInstrumen : item.idInstrumen,
            kodeSasaran : await getKode(item.idInstrumen,'sasaran'),
            namaSasaran : item.tujuan
        }
        return parse
    })).then((items)=>{
        result.push(items)
    })

    return result[0]
}

const getProgramByIdSasaran = async (req)=>{
    const bodyAllow = Object.keys(req.params)
    bodyAllow.forEach(element => {
        if(element === 'opd' || element === 'tahun'){
        }else{
            throw new errorHandling(401,'Parameter salah silahakn cek body request')
        }

    });

    const fetchProgram = await prisma.instrumen.findMany({
        where : {
            type : 'program',
            opd : parseInt(req.params.opd),
            periode : parseInt(req.params.tahun)
        }
    })
    if(fetchProgram.length < 1){
        throw new errorHandling(404,"Data yang di cari tidak ada")
    }
    const result =[]
    await Promise.all(fetchProgram.map(async (item)=>{
        const idTujuans = await getParentId(item.parent, 'sasaran')
        const parse = {
            kodeUrusan : await getKode(item.urusan,'urusan'),
            idSubUrusan : item.subUrusan,
            kodeSubUrusan : await getKode(item.subUrusan,'subUrusan'),
            idTujuan : idTujuans,
            kodeTujuan : await getKode(idTujuans,'tujuan'),
            idSasaran : item.parent,
            kodeSasaran : await getKode(item.parent,'sasaran'),
            idInstrumen : item.idInstrumen,
            kodeProgram : await getKode(item.idInstrumen,'program'),
            namaProgram : item.tujuan,
        }
        return parse
    })).then((items)=>{
        result.push(items)
    })

    return result[0]
}

const getKegiatanByIdProgram = async (req)=>{
    const bodyAllow = Object.keys(req.params)
    bodyAllow.forEach(element => {
        if(element === 'opd' || element === 'tahun'){
        }else{
            throw new errorHandling(401,'Parameter salah silahakn cek body request')
        }

    });

    const fetchKegiatan = await prisma.instrumen.findMany({
        where : {
          type : 'kegiatan',
          opd : parseInt(req.params.opd),
          periode : parseInt(req.params.tahun)
        }
    })
    if(fetchKegiatan.length < 1){
        throw new errorHandling(404,"Data yang di cari tidak ada")
    }
    const result =[]
    await Promise.all(fetchKegiatan.map(async (item)=>{
        const idSasarans = await getParentId(item.parent,'program')
        const idTujuans = await getParentId(idSasarans,'sasaran')
        const parse = {
            kodeUrusan : await getKode(item.urusan,'urusan'),
            idSubUrusan : item.subUrusan,
            kodeSubUrusan : await getKode(item.subUrusan,'subUrusan'),
            idTujuan : idTujuans,
            kodeTujuan : await getKode(idTujuans,'tujuan'),
            idSasaran : idSasarans,
            kodeSasaran : await getKode(idSasarans,'sasaran'),
            idProgram : item.parent,
            kodeProgram : await getKode(item.parent,'program'),
            idInstrumen : item.idInstrumen,
            kodeKegiatan : await getKode(item.idInstrumen,'kegiatan'),
            namaKegiatan : item.tujuan,
        }
        return parse
    })).then((items)=>{
        result.push(items)
    })

    return result[0]
}

const getSubByIdKegiatan = async (req)=>{
    const bodyAllow = Object.keys(req.params)
    bodyAllow.forEach(element => {
        if(element === 'opd' || element === 'tahun'){
        }else{
            throw new errorHandling(401,'Parameter salah silahakn cek body request')
        }

    });

    const fetchSub = await prisma.instrumen.findMany({
        where : {
          type : 'subKegiatan',
          opd : parseInt(req.params.opd),
          periode : parseInt(req.params.tahun)
        }
    })
    if(fetchSub.length < 1){
        throw new errorHandling(404,"Data yang di cari tidak ada")
    }
    const result =[]
    await Promise.all(fetchSub.map(async (item)=>{
        const idPrograms = await getParentId(item.parent,'kegiatan')
        const idSasarans = await getParentId(idPrograms,'program')
        const idTujuans = await getParentId(idSasarans,'sasaran')
        const parse = {
            kodeUrusan : await getKode(item.urusan,'urusan'),
            idSubUrusan : item.subUrusan,
            kodeSubUrusan : await getKode(item.subUrusan,'subUrusan'),
            idTujuan : idTujuans,
            kodeTujuan : await getKode(idTujuans,'tujuan'),
            idSasaran : idSasarans,
            kodeSasaran : await getKode(idSasarans,'sasaran'),
            idProgram : idPrograms,
            kodeProgram : await getKode(idPrograms,'program'),
            idKegiatan : item.parent,
            kodeKegiatan : await getKode(item.parent,'kegiatan'),
            idInstrumen : item.idInstrumen,
            kodeSubKegiatan : await getKode(item.idInstrumen,'subKegiatan'),
            namaSubKegiatan : item.tujuan,
        }
        return parse
    })).then((items)=>{
        result.push(items)
    })

    return result[0]
}


const logInstrumen = async (req)=>{
    const bodyAllow = Object.keys(req.params)
    bodyAllow.forEach(element => {
        if(element === 'idData'){
        }else{
            throw new errorHandling(401,'Parameter salah silahakn cek body request')
        }

    });

    const data = await prisma.logAktivitas.findMany({
        where:{
            data : parseInt(req.params.idData)
        }
    })

    // console.log(data)

    if(data.length < 1){
        throw new errorHandling(404,"Data Log tidak ditemukan")
    }
    const result = []
    await Promise.all(data.map(async (item) => {
        const dat = new Date(item.dateCreate)
        const tanggal = dates.format(dat,'YYYY-MM-DD HH:mm:ss')
        const nama = await getname(item.userId)
        const push = {
            idLog : item.idLog,
            idUser : item.userId,
            NamaUser : nama,
            aktivitas : item.aktiviti,
            tanggal : tanggal
        }
        return push
    })).then((is) => {
        result.push(is)
    })
    return result[0]
}


async function hapusInstrumen(req) {
    const bodyAllow = Object.keys(req.params)
    bodyAllow.forEach(element => {
        if(element === 'type' || element === 'id'){
        }else{
            throw new errorHandling(401,'Parameter salah silahakn cek body request')
        }
    });

    async function cekInstumen(id,type) {
        const cekData = await prisma.instrumen.findFirst({
            where:{
                idInstrumen : parseInt(id),
                type : type.toString()
            }
        })
        return cekData
    }

    //delete instrumen, indikator dan kodeintrumen function
    async function deleteInstrumen(id,types){
        if(types === 'tujuan'){
            const delinstrumen = await prisma.instrumen.deleteMany({
                where  : {
                    idInstrumen : parseInt(id),
                    type : types.toString()
                }
            })

            await Promise.all([deleteInstrumen]).then(async()=>{
                await prisma.indikatorInstrumen.deleteMany({
                    where : {
                        idInstrumen : parseInt(id)
                    }
                })
            }).then(async()=>{
                await prisma.kodeInstrumen.deleteMany({
                    where : {
                        idIstrumen : parseInt(id),
                        type : types.toString()
                    }
                })
            })
            return
        }else{
            const delInstrumen = await prisma.instrumen.deleteMany({
                where : {
                    idInstrumen: {
                        in : id
                    },
                    type : types.toString()
                }
            })

            await Promise.all([delInstrumen]).then(async()=>{
                await prisma.indikatorInstrumen.deleteMany({
                    where : {
                        idInstrumen : {
                            in : id
                        }
                    }
                })
            }).then(async()=>{
                await prisma.kodeInstrumen.deleteMany({
                    where : {
                        idIstrumen : {
                            in : id
                        },
                        type : types.toString()
                    }
                })
            })  
        }

        
        return
    }

    if(req.params.type === 'tujuan'){
        const cekTujuan = await cekInstumen(req.params.id,req.params.type)
        if(!cekTujuan){
            throw new errorHandling(404,'Data Tujuan Tidak Di Temukan')
        }
        const allId = []
        const getIdSasaran = await prisma.instrumen.findMany({
            where : {
                parent : parseInt(req.params.id),
                type : 'sasaran'
            }
        })
        await Promise.all([getIdSasaran]).then(async(item)=>{
            const arrayPush = item[0].map((id)=>{
                return id.idInstrumen
            })
            const getIdProgram = await prisma.instrumen.findMany({
                where : {
                    parent :{
                        in : arrayPush
                    },
                    type : 'program'
                }
            })
            const sasaranParse = {
                sasaranId : arrayPush ? arrayPush : 0
            }
            allId.push(sasaranParse)
            return getIdProgram
        }).then(async(program)=>{
            const arrayPush = program.map((id)=>{
                return id.idInstrumen
            })

            const getIdKegiatan = await prisma.instrumen.findMany({
                where : {
                    parent : {
                        in : arrayPush
                    },
                    type : 'kegiatan'
                }
            })
            const programParse = {
                programId : arrayPush ? arrayPush : 0
            }
            allId.push(programParse)
            return getIdKegiatan
        }).then(async(kegiatan)=>{
            const arrayPush = kegiatan.map((id)=>{
                return id.idInstrumen
            })
            const getIdSub = await prisma.instrumen.findMany({
                where : {
                    parent : {
                        in : arrayPush
                    },
                    type : 'subKegiatan'
                }
            })
            const kegiatanParse = {
                kegiatanId : arrayPush ? arrayPush : 0
            }
            allId.push(kegiatanParse)
            return getIdSub
        }).then(async(sub)=>{
           
            const arrayPush = sub.map((id)=>{
                return id.idInstrumen
            })
            const subParse = {
                subId : arrayPush ? arrayPush : 0
            }
            allId.push(subParse)
            return
        }).then(async ()=>{
            //delete tujuan 
            const dell = await deleteInstrumen(req.params.id,'tujuan')
            await Promise.all([dell]).then(async()=>{
                //delete sasaran
                if(allId[0].sasaranId){
                    deleteInstrumen(allId[0].sasaranId,'sasaran')
                }
            }).then(async()=>{
                // delete program
                if(allId[1].programId){
                    deleteInstrumen(allId[1].programId,'program')
                }
            }).then(async()=>{
                // delete kegiatan
                if(allId[2].kegiatanId){
                    deleteInstrumen(allId[2].kegiatanId,'kegiatan')
                }
            }).then(async()=>{
                //delete subKegiatan
                if(allId[3].subId){
                    deleteInstrumen(allId[3].subId,'subKegiatan')
                }
            })
        
        })  

        return
    }else if(req.params.type === 'sasaran'){
        const cekSasaran = await cekInstumen(req.params.id,req.params.type)
        if(!cekSasaran){
            throw new errorHandling(404,'Data Sasaran Tidak Di Temukan')
        }
        const allId = []
        const getIdProgram = await prisma.instrumen.findMany({
            where : {
                parent : parseInt(req.params.id),
                type : 'program'
            }
        })
        await Promise.all([getIdProgram]).then(async(program)=>{
            const arrayPush = program[0].map((id)=>{
                return id.idInstrumen
            })

            const getIdKegiatan = await prisma.instrumen.findMany({
                where : {
                    parent : {
                        in : arrayPush
                    },
                    type : 'kegiatan'
                }
            })
            const programParse = {
                programId : arrayPush ? arrayPush : 0
            }
            allId.push(programParse)
            return getIdKegiatan
        }).then(async(kegiatan)=>{
            const arrayPush = kegiatan.map((id)=>{
                return id.idInstrumen
            })
            const getIdSub = await prisma.instrumen.findMany({
                where : {
                    parent : {
                        in : arrayPush
                    },
                    type : 'subKegiatan'
                }
            })
            const kegiatanParse = {
                kegiatanId : arrayPush ? arrayPush : 0
            }
            allId.push(kegiatanParse)
            return getIdSub
        }).then(async(sub)=>{
            const arrayPush = sub.map((id)=>{
                return id.idInstrumen
            })
            const subParse = {
                subId : arrayPush ? arrayPush : 0
            }
            allId.push(subParse)
            return
        }).then(()=>{

            //delete sasaran
            deleteInstrumen([parseInt(req.params.id)],'sasaran')

            // delete program
            if(allId[0].programId){
                deleteInstrumen(allId[0].programId,'program')
            }
            
            // delete kegiatan
            if(allId[1].kegiatanId){
                deleteInstrumen(allId[1].kegiatanId,'kegiatan')
            }
            //delete subKegiatan
            if(allId[2].subId){
                deleteInstrumen(allId[2].subId,'subKegiatan')
            }
        })
        return
    }else if(req.params.type === 'program'){
        const cekProgram = await cekInstumen(req.params.id,req.params.type)
        if(!cekProgram){
            throw new errorHandling(404,'Data Program Tidak Di Temukan')
        }
        const allId = []
        const getIdKegiatan = await prisma.instrumen.findMany({
            where : {
                parent : parseInt(req.params.id),
                type : 'kegiatan'
            }
        })
        await Promise.all([getIdKegiatan]).then(async(kegiatan)=>{
            const arrayPush = kegiatan[0].map((id)=>{
                return id.idInstrumen
            })
            const getIdSub = await prisma.instrumen.findMany({
                where : {
                    parent : {
                        in : arrayPush
                    },
                    type : 'subKegiatan'
                }
            })
            const kegiatanParse = {
                kegiatanId : arrayPush ? arrayPush : 0
            }
            allId.push(kegiatanParse)
            return getIdSub
        }).then(async(sub)=>{
            const arrayPush = sub.map((id)=>{
                return id.idInstrumen
            })
            const subParse = {
                subId : arrayPush ? arrayPush : 0
            }
            allId.push(subParse)
            return
        }).then(async ()=>{
            // delete program
            const d = await deleteInstrumen([parseInt(req.params.id)],'program')

            await Promise.all([d]).then(async()=>{
                // delete kegiatan
                if(allId[0].kegiatanId){
                    deleteInstrumen(allId[0].kegiatanId,'kegiatan')
                }
            }).then(async()=>{
                //delete subKegiatan
                if(allId[1].subId){
                    deleteInstrumen(allId[1].subId,'subKegiatan')
                }
            })
            

            
        })
        return
    }else if(req.params.type === 'kegiatan'){
        const cekKegiatan = await cekInstumen(req.params.id,req.params.type)
        if(!cekKegiatan){
            throw new errorHandling(404,'Data Kegiatan Tidak Di Temukan')
        }
        const allId = []
        const getIdSubKegiatan = await prisma.instrumen.findMany({
            where : {
                parent : parseInt(req.params.id),
                type : 'subKegiatan'
            }
        })
        await Promise.all([getIdSubKegiatan]).then(async(sub)=>{
            const arrayPush = sub[0].map((id)=>{
                return id.idInstrumen
            })
            const subParse = {
                subId : arrayPush ? arrayPush : 0
            }
            allId.push(subParse)
            return
        }).then(()=>{
            // delete kegiatan
            deleteInstrumen([parseInt(req.params.id)],'kegiatan')
            
             //delete subKegiatan
            if(allId[0].subId){
                deleteInstrumen(allId[0].subId,'subKegiatan')
            }
        })
        return
    }else if(req.params.type === 'subKegiatan'){
        const cekSub = await cekInstumen(req.params.id,req.params.type)
        if(!cekSub){
            throw new errorHandling(404,'Data Sub Kegiatan Tidak Di Temukan')
        }
        deleteInstrumen([parseInt(req.params.id)],req.params.type)
        return
    }

    return
}

export {
    hapusInstrumen,
    inputEval,
    listEval,
    listTujuan,
    updateTujuan,
    logInstrumen,
    getTujuanByIdSubUrusan,
    getSasaranByIdTujuan,
    getProgramByIdSasaran,
    getKegiatanByIdProgram,
    getSubByIdKegiatan
}