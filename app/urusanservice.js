import { PrismaClient } from '@prisma/client'
import { errorHandling } from '../middlewares/errorHandler.js'
import dates from "date-and-time"

const prisma = new PrismaClient()
process.env.TZ = 'Asia/Jakarta'

const cekUrusan = async (id) => {
    return await prisma.urusan.findFirst({
        where : {
            idUrusan : parseInt(id)
        }
    })
}

async function getKode(id,type) {
    const result = await prisma.kodeInstrumen.findMany({
        where : {
            idIstrumen : parseInt(id),
            type : type.toString()
        }
    })
    const retur = result[0] ? result[0].kode : null
    return retur
}

const addUrusan = async (req) => {
    const admin = req.user.userId

    const create = await prisma.urusan.create({
        data : {
            namaUrusan : req.body.namaUrusan,
            createDate : new Date(),
            idCreate : parseInt(admin)
        }
    })

    const result = []

    await Promise.all([create]).then(async (item) => {
        const cekKode = await prisma.kodeInstrumen.findMany({
            where : {
                idIstrumen : parseInt(item[0].idUrusan),
                type : 'urusan'
            }
        })
        if(cekKode.length > 0){
            await prisma.kodeInstrumen.updateMany({
                data : {
                    kode : req.body.kode ? req.body.kode.toString() : null 
                },
                where : {
                    idIstrumen : parseInt(item[0].idUrusan),
                    type : 'urusan'
                }
            })
        }else{
            await prisma.kodeInstrumen.create({
                data : {
                    idIstrumen : parseInt(item[0].idUrusan),
                    type : 'urusan',
                    kode : req.body.kode ? req.body.kode.toString() : null 
                }
            })
        }
        const rekon = {
            idUrusan : item[0].idUrusan,
            namaUrusan : item[0].namaUrusan,
            kode : req.body.kode ? req.body.kode.toString() : null
        }
        result.push(rekon)
    })

    return result
}

const updateUrusan = async (req) => {
    const bodyAllow = Object.keys(req.body)
    bodyAllow.forEach(element => {
        if(element === 'idUrusan' || element === 'namaUrusan' || element === 'kode'){
        }else{
            throw new errorHandling(401,'Parameter salah silahakn cek body request')
        }

    });
    const idExist = bodyAllow.filter(item => item === 'idUrusan');
    if(idExist.length < 1){
        throw new errorHandling(404, "Tidak dapat menemukan parameter ID Urusan")
    }

    const namaExist = bodyAllow.filter(item => item === 'namaUrusan');
    if(namaExist.length < 1){
        throw new errorHandling(404, "Tidak dapat menemukan parameter namaUrusan")
    }else{
        const cek = await cekUrusan(req.body.idUrusan)
        if(!cek){
            throw new errorHandling(404, "Data urusan tidak ditemukan")
        }
    
        const update = await prisma.urusan.update({
            data: {
                namaUrusan : req.body.namaUrusan
            },
            where : {
                idUrusan : parseInt(req.body.idUrusan)
            }
        })

        await Promise.all([update]).then(async (item)=>{
            const cekKode = await prisma.kodeInstrumen.findMany({
                where : {
                    idIstrumen : parseInt(req.body.idUrusan),
                    type : 'urusan'
                }
            })
            if(cekKode.length > 0){
                await prisma.kodeInstrumen.updateMany({
                    data : {
                        kode : req.body.kode ? req.body.kode.toString() : null 
                    },
                    where : {
                        idIstrumen : parseInt(req.body.idUrusan),
                        type : 'urusan'
                    }
                })
            }else{
                await prisma.kodeInstrumen.create({
                    data : {
                        idIstrumen : parseInt(req.body.idUrusan),
                        type : 'urusan',
                        kode : req.body.kode ? req.body.kode.toString() : null 
                    }
                })
            }
        })
        return
    }
        
}

const hapusUrusan = async (req) => {
    const bodyAllow = Object.keys(req.params)
    bodyAllow.forEach(element => {
        if(element === 'id'){
        }else{
            throw new errorHandling(401,'Parameter salah silahakn cek body request')
        }

    });
    const idExist = bodyAllow.filter(item => item === 'id');
    if(idExist.length < 1){
        throw new errorHandling(404, "Tidak dapat menemukan parameter ID untuk di hapus")
    }

    const cek = await cekUrusan(req.params.id)
    if(!cek){
        throw new errorHandling(404, "Data urusan tidak ditemukan")
    }

    const getDataSub = await prisma.subUrusan.findMany({
        where : {
            urusan : parseInt(req.params.id)
        }
    })

    await Promise.all( getDataSub.map( async (item) => {
        await prisma.subUrusan.delete({
            where : {
                idSub : item.idSub
            }
        })
        return "panic"
    })).then(async ()=>{
        await prisma.urusan.delete({
            where : {
                idUrusan : parseInt(req.params.id)
            }
        })

        await prisma.kodeInstrumen.deleteMany({
            where :{
                idIstrumen : parseInt(req.params.id),
                type : 'urusan'
            }
        })
    })
    return

}

const listAll = async (req) => {
    const bodyAllow = Object.keys(req.params)
    bodyAllow.forEach(element => {
        if(element === 'page'){
        }else{
            throw new errorHandling(401,'Parameter salah silahakn cek body request')
        }

    });
    const countData = await prisma.urusan.aggregate({
        _count: {
            idUrusan : true
        }
    })
    const JumlahPage = Math.ceil(parseInt(countData._count.idUrusan)/20)
    if(req.params.page > JumlahPage) {
        throw new errorHandling(401,'Tidak ada data pada page yang di pilih')
    }
    const skipUrusanList = (req.params.page == 1) ? 0 : (req.params.page - 1) * 20
    
    const fetch = await prisma.urusan.findMany({
        skip : parseInt(skipUrusanList),
        take : 20,
        orderBy : {
            idUrusan : 'desc'
        }
    })

  const dataUrusan = []
  await Promise.all(fetch.map(async(item) => {
    const getNameAdmin = await prisma.users.findFirst({
        where : {
            userId : parseInt(item.idCreate)
        }
    })
    const dataUrusans = []
    await Promise.all([getNameAdmin]).then(async (it) => {
        const dataJadi = {
            idUrusan : item.idUrusan,
            namaUrusan : item.namaUrusan,
            usernameAdmin : it[0] ? it[0].username : null,
            idAdmin : item.idCreate,
            kode : await getKode(item.idUrusan,'urusan')
        }
        dataUrusans.push(dataJadi)
    })
    dataUrusan.push(dataUrusans[0])
  }))
  const dataAkhir = {
    jumlahPage : JumlahPage,
    jumlahData : countData._count.idUrusan,
    statusPage : req.params.page,
    dataUrusan
 }
  return dataAkhir

}

const singleFetch = async (req) => {
    const bodyAllow = Object.keys(req.params)
    bodyAllow.forEach(element => {
        if(element === 'id'){
        }else{
            throw new errorHandling(401,'Parameter salah silahakn cek body request')
        }

    });
    const idExist = bodyAllow.filter(item => item === 'id');
    if(idExist.length < 1){
        throw new errorHandling(404, "Tidak dapat menemukan parameter ID")
    }

    const cek = await cekUrusan(req.params.id)
    if(!cek){
        throw new errorHandling(404, "Data urusan tidak ditemukan")
    }

    const fetch = await prisma.$queryRaw`
    SELECT *
    FROM urusan
    LEFT JOIN users ON urusan.idCreate = users.userId
    WHERE urusan.idUrusan = ${req.params.id}
  `;

    const result = []
    await Promise.all(fetch.map(async (item) => {
        const dataJadi = {
            "idUrusan" : item.idUrusan,
            "namaUrusan" : item.namaUrusan,
            "usernameAdmin" : item.username,
            "idAdmin" : item.userId,
            "kode" : await getKode(item.idUrusan,'urusan')
        }
        result.push(dataJadi)
    }))
    return result

}

const filterByName = async (req) => {
    const bodyAllow = Object.keys(req.params)
    bodyAllow.forEach(element => {
        if(element === 'nama'){
        }else{
            throw new errorHandling(401,'Parameter salah silahakn cek body request')
        }

    });
    const idExist = bodyAllow.filter(item => item === 'nama');
    if(idExist.length < 1){
        throw new errorHandling(404, "Tidak dapat menemukan parameter ID")
    }
    const fetch = await prisma.urusan.findMany({
        where : {
            namaUrusan : {
                contains : req.params.nama.toString()
            }
        }
    })

    const result = []
    await Promise.all(fetch.map(async (item) => {
        const id =  await prisma.users.findFirst({
        where :{
            userId : parseInt(item.idCreate)
        }
       })

       const dataJadi = {
        "idUrusan" : item.idUrusan,
        "namaUrusan" : item.namaUrusan,
        "usernameAdmin" : id.username,
        "idAdmin" : id.userId,
        "kode" : await getKode(item.idUrusan,'urusan')
       }
       result.push(dataJadi)
    }))
    return result

}

const hapusUraian = async (req) => {
    const bodyAllow = Object.keys(req.params)
    bodyAllow.forEach(element => {
        if (element !== 'id') {
            throw new errorHandling(401, 'Parameter salah silahakn cek body request')
        }
    });

    const cekUraian = await prisma.uraianRenja.findFirst({
        where: {
            idUraian: parseInt(req.params.id)
        }
    });

    if (!cekUraian) {
        throw new errorHandling(404, 'Data Uraian Tidak Di Temukan')
    }
    const getRekapExist = await prisma.targetTriwulan.findFirst({
        where: {
            idSubKegiatan: parseInt(cekUraian.idSubKegiatan),
            tahun: parseInt(cekUraian.tahun),
            opd: parseInt(cekUraian.opd)
        }
    });

    const deleteUraian = await prisma.uraianRenja.delete({
        where: {
            idUraian: parseInt(req.params.id)
        }
    });

    const deleteTwUraian = await prisma.uraianTriwulan.deleteMany({
        where: {
            idUraian: parseInt(req.params.id)
        }
    });

    if (!deleteUraian && !deleteTwUraian) {
        throw new errorHandling(400, 'Gagal menghapus data uraian');
    }

    const totalPersenTw1 = await totalPersenTriwulan(cekUraian.idSubKegiatan, 1)
    const totalPersenTw2 = await totalPersenTriwulan(cekUraian.idSubKegiatan, 2)
    const totalPersenTw3 = await totalPersenTriwulan(cekUraian.idSubKegiatan, 3)
    const totalPersenTw4 = await totalPersenTriwulan(cekUraian.idSubKegiatan, 4)
    await Promise.all([totalPersenTw1, totalPersenTw2, totalPersenTw3, totalPersenTw4]).then(async (item) => {
        await prisma.targetTriwulan.update({
            data: {
                t1: item[0].t1 ? item[0].t1 : 0,
                t2: item[1].t2 ? item[1].t2 : 0,
                t3: item[2].t3 ? item[2].t3 : 0,
                t4: item[3].t4 ? item[3].t4 : 0
            },
            where: {
                idTriwulan: parseInt(getRekapExist.idTriwulan)
            }
        })
    })

    return;
}

export {
    addUrusan,
    updateUrusan,
    hapusUrusan,
    listAll,
    singleFetch,
    filterByName
}