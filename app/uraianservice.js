import { PrismaClient } from "@prisma/client";
import { errorHandling } from '../middlewares/errorHandler.js'

const prisma = new PrismaClient()

const cekSubKegiatan = async (idSub) => {
    return await prisma.instrumen.findFirst({
        where : {
            idInstrumen : parseInt(idSub),
            type : "subKegiatan"
        }
    })
}

const cekUraian = async (idUraian)=>{
    return await prisma.uraianRenja.findFirst({
        where :{
            idUraian : parseInt(idUraian)
        }
    })
}

const totalPersenTriwulan = async(idSub,triwulan)=>{
        const getAllTriwulan = await prisma.uraianTriwulan.findMany({
            where : {
                idSubKegiatan : parseInt(idSub),
                triwulan : parseInt(triwulan)
            }
        })
        const totalCapaians = getAllTriwulan.reduce((total, item) => {
            if (item.targetPersen !== null && item.targetPersen !== undefined) {
            total += parseInt(item.targetPersen, 10);
            }
            return total;
        }, 0);
        
        // Menghitung jumlah elemen yang tidak null
        const countNonNull = getAllTriwulan.reduce((count, item) => {
            if (item.targetPersen !== null && item.targetPersen !== undefined) {
            count += 1;
            }
            return count;
        }, 0);
        
        // Menghitung rata-rata
        const averageTarget = countNonNull > 0 ? totalCapaians / countNonNull : 0;
        if(triwulan === 1){
            const data = {
                t1 : averageTarget
            }
            return data
        }else if(triwulan === 2){
            const data = {
                t2 : averageTarget
            }
            return data
        }else if(triwulan === 3){
            const data = {
                t3 : averageTarget
            }
            return data
        }else if(triwulan === 4){
            const data = {
                t4 : averageTarget
            }
            return data
        }
        
}


const inputUraian = async (req)=>{
    const bodyAllow = Object.keys(req.body)
    bodyAllow.forEach(element => {
        if(element === 'idSubKegiatan'||element === 'namaUraian'||element === 'targetVolume'||element === 'tahun'||element === 'opd'||element === 'satuan'||element === 'targetTriwulan'){
        }else{
            throw new errorHandling(401,'Parameter salah silahakn cek body request')
        }

    });
    const subExist = await cekSubKegiatan(req.body.idSubKegiatan)
    if(!subExist){
        throw new errorHandling(404,"Data Sub Kegiatan tidak di temukan")
    }

    const create = await prisma.uraianRenja.create({
        data : {
            tahun : parseInt(req.body.tahun),
            opd : parseInt(req.body.opd),
            idSubKegiatan : parseInt(req.body.idSubKegiatan),
            namaUraian : req.body.namaUraian.toString(),
            satuan : req.body.satuan.toString(),
            targetVolume : parseInt(req.body.targetVolume)
        }
    })

    await Promise.all([create]).then(async(item)=>{
        await Promise.all(req.body.targetTriwulan.map(async(items) =>{
            await prisma.uraianTriwulan.create({
                data:{
                    idUraian : parseInt(item[0].idUraian),
                    idSubKegiatan : parseInt(req.body.idSubKegiatan),
                    triwulan : parseInt(items.triwulan),
                    targetVolume : parseInt(items.targetVolume),
                    targetPersen : items.targetPersen.toString()
                }
            })
        })).then(async()=>{
            const totalPersenTw1 = await totalPersenTriwulan(req.body.idSubKegiatan,1)
            const totalPersenTw2= await totalPersenTriwulan(req.body.idSubKegiatan,2)
            const totalPersenTw3 = await totalPersenTriwulan(req.body.idSubKegiatan,3)
            const totalPersenTw4 = await totalPersenTriwulan(req.body.idSubKegiatan,4)
            await Promise.all([totalPersenTw1,totalPersenTw2,totalPersenTw3,totalPersenTw4]).then(async (item) => {
                const getRekapExist = await prisma.targetTriwulan.findFirst({
                    where : {
                        idSubKegiatan : parseInt(req.body.idSubKegiatan),
                        tahun : parseInt(req.body.tahun),
                        opd : parseInt(req.body.opd)
                    }
                })

                if(!getRekapExist){
                    await prisma.targetTriwulan.create({
                        data : {
                            idSubKegiatan : parseInt(req.body.idSubKegiatan),
                            tahun : parseInt(req.body.tahun),
                            opd : parseInt(req.body.opd),
                            idUser : parseInt(req.user.userId),
                            dateCreate : new Date(),
                            t1 : item[0].t1 ? item[0].t1: 0,
                            t2 : item[1].t2 ? item[1].t2: 0,
                            t3 : item[2].t3 ? item[2].t3: 0,
                            t4 : item[3].t4 ? item[3].t4: 0
                        }
                    })
                }else{
                    await prisma.targetTriwulan.updateMany({
                        data : {
                            t1 : item[0].t1 ? item[0].t1: 0,
                            t2 : item[1].t2 ? item[1].t2: 0,
                            t3 : item[2].t3 ? item[2].t3: 0,
                            t4 : item[3].t4 ? item[3].t4: 0
                        },
                        where : {
                            idTriwulan : parseInt(getRekapExist.idTriwulan)
                        }
                    })
                }
            })
        })
    } )

    return create
}

const updateUraian = async (req)=>{
    const bodyAllow = Object.keys(req.body)
    bodyAllow.forEach(element => {
        if(element === 'idUraian'||element === 'namaUraian'||element === 'targetVolume'||element === 'tahun'||element === 'opd'||element === 'satuan'||element === 'targetTriwulan'){
        }else{
            throw new errorHandling(401,'Parameter salah silahakn cek body request')
        }

    });
    const uraianExist = await cekUraian(req.body.idUraian)
    if(!uraianExist){
        throw new errorHandling(404,"Data Sub Kegiatan tidak di temukan")
    }

    const update = await prisma.uraianRenja.update({
        data : {
            tahun : parseInt(req.body.tahun),
            opd : parseInt(req.body.opd),
            namaUraian : req.body.namaUraian.toString(),
            satuan : req.body.satuan.toString(),
            targetVolume : parseInt(req.body.targetVolume)
        },
        where:{
            idUraian : parseInt(req.body.idUraian)
        }
    })

    const getTriwulan = await prisma.uraianTriwulan.deleteMany({
        where:{
            idUraian : parseInt(req.body.idUraian)
        }
    }) 

    await Promise.all([update,getTriwulan]).then(async(item)=>{
        await Promise.all(req.body.targetTriwulan.map(async(items) =>{
            await prisma.uraianTriwulan.create({
                data:{
                    idUraian : parseInt(item[0].idUraian),
                    idSubKegiatan : parseInt(item[0].idSubKegiatan),
                    triwulan : parseInt(items.triwulan),
                    targetVolume : parseInt(items.targetVolume),
                    targetPersen : items.targetPersen.toString()
                }
            })
            return item[0].idSubKegiatan
        })).then(async(idSub)=>{
            const totalPersenTw1 = await totalPersenTriwulan(idSub[0],1)
            const totalPersenTw2= await totalPersenTriwulan(idSub[0],2)
            const totalPersenTw3 = await totalPersenTriwulan(idSub[0],3)
            const totalPersenTw4 = await totalPersenTriwulan(idSub[0],4)
            await Promise.all([totalPersenTw1,totalPersenTw2,totalPersenTw3,totalPersenTw4]).then(async (item) => {
                const getRekapExist = await prisma.targetTriwulan.findFirst({
                    where : {
                        idSubKegiatan : parseInt(idSub[0]),
                        tahun : parseInt(req.body.tahun),
                        opd : parseInt(req.body.opd)
                    }
                })

                if(!getRekapExist){
                    await prisma.targetTriwulan.create({
                        data : {
                            idSubKegiatan : parseInt(idSub[0]),
                            tahun : parseInt(req.body.tahun),
                            opd : parseInt(req.body.opd),
                            idUser : parseInt(req.user.userId),
                            dateCreate : new Date(),
                            t1 : item[0].t1 ? item[0].t1: 0,
                            t2 : item[1].t2 ? item[1].t2: 0,
                            t3 : item[2].t3 ? item[2].t3: 0,
                            t4 : item[3].t4 ? item[3].t4: 0
                        }
                    })
                }else{
                    await prisma.targetTriwulan.updateMany({
                        data : {
                            t1 : item[0].t1 ? item[0].t1: 0,
                            t2 : item[1].t2 ? item[1].t2: 0,
                            t3 : item[2].t3 ? item[2].t3: 0,
                            t4 : item[3].t4 ? item[3].t4: 0
                        },
                        where : {
                            idTriwulan : parseInt(getRekapExist.idTriwulan)
                        }
                    })
                }
            })
        })
    } )
    return update
}

const listUraian = async (req) => {
    const bodyAllow = Object.keys(req.params)
    bodyAllow.forEach(element => {
        if(element === 'tahun'||element === 'opd'){
        }else{
            throw new errorHandling(401,'Parameter salah silahakn cek body request')
        }

    });

    try {
        const getUraian = await prisma.uraianRenja.findMany({
            where : {
                tahun : parseInt(req.params.tahun),
                opd : parseInt(req.params.opd)
            }
        })

        if(getUraian.length < 1) {
            throw new errorHandling(404,'Data Uraian Belum Ada')
        }

        const getSubkegiatan = await prisma.instrumen.findMany({
            where : {
                periode : parseInt(req.params.tahun),
                opd : parseInt(req.params.opd),
                type : 'subKegiatan'
            }
        });

        let result = [];
        await Promise.all(getSubkegiatan.map(async(item)=>{
            const uraian = await prisma.uraianRenja.findMany({
                where : {
                    idSubKegiatan : parseInt(item.idInstrumen)
                }
            })

            const getPersenTarget = await prisma.targetTriwulan.findFirst({
                where : {
                    idSubKegiatan :  parseInt(item.idInstrumen),
                    tahun : parseInt(req.params.tahun),
                    opd : parseInt(req.params.opd)
                },
                select :{
                    t1 : true,
                    t2 : true,
                    t3 : true,
                    t4 : true
                }
            })

            //console.log(getPersenTarget ? "Kondisi")

            const targetPersenTw1s = getPersenTarget ? getPersenTarget.t1 : 0
            const targetPersenTw2s = getPersenTarget ? getPersenTarget.t2 : 0
            const targetPersenTw3s = getPersenTarget ? getPersenTarget.t3 : 0
            const targetPersenTw4s = getPersenTarget ? getPersenTarget.t4 : 0

            const totalPersenTarget = {
                targetPersenTw1 : targetPersenTw1s,
                targetPersenTw2 : targetPersenTw2s,
                targetPersenTw3 : targetPersenTw3s,
                targetPersenTw4 : targetPersenTw4s,
                TotalPersenT1sdT4 : parseInt(targetPersenTw1s) + parseInt(targetPersenTw2s) + parseInt(targetPersenTw3s) + parseInt(targetPersenTw4s)
                //TotalPersenT1sdT4 : parseInt(getPersenTarget.t1) + parseInt(getPersenTarget.t2) + parseInt(getPersenTarget.t3) + parseInt(getPersenTarget.t1)
            }

            const Uraian = []
            await Promise.all(uraian.map(async(item)=>{
                const target = await prisma.uraianTriwulan.findMany({
                    where : {
                        idUraian : parseInt(item.idUraian)
                    },
                    orderBy: {
                        'triwulan': 'asc'
                    }
                })

                const dataTarget = target.map((items)=>{
                    const parser = {
                        triwulan : items.triwulan,
                        targetVolume : items.targetVolume,
                        targetPersen : items.targetPersen
                    }
                    return parser
                })
                const dataTargetUraian = {
                    idUraian : item.idUraian,
                    namaUraian : item.namaUraian,
                    targetVolume : item.targetVolume,
                    satuan : item.satuan,
                    dataTarget
                }
                Uraian.push(dataTargetUraian)
            }))

            const dataSubKagiatan = {
                idSubKegiatan : item.idInstrumen,
                namaSubKegiatan : item.tujuan,
                tahun : item.periode,
                opd : item.opd,
                totalPersenTarget,
                Uraian
            }

            result.push(dataSubKagiatan)
        }))

        return result
    }
    catch (e) {
        throw new errorHandling(500, e);
    }
}

export {
    inputUraian,
    updateUraian,
    listUraian
}