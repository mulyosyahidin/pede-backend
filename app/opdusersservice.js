import { PrismaClient } from '@prisma/client'
import { errorHandling } from '../middlewares/errorHandler.js'

const prisma = new PrismaClient()
process.env.TZ = 'Asia/Jakarta'

const validId = async (id) => {
    const result = await prisma.opdUser.findUnique({
        where : {
            opdId : parseInt(id)
        }
    })

    return result
}

const listOpd= async (req) =>{
    const bodyAllow = Object.keys(req.params)
    bodyAllow.forEach(element => {
        if(element === 'page'){
        }else{
            throw new errorHandling(401,'Parameter salah silahakn cek body request')
        }

    });
    const countData = await prisma.opdUser.aggregate({
        _count: {
            opdId : true
        }
    })
    const JumlahPage = Math.ceil(parseInt(countData._count.opdId)/20)
    if(req.params.page > JumlahPage) {
        throw new errorHandling(401,'Tidak ada data pada page yang di pilih')
    }
    const takeOpdList = (req.params.page == 1) ? 0 : (req.params.page - 1) * 20
    const dataOpd = await prisma.opdUser.findMany({
        skip: parseInt(takeOpdList),
        take: 20,
        orderBy: {
            opdId: 'desc',
        }
    })
    const dataAkhir = {
        jumlahPage : JumlahPage,
        jumlahData : countData._count.opdId,
        statusPage : req.params.page,
        dataOpd
    }
    return dataAkhir
}

const opdKeyword= async (req) =>{
    const bodyAllow = Object.keys(req.params)
    bodyAllow.forEach(element => {
        if(element === 'page' || element === 'keyword'){
        }else{
            throw new errorHandling(401,'Parameter salah silahakn cek body request')
        }

    });
    const countData = await prisma.opdUser.aggregate({
        _count: {
            opdId : true
        },
        where : {
            namaOpd : {
                contains : req.params.keyword ? req.params.keyword.toString() : ''
            }
        }
    })
    const JumlahPage = Math.ceil(parseInt(countData._count.opdId)/20)
    if(req.params.page > JumlahPage) {
        throw new errorHandling(401,'Tidak ada data pada page yang di pilih')
    }
    const takeOpdList = (req.params.page == 1) ? 0 : (req.params.page - 1) * 20
    const dataOpd = await prisma.opdUser.findMany({
        skip: parseInt(takeOpdList),
        take: 20,
        orderBy: {
            opdId: 'desc',
        },
        where : {
            namaOpd : {
                contains : req.params.keyword ? req.params.keyword.toString() : ''
            }
        }
    })
    const dataAkhir = {
        jumlahPage : JumlahPage,
        jumlahData : countData._count.opdId,
        statusPage : req.params.page,
        dataOpd
    }
    return dataAkhir
}

const addOpd = async (req) => {
    await prisma.opdUser.create({
        data : req.body
    })
    return
}

const updateOpd = async (req) => {
    const cekId = await validId(req.body.id)
    if(!cekId){
        throw new errorHandling(404,'Data opd tidak terdaftar di sistem')
    } 

    const bodyAllow = Object.keys(req.body.data)
    bodyAllow.forEach(element => {
        if(element === 'namaOpd'){
        }else{
            throw new errorHandling(401,'Parameter salah silahakn cek body request')
        }

    });

    await prisma.opdUser.update({
        data : req.body.data,
        where : {
            opdId : parseInt(req.body.id)
        }
    })

    return
}

const hapusOpd = async (req) => {
    const cekId = await validId(req.params.id)
    if(!cekId){
        throw new errorHandling(404,'Data opd tidak terdaftar di sistem')
    } 

    await prisma.opdUser.delete({
        where : {
            opdId : parseInt(req.params.id)
        }
    })

    return
}

const getSingle = async (req) => {
    const cekId = await validId(req.params.id)
    if(!cekId){
        throw new errorHandling(404,'Data opd tidak terdaftar di sistem')
    } 

    const result = await prisma.opdUser.findUnique({
        where : {
            opdId : parseInt(req.params.id)
        }
    })

    return result
}

const listAllOpd = async (req) => {
    const dataOpd = await prisma.opdUser.findMany({
        orderBy: {
            opdId: 'desc',
        }
    })

    return dataOpd
}

export {
    listOpd,
    addOpd,
    updateOpd,
    hapusOpd,
    getSingle,
    opdKeyword,
    listAllOpd,
}