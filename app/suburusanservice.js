import { PrismaClient } from '@prisma/client'
import { errorHandling } from '../middlewares/errorHandler.js'


const prisma = new PrismaClient()
process.env.TZ = 'Asia/Jakarta'

const cekUrusan =  async (id) => {
    return await prisma.urusan.findFirst({
        where : {
            idUrusan : parseInt(id)
        }
    })
}

const cekSuburusan = async (id) => {
    return await prisma.subUrusan.findFirst({
        where : {
            idSub : parseInt(id)
        }
    })
}

const add = async (req) => {
    const bodyAllow = Object.keys(req.body)
    bodyAllow.forEach(element => {
        if(element === 'idUrusan' || element === 'namaSub'||element === 'kode'){
        }else{
            throw new errorHandling(401,'Parameter salah silahakn cek body request')
        }

    });
    const idExist = bodyAllow.filter(item => item === 'idUrusan');
    if(idExist.length < 1){
        throw new errorHandling(404, "Tidak dapat menemukan parameter ID Urusan")
    }
    const subExist = bodyAllow.filter(item => item === 'namaSub');
    if(subExist.length < 1){
        throw new errorHandling(404, "Tidak dapat menemukan parameter namaSub")
    }

    const cekUrusans = await cekUrusan(req.body.idUrusan)
    if(!cekUrusans){
        throw new errorHandling(404, "Data Urusan tidak ada dalam sistem")
    }

    const hasil =  await prisma.subUrusan.create({
        data : {
            urusan : parseInt(req.body.idUrusan),
            namaSubUrusan : req.body.namaSub.toString(),
            createDate : new Date(),
            idCreate : parseInt(req.user.userId)
        }
    })

    const result = []
    await Promise.all([hasil]).then(async (item)=>{
        const cekKode = await prisma.kodeInstrumen.findMany({
            where : {
                idIstrumen : parseInt(item[0].idSub),
                type : 'subUrusan'
            }
        })
        if(cekKode.length > 0){
            await prisma.kodeInstrumen.updateMany({
                data : {
                    kode : req.body.kode ? req.body.kode.toString() : null 
                },
                where : {
                    idIstrumen : parseInt(item[0].idSub),
                    type : 'subUrusan'
                }
            })
        }else{
            await prisma.kodeInstrumen.create({
                data : {
                    idIstrumen : parseInt(item[0].idSub),
                    type : 'subUrusan',
                    kode : req.body.kode ? req.body.kode.toString() : null 
                }
            })
        }
        const parse = {
            "idSub": item[0].idSub,
            "namaSub": item[0].namaSubUrusan,
            "idUrusan" : item[0].urusan,
            "kode" : req.body.kode ? req.body.kode.toString() : null
        }
        result.push(parse)
    })
    return result
}

const update = async (req) => {
    const bodyAllow = Object.keys(req.body)
    bodyAllow.forEach(element => {
        if(element === 'idSub' || element === 'data' || element === 'kode'){
        }else{
            throw new errorHandling(401,'Parameter salah silahakn cek body request')
        }

    });
    const dataExist = Object.keys(req.body.data)
    dataExist.forEach(element => {
        if(element === 'urusan' || element === 'namaSubUrusan'){
        }else{
            throw new errorHandling(401,'Parameter data tidak sesuai')
        }

    });

    const recordExist = await cekSuburusan(parseInt(req.body.idSub))
    if(!recordExist){
        throw new errorHandling(404,'Data Sub Urusan tidak di temukan')
    }
    const hasil =  await prisma.subUrusan.update({
        data : req.body.data,
        where : {
            idSub : parseInt(req.body.idSub)
        }
    })

    const result = []
    await Promise.all([hasil]).then(async (item)=>{
        const cekKode = await prisma.kodeInstrumen.findMany({
            where : {
                idIstrumen : parseInt(req.body.idSub),
                type : 'subUrusan'
            }
        })
        if(cekKode.length > 0){
            await prisma.kodeInstrumen.updateMany({
                data : {
                    kode : req.body.kode ? req.body.kode.toString() : null 
                },
                where : {
                    idIstrumen : parseInt(req.body.idSub),
                    type : 'subUrusan'
                }
            })
        }else{
            await prisma.kodeInstrumen.create({
                data : {
                    idIstrumen : parseInt(req.body.idSub),
                    type : 'subUrusan',
                    kode : req.body.kode ? req.body.kode.toString() : null 
                }
            })
        }
        const parse = {
            "idSub": item[0].idSub,
            "namaSub": item[0].namaSubUrusan,
            "idUrusan" : item[0].urusan,
            "kode" : req.body.kode ? req.body.kode.toString() : null
        }
        result.push(parse)
    })
    
    return result
}


const hapus = async (req) => {
    const bodyAllow = Object.keys(req.params)
    bodyAllow.forEach(element => {
        if(element === 'idSub'){
        }else{
            throw new errorHandling(401,'Parameter salah silahakn cek body request')
        }

    });
    const recordExist = await cekSuburusan(parseInt(req.params.idSub))
    if(!recordExist){
        throw new errorHandling(404,'Data Sub Urusan tidak di temukan')
    }

    await prisma.subUrusan.delete({
        where : {
            idSub : parseInt(req.params.idSub)
        }
    })

    await prisma.kodeInstrumen.deleteMany({
        where :{
            idIstrumen : parseInt(req.params.idSub),
            type : 'subUrusan'
        }
    })

    return
}

async function getKode(id,type) {
    const result = await prisma.kodeInstrumen.findMany({
        where : {
            idIstrumen : parseInt(id),
            type : type.toString()
        }
    })
    const retur = result[0] ? result[0].kode : null
    return retur
}

const listAll = async (req) => {
    const bodyAllow = Object.keys(req.params)
    bodyAllow.forEach(element => {
        if(element === 'page'){
        }else{
            throw new errorHandling(401,'Parameter salah silahakn cek body request')
        }

    });
    const countData = await prisma.subUrusan.aggregate({
        _count: {
            idSub : true
        }
    })
    const JumlahPage = Math.ceil(parseInt(countData._count.idSub)/20)
    if(req.params.page > JumlahPage) {
        throw new errorHandling(401,'Tidak ada data pada page yang di pilih')
    }
    const skipSubList = (req.params.page == 1) ? 0 : (req.params.page - 1) * 20
    const fetch = await prisma.subUrusan.findMany({
        skip : parseInt(skipSubList),
        take : 20,
        orderBy : {
            idSub : 'desc'
        }
    })
    const dataSubUrusan = []
    await Promise.all(fetch.map(async (item) => {
        const getUrusan = await prisma.urusan.findFirst({
            where : {
                idUrusan : parseInt(item.urusan)
            }
        })
        const dataUrusanParser = []
        await Promise.all([getUrusan]).then(async (it)=>{
            const parse = {
                idSub : item.idSub,
                urusan : it[0] ? it[0].namaUrusan : null,
                idUrusan : item.urusan,
                namaSubUrusan : item.namaSubUrusan,
                kodeSub : await getKode(item.idSub, 'subUrusan')
            }
            dataUrusanParser.push(parse)
        })
        dataSubUrusan.push(dataUrusanParser[0])
    }))

    const dataAkhir = {
        jumlahPage : JumlahPage,
        jumlahData : countData._count.idSub,
        statusPage : req.params.page,
        dataSubUrusan
    }
    return dataAkhir
}

const getByUrusan = async (req) => {
    const bodyAllow = Object.keys(req.params)
    bodyAllow.forEach(element => {
        if(element === 'idUrusan' || element === 'page'){
        }else{
            throw new errorHandling(401,'Parameter salah silahakn cek body request')
        }

    });
    const recordExist = await cekUrusan(parseInt(req.params.idUrusan))
    if(!recordExist){
        throw new errorHandling(404,'Data Sub Urusan tidak di temukan')
    }

    const countData = await prisma.subUrusan.aggregate({
        where : {
            urusan : parseInt(req.params.idUrusan)
        },
        _count: {
            idSub : true
        }
    })
    const JumlahPage = Math.ceil(parseInt(countData._count.idSub)/20)
    if(req.params.page > JumlahPage) {
        throw new errorHandling(401,'Tidak ada data pada page yang di pilih')
    }
    const skipSubList = (req.params.page == 1) ? 0 : (req.params.page - 1) * 20
    const fetch = await prisma.subUrusan.findMany({
        where : {
            urusan : parseInt(req.params.idUrusan)
        },
        skip : parseInt(skipSubList),
        take : 20,
        orderBy : {
            idSub : 'desc'
        }
    })
    const dataSubUrusan = []
    await Promise.all(fetch.map(async (item) => {
        const getUrusan = await prisma.urusan.findFirst({
            where : {
                idUrusan : parseInt(item.urusan)
            }
        })
        const dataUrusanParser = []
        await Promise.all([getUrusan]).then(async (it)=>{
            const parse = {
                idSub : item.idSub,
                urusan : it[0] ? it[0].namaUrusan : null,
                idUrusan : item.urusan,
                namaSubUrusan : item.namaSubUrusan,
                kodeSub : await getKode(item.idSub, 'subUrusan')
            }
            dataUrusanParser.push(parse)
        })
        dataSubUrusan.push(dataUrusanParser[0])
    }))

    const dataAkhir = {
        jumlahPage : JumlahPage,
        jumlahData : countData._count.idSub,
        statusPage : req.params.page,
        dataSubUrusan
    }
    return dataAkhir

}
const getByNamaSub = async (req) => {
    const bodyAllow = Object.keys(req.params)
    bodyAllow.forEach(element => {
        if(element === 'idUrusan' || element === 'namaSub'){
        }else{
            throw new errorHandling(401,'Parameter salah silahakn cek body request')
        }

    });
    const recordExist = await cekUrusan(parseInt(req.params.idUrusan))
    if(!recordExist){
        throw new errorHandling(404,'Data Sub Urusan tidak di temukan')
    }

    const parse = await prisma.subUrusan.findMany({
        where : {
            urusan : parseInt(req.params.idUrusan),
            namaSubUrusan : {
                contains : req.params.namaSub
            }
        }
    })
    const result = []
    await Promise.all( parse.map (async (item) => {
        const urusan = await prisma.urusan.findFirst({
            where : {
                idUrusan : parseInt(item.urusan)
            },
            select : {
                idUrusan :true,
                namaUrusan : true
            }
        })
        const push = {
            idSub : item.idSub,
            urusan : urusan.namaUrusan,
            idUrusan : urusan.idUrusan,
            namaSubUrusan : item.namaSubUrusan,
            kodeSub : await getKode(item.idSub, 'subUrusan')
            
        }
        result.push(push)
    }))
    return result

}

const getSingleData = async (req) => {
    const bodyAllow = Object.keys(req.params)
    bodyAllow.forEach(element => {
        if(element === 'idSub'){
        }else{
            throw new errorHandling(401,'Parameter salah silahakn cek body request')
        }

    });
    const recordExist = await cekSuburusan(parseInt(req.params.idSub))
    if(!recordExist){
        throw new errorHandling(404,'Data Sub Urusan tidak di temukan')
    }
    const fetch = await prisma.$queryRaw`
        SELECT *
        FROM suburusan
        LEFT JOIN urusan ON suburusan.urusan = urusan.idUrusan
        WHERE suburusan.idSub = ${parseInt(req.params.idSub)}
      `;
    const result = []
    await Promise.all(fetch.map( async (item) => {
        const parse = {
            idSub : item.idSub,
            urusan : item.namaUrusan,
            idUrusan : item.urusan,
            namaSubUrusan : item.namaSubUrusan,
            kodeSub : await getKode(item.idSub, 'subUrusan')
        }
        result.push(parse)
    }))
    return result

}

export {
    add,
    update,
    hapus,
    listAll,
    getByUrusan,
    getByNamaSub,
    getSingleData
}