import { PrismaClient } from "@prisma/client";
import { errorHandling } from '../middlewares/errorHandler.js'

const prisma = new PrismaClient()

const list = async (req) =>{
    const bodyAllow = Object.keys(req.params)
    bodyAllow.forEach(element => {
        if(element === 'tahun'||element === 'opd'){
        }else{
            throw new errorHandling(401,'Parameter salah silahakn cek body request')
        }
    });

    const getKegiatan = await prisma.instrumen.findMany({
        where : {
            periode : parseInt(req.params.tahun),
            opd : parseInt(req.params.opd),
            type : 'kegiatan'
        }
    })

    if(getKegiatan.length < 1){
        throw new errorHandling(404,'Data kegiatan blm ada')
    }

    const result = []
    await Promise.all(getKegiatan.map(async(item)=>{
        const getSubkegiatan = await prisma.instrumen.findMany({
            where : {
                parent : parseInt(item.idInstrumen),
                type : 'subKegiatan'
            }
        })
        const dataSubKegiatan =[]
        await Promise.all(getSubkegiatan.map(async(item)=>{
            const dataTriwulan = await prisma.targetTriwulan.findMany({
                where : {
                    idSubKegiatan : parseInt(item.idInstrumen),
                    tahun : parseInt(req.params.tahun),
                    opd : parseInt(req.params.opd)
                },
                select:{
                    t1 : true,
                    t2 : true,
                    t3 : true,
                    t4 : true
                }
            })
            const dataRes = {
                idInstrumen : item.idInstrumen,
                tahun : item.periode,
                opd : item.opd,
                namaSubKegiatan : item.tujuan,
                dataTriwulan
            }

            dataSubKegiatan.push(dataRes)
        }))
        const dataSubKegiatans = dataSubKegiatan
        const parse = {
            idInstrumen : item.idInstrumen,
            tahun : item.periode,
            opd : item.opd,
            namaKegiatan : item.tujuan,
            dataSubKegiatans
        }
        result.push(parse)
    }))

    return result
}

export{
    list
}