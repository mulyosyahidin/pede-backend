import { validationResult } from 'express-validator'
import response from './../response.js'

const validate = (req, res, next) => {
    const errors = validationResult(req)

    if (!errors.isEmpty()) {
        return response(res, 400, false, 'Perhatikan parameter harus sesuai format yang di minta')
    }

    next()
}

export default validate
