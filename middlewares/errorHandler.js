class errorHandling extends Error {

    constructor(status,message){
        super(message)
        this.status = status
    }
}

export {errorHandling}