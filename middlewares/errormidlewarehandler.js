import response from "../response.js";
import { errorHandling } from "./errorHandler.js";

const errorMidleware = async (err, req, res, next) => {
    if(!err){
        next()
        return
    }
    if( err instanceof errorHandling){
      return response(res, err.status, false, err.message)
    }else{
        console.log(err)
        return response(res, 500, false, err.message)
    }
}

export default errorMidleware