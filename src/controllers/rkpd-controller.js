import rkpdService from "../services/rkpd-service.js";
import response from "../../response.js";
import {excelLaporanF} from "../../app/faktorrenjaservice.js";
import fs from "fs";

const evaluasi = async (req, res, next) => {
    try {
        const dataEvaluasi = await rkpdService.evaluasi(req);

        return response(res, 200, true, 'Data laporan berhasil di tampilkan', dataEvaluasi);
    } catch (error) {
        next(error);
    }
}

const excel = async (req, res, next) => {
    try {
        const result = await rkpdService.excel(req);

        const fileName = result.data.file_name;
        const filePath = `${process.cwd()}/public/laporan/${fileName}`;

        if (fs.existsSync(filePath)) {
            res.download(filePath, fileName, (err) => {
                if (err) {
                    return response(res, 500, false, 'Gagal mendownload file', err);
                }

                fs.unlinkSync(filePath);
            });
        } else {
            return response(res, 404, false, 'File tidak ditemukan', null);
        }
    } catch (e) {
        next(e)
    }
}


export default {
    evaluasi,
    excel,
};