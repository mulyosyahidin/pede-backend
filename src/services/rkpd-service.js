import prismaClient from "../config/database.js";
import fs from "fs";
import pkg from 'exceljs';
import {htmlToRichText} from "../utils/excel/html-converter.js";
import {formatNumber, formatRupiah} from "../utils/util.js";

const {Workbook} = pkg;

const evaluasi = async (req) => {
    const idOpd = parseInt(req.params.idOpd);

    // check is opd exist
    const opd = await prismaClient.opdUser.findFirst({
        where: {
            opdId: parseInt(idOpd),
        },
    });

    if (!opd) {
        throw new Error('Opd tidak ditemukan');
    }

    const dataTahun = [2022, 2023, 2024];
    const dataEvaluasi = [];

    let _getAllTujuan = await prismaClient.instrumenView.findMany({
        where: {
            type: 'tujuan',
            opd: idOpd,
        },
    });

    for (const item of _getAllTujuan) {
        let idInstrumen = item.idInstrumen;
        let periode = item.periode;
        let tujuan = item.tujuan;

        // check is dataEvaluasi has item with tujuan
        let isExist = dataEvaluasi.find((item) => item.nama == tujuan);

        if (!isExist) {
            let idsInstrumenTujuan = _getAllTujuan.filter((item) => item.tujuan == tujuan).map((item) => item.idInstrumen);

            let dataIndikatorTujuan = [];
            let getDataIndikatorTujuan = await prismaClient.instrumenView.findMany({
                where: {
                    opd: idOpd,
                    indikatorParent: {
                        in: idsInstrumenTujuan,
                    },
                },
                distinct: ['indikator_kinerja'],
            });
            let getDataIndikatorTujuanAll = await prismaClient.instrumenView.findMany({
                where: {
                    opd: idOpd,
                    indikatorParent: {
                        in: idsInstrumenTujuan,
                    },
                },
            });

            for (const item of getDataIndikatorTujuan) {
                let dataTargetDanRealisasi = [];

                let getDataTargetDanRealisasi = getDataIndikatorTujuanAll.filter((item2) => item2.indikator_kinerja == item.indikator_kinerja);
                for (const item2 of getDataTargetDanRealisasi) {
                    dataTargetDanRealisasi.push({
                        periode: item2.periode,
                        target: {
                            kinerja: item2.target,
                            satuan: item2.satuan,
                        },
                        realisasi: {
                            kinerja: null,
                            rp: null,
                        }
                    });
                }

                for (const item2 of dataTahun) {
                    let isExist = dataTargetDanRealisasi.find((item3) => item3.periode == item2);

                    if (!isExist) {
                        dataTargetDanRealisasi.push({
                            periode: item2,
                            target: {
                                kinerja: null,
                                satuan: null,
                            },
                            realisasi: {
                                kinerja: null,
                                rp: null,
                            }
                        });
                    }
                }

                // sort dataTargetDanRealisasi by periode
                dataTargetDanRealisasi.sort((a, b) => a.periode - b.periode);

                dataIndikatorTujuan.push({
                    nama: item.indikator_kinerja,
                    targetAkhirPeriode: {
                        kinerja: item.target_akhir_periode,
                        satuan: item.satuan,
                    },
                    targetDanRealisasi: dataTargetDanRealisasi,
                })
            }

            // start: sasaran
            let dataSasaranTujuan = [];

            let _getAllSasaran = await prismaClient.instrumenView.findMany({
                where: {
                    type: 'sasaran',
                    opd: idOpd,
                    parent: idInstrumen,
                },
            });

            for (const item of _getAllSasaran) {
                let idInstrumen = item.idInstrumen;
                let sasaran = item.tujuan;

                // check is dataSasaranTujuan has item with sasaran
                let isExist = dataSasaranTujuan.find((item) => item.nama == sasaran);

                if (!isExist) {
                    let idsInstrumenSasaran = _getAllSasaran.filter((item) => item.tujuan == sasaran).map((item) => item.idInstrumen);

                    let dataIndikatorSasaran = [];
                    let getDataIndikatorSasaran = await prismaClient.instrumenView.findMany({
                        where: {
                            opd: idOpd,
                            indikatorParent: {
                                in: idsInstrumenSasaran,
                            },
                        },
                        distinct: ['indikator_kinerja'],
                    });
                    let getDataIndikatorSasaranAll = await prismaClient.instrumenView.findMany({
                        where: {
                            opd: idOpd,
                            indikatorParent: {
                                in: idsInstrumenSasaran,
                            },
                        },
                    });

                    for (const item of getDataIndikatorSasaran) {
                        let dataTargetDanRealisasi = [];

                        let getDataTargetDanRealisasi = getDataIndikatorSasaranAll.filter((item2) => item2.indikator_kinerja == item.indikator_kinerja);
                        for (const item2 of getDataTargetDanRealisasi) {
                            dataTargetDanRealisasi.push({
                                periode: item2.periode,
                                target: {
                                    kinerja: item2.target,
                                    satuan: item2.satuan,
                                },
                                realisasi: {
                                    kinerja: null,
                                    rp: null,
                                }
                            });
                        }

                        for (const item2 of dataTahun) {
                            let isExist = dataTargetDanRealisasi.find((item3) => item3.periode == item2);

                            if (!isExist) {
                                dataTargetDanRealisasi.push({
                                    periode: item2,
                                    target: {
                                        kinerja: null,
                                        satuan: null,
                                    },
                                    realisasi: {
                                        kinerja: null,
                                        rp: null,
                                    }
                                });
                            }
                        }

                        // sort dataTargetDanRealisasi by periode
                        dataTargetDanRealisasi.sort((a, b) => a.periode - b.periode);

                        dataIndikatorSasaran.push({
                            nama: item.indikator_kinerja,
                            targetAkhirPeriode: {
                                kinerja: item.target_akhir_periode,
                                satuan: item.satuan,
                            },
                            targetDanRealisasi: dataTargetDanRealisasi,
                        })
                    }

                    // start: program
                    let dataProgramSasaran = [];

                    let _getAllProgram = await prismaClient.instrumenView.findMany({
                        where: {
                            type: 'program',
                            opd: idOpd,
                            parent: idInstrumen,
                        },
                    });

                    for (const item of _getAllProgram) {
                        let idInstrumen = item.idInstrumen;
                        let program = item.tujuan;

                        // check is dataProgramSasaran has item with program
                        let isExist = dataProgramSasaran.find((item) => item.nama == program);

                        if (!isExist) {
                            let idsInstrumenProgram = _getAllProgram.filter((item) => item.tujuan == program).map((item) => item.idInstrumen);

                            let dataIndikatorProgram = [];
                            let getDataIndikatorProgram = await prismaClient.instrumenView.findMany({
                                where: {
                                    opd: idOpd,
                                    indikatorParent: {
                                        in: idsInstrumenProgram,
                                    },
                                },
                                distinct: ['indikator_kinerja'],
                            });
                            let getDataIndikatorProgramAll = await prismaClient.instrumenView.findMany({
                                where: {
                                    opd: idOpd,
                                    indikatorParent: {
                                        in: idsInstrumenProgram,
                                    },
                                },
                            });

                            for (const item of getDataIndikatorProgram) {
                                let dataTargetDanRealisasi = [];

                                let getDataTargetDanRealisasi = getDataIndikatorProgramAll.filter((item2) => item2.indikator_kinerja == item.indikator_kinerja);
                                for (const item2 of getDataTargetDanRealisasi) {
                                    dataTargetDanRealisasi.push({
                                        periode: item2.periode,
                                        target: {
                                            kinerja: item2.target,
                                            satuan: item2.satuan,
                                        },
                                        realisasi: {
                                            kinerja: null,
                                            rp: null,
                                        }
                                    });
                                }

                                for (const item2 of dataTahun) {
                                    let isExist = dataTargetDanRealisasi.find((item3) => item3.periode == item2);

                                    if (!isExist) {
                                        dataTargetDanRealisasi.push({
                                            periode: item2,
                                            target: {
                                                kinerja: null,
                                                satuan: null,
                                            },
                                            realisasi: {
                                                kinerja: null,
                                                rp: null,
                                            }
                                        });
                                    }
                                }

                                // sort dataTargetDanRealisasi by periode
                                dataTargetDanRealisasi.sort((a, b) => a.periode - b.periode);

                                dataIndikatorProgram.push({
                                    nama: item.indikator_kinerja,
                                    targetAkhirPeriode: {
                                        kinerja: item.target_akhir_periode,
                                        satuan: item.satuan,
                                    },
                                    targetDanRealisasi: dataTargetDanRealisasi,
                                });
                            }

                            // start: kegiatan
                            let dataKegiatanProgram = [];

                            let _getAllKegiatan = await prismaClient.instrumenView.findMany({
                                where: {
                                    type: 'kegiatan',
                                    opd: idOpd,
                                    parent: idInstrumen,
                                },
                            });

                            for (const item of _getAllKegiatan) {
                                let idInstrumen = item.idInstrumen;
                                let kegiatan = item.tujuan;

                                // check is dataKegiatanProgram has item with kegiatan
                                let isExist = dataKegiatanProgram.find((item) => item.nama == kegiatan);

                                if (!isExist) {
                                    let idsInstrumenKegiatan = _getAllKegiatan.filter((item) => item.tujuan == kegiatan).map((item) => item.idInstrumen);

                                    let dataIndikatorKegiatan = [];
                                    let getDataIndikatorKegiatan = await prismaClient.instrumenView.findMany({
                                        where: {
                                            opd: idOpd,
                                            indikatorParent: {
                                                in: idsInstrumenKegiatan,
                                            },
                                        },
                                        distinct: ['indikator_kinerja'],
                                    });
                                    let getDataIndikatorKegiatanAll = await prismaClient.instrumenView.findMany({
                                        where: {
                                            opd: idOpd,
                                            indikatorParent: {
                                                in: idsInstrumenKegiatan,
                                            },
                                        },
                                    });

                                    for (const item of getDataIndikatorKegiatan) {
                                        let dataTargetDanRealisasi = [];

                                        let getDataTargetDanRealisasi = getDataIndikatorKegiatanAll.filter((item2) => item2.indikator_kinerja == item.indikator_kinerja);
                                        for (const item2 of getDataTargetDanRealisasi) {
                                            dataTargetDanRealisasi.push({
                                                periode: item2.periode,
                                                target: {
                                                    kinerja: item2.target,
                                                    satuan: item2.satuan,
                                                },
                                                realisasi: {
                                                    kinerja: null,
                                                    rp: null,
                                                }
                                            });
                                        }

                                        for (const item2 of dataTahun) {
                                            let isExist = dataTargetDanRealisasi.find((item3) => item3.periode == item2);

                                            if (!isExist) {
                                                dataTargetDanRealisasi.push({
                                                    periode: item2,
                                                    target: {
                                                        kinerja: null,
                                                        satuan: null,
                                                    },
                                                    realisasi: {
                                                        kinerja: null,
                                                        rp: null,
                                                    }
                                                });
                                            }
                                        }

                                        // sort dataTargetDanRealisasi by periode
                                        dataTargetDanRealisasi.sort((a, b) => a.periode - b.periode);

                                        dataIndikatorKegiatan.push({
                                            nama: item.indikator_kinerja,
                                            targetAkhirPeriode: {
                                                kinerja: item.target_akhir_periode,
                                                satuan: item.satuan,
                                            },
                                            targetDanRealisasi: dataTargetDanRealisasi,
                                        });
                                    }

                                    // start: sub kegiatan
                                    let dataSubKegiatan = [];

                                    let _getAllSubKegiatan = await prismaClient.instrumenView.findMany({
                                        where: {
                                            type: 'subKegiatan',
                                            opd: idOpd,
                                            parent: idInstrumen,
                                        },
                                    });

                                    for (const item of _getAllSubKegiatan) {
                                        let idInstrumen = item.idInstrumen;
                                        let subKegiatan = item.tujuan;

                                        // check is dataSubKegiatan has item with subKegiatan
                                        let isExist = dataSubKegiatan.find((item) => item.nama == subKegiatan);

                                        if (!isExist) {
                                            let idsInstrumenSubKegiatan = _getAllSubKegiatan.filter((item) => item.tujuan == subKegiatan).map((item) => item.idInstrumen);

                                            let dataIndikatorSubKegiatan = [];
                                            let getDataIndikatorSubKegiatan = await prismaClient.instrumenView.findMany({
                                                where: {
                                                    opd: idOpd,
                                                    indikatorParent: {
                                                        in: idsInstrumenSubKegiatan,
                                                    },
                                                },
                                                distinct: ['indikator_kinerja'],
                                            });
                                            let getDataIndikatorSubKegiatanAll = await prismaClient.instrumenView.findMany({
                                                where: {
                                                    opd: idOpd,
                                                    indikatorParent: {
                                                        in: idsInstrumenSubKegiatan,
                                                    },
                                                },
                                            });

                                            let realisasiTriwulan = await prismaClient.paguTriwulan.findMany({
                                                where: {
                                                    idSubKegiatan: idInstrumen,
                                                }
                                            });

                                            let totalPaguIndikatif = 0;

                                            for (const item of realisasiTriwulan) {
                                                totalPaguIndikatif += item.paguIndikatif;
                                            }

                                            let uraianTotalTriwulan = await prismaClient.uraianTotalTriwulan.findMany({
                                                where: {
                                                    idSubKegiatan: idInstrumen,
                                                }
                                            });

                                            let totalKinerja = 0;

                                            for (const item of uraianTotalTriwulan) {
                                                totalKinerja += parseInt(item.totalKinerja);
                                            }

                                            for (const item of getDataIndikatorSubKegiatan) {
                                                let dataTargetDanRealisasi = [];

                                                let getDataTargetDanRealisasi = getDataIndikatorSubKegiatanAll.filter((item2) => item2.indikator_kinerja == item.indikator_kinerja);
                                                for (const item2 of getDataTargetDanRealisasi) {
                                                    dataTargetDanRealisasi.push({
                                                        periode: item2.periode,
                                                        target: {
                                                            kinerja: item2.target,
                                                            satuan: item2.satuan,
                                                        },
                                                        realisasi: {
                                                            kinerja: totalKinerja,
                                                            rp: totalPaguIndikatif,
                                                        }
                                                    });
                                                }

                                                for (const item2 of dataTahun) {
                                                    let isExist = dataTargetDanRealisasi.find((item3) => item3.periode == item2);

                                                    if (!isExist) {
                                                        dataTargetDanRealisasi.push({
                                                            periode: item2,
                                                            target: {
                                                                kinerja: null,
                                                                satuan: null,
                                                            },
                                                            realisasi: {
                                                                kinerja: null,
                                                                rp: null,
                                                            }
                                                        });
                                                    }
                                                }

                                                // sort dataTargetDanRealisasi by periode
                                                dataTargetDanRealisasi.sort((a, b) => a.periode - b.periode);

                                                dataIndikatorSubKegiatan.push({
                                                    nama: item.indikator_kinerja,
                                                    targetAkhirPeriode: {
                                                        kinerja: item.target_akhir_periode,
                                                        satuan: item.satuan,
                                                    },
                                                    targetDanRealisasi: dataTargetDanRealisasi,
                                                });
                                            }

                                            dataSubKegiatan.push({
                                                nama: subKegiatan,
                                                paguIndikatif: item.paguIndikatif,
                                                indikator: dataIndikatorSubKegiatan,
                                            });
                                        }
                                    }

                                    dataKegiatanProgram.push({
                                        nama: kegiatan,
                                        paguIndikatif: item.paguIndikatif,
                                        indikator: dataIndikatorKegiatan,
                                        subKegiatan: dataSubKegiatan,
                                    });
                                }
                            }

                            dataProgramSasaran.push({
                                nama: program,
                                paguIndikatif: item.paguIndikatif,
                                indikator: dataIndikatorProgram,
                                kegiatan: dataKegiatanProgram,
                            });
                        }
                    }

                    dataSasaranTujuan.push({
                        nama: sasaran,
                        paguIndikatif: item.paguIndikatif,
                        indikator: dataIndikatorSasaran,
                        program: dataProgramSasaran,
                    });
                }
            }

            dataEvaluasi.push({
                nama: item.tujuan,
                paguIndikatif: item.paguIndikatif,
                indikator: dataIndikatorTujuan,
                sasaran: dataSasaranTujuan,
            });
        }
    }

    return {
        tahun: dataTahun,
        evaluasi: dataEvaluasi,
    };
}

const excel = async (req) => {
    const idOpd = parseInt(req.params.idOpd);
    const opd = await prismaClient.opdUser.findUnique({
        where: {
            opdId: idOpd
        }
    });

    const dataTahun = [2022, 2023, 2024];

    const path = `${process.cwd()}/public/laporan`;
    const fileName = `Laporan RPKD ${opd.namaOpd}-${Date.now()}.xlsx`;

    const workbook = new Workbook();
    const worksheet = workbook.addWorksheet('Laporan RPKD');

    const vMiddleHCenterAlignment = {vertical: 'middle', horizontal: 'center', wrapText: true};
    const vLeftHCenterAlignment = {vertical: 'middle', horizontal: 'left', wrapText: true};
    const fontBold = {bold: true, size: 10};
    const fontNormal = {size: 10};
    const fontSmall = {size: 8};
    const borderStyle = {
        top: {style: 'thin'},
        left: {style: 'thin'},
        bottom: {style: 'thin'},
        right: {style: 'thin'},
    };

    const applyCellStyle = (cells, font = fontNormal, alignment = vMiddleHCenterAlignment) => {
        for (const cell of cells) {
            let getCell = worksheet.getCell(cell);

            getCell.font = font;
            getCell.border = borderStyle;
            getCell.alignment = alignment;
        }
    }

    const applyStyleToCell = (cell, font = fontNormal, alignment = vMiddleHCenterAlignment) => {
        cell.font = font;
        cell.border = borderStyle;
        cell.alignment = alignment;
    }

    const setCellValue = (cell, value) => {
        let getCell = worksheet.getCell(cell);

        getCell.value = value;
    }

    const getColumnName = (n) => {
        let columnName = '';

        while (n >= 0) {
            columnName = String.fromCharCode((n % 26) + 65) + columnName;
            n = Math.floor(n / 26) - 1;
        }

        return columnName;
    }

    worksheet.addRow([
        'No',
        'Urusan/Bidang Urusan Pemerintahan\r\nDaerah Dan Program/Kegiatan/Sub Kegiatan',
        'Indikator Kinerja Program (outcome)\r\n/Kegiatan (output) / Sub Kegiatan (sub output)',
        'Target Capaian Kinerja RPJMD\r\nTahun 2021-2026 (akhir periode RPJMD)',
        '',
        '',
        'Realisasi Target\r\nKinerja Hasil Program dan Keluaran Kegiatan\r\ns/d Tahun 2021',
        '',
        '',
        'Target dan Realisasi Kinerja Program\r\ndan Keluaran Kegiatan Tahun Lalu\r\nTarget RKPD Tahun ' + dataTahun[0],
        '',
        '',
        '',
        '',
        '',
        '',
        'Target dan Realisasi\r\nProgram/Kegiatan RKPD\r\n ' + dataTahun[1],
        '',
        '',
        '',
        '',
        '',
        '',
        'Target Program atau Kegiatan RKPD\r\nTahun Berjalan\r\nTahun ' + dataTahun[2],
        '',
        '',
        'Perkiraan Realisasi Capaian Target\r\nRPJMD dan RENSTRA s/d\r\nTahun Berjalan',
        '',
        '',
        '',
        '',
        'Perangkat Daerah\r\nPenanggung Jawab',
        'Keterangan'
    ]);

    worksheet.getRow(1).height = 45;
    worksheet.getRow(2).height = 45;

    // baris judul
    worksheet.mergeCells('A1:A2');
    worksheet.mergeCells('B1:B2');
    worksheet.mergeCells('C1:C2');

    worksheet.mergeCells('D1:F2');
    worksheet.mergeCells('G1:I2');

    let aColumn = worksheet.getColumn('A');
    let bColumn = worksheet.getColumn('B');
    let cColumn = worksheet.getColumn('C');
    let dColumn = worksheet.getColumn('D');
    let eColumn = worksheet.getColumn('E');
    let fColumn = worksheet.getColumn('F');
    let gColumn = worksheet.getColumn('G');
    let hColumn = worksheet.getColumn('H');
    let iColumn = worksheet.getColumn('I');
    let jColumn = worksheet.getColumn('J');
    let kColumn = worksheet.getColumn('K');
    let lColumn = worksheet.getColumn('L');
    let mColumn = worksheet.getColumn('M');
    let nColumn = worksheet.getColumn('N');
    let oColumn = worksheet.getColumn('O');
    let pColumn = worksheet.getColumn('P');
    let qColumn = worksheet.getColumn('Q');
    let rColumn = worksheet.getColumn('R');
    let sColumn = worksheet.getColumn('S');
    let tColumn = worksheet.getColumn('T');
    let uColumn = worksheet.getColumn('U');
    let vColumn = worksheet.getColumn('V');
    let wColumn = worksheet.getColumn('W');
    let xColumn = worksheet.getColumn('X');
    let yColumn = worksheet.getColumn('Y');
    let zColumn = worksheet.getColumn('Z');

    let aaColumn = worksheet.getColumn('AA');
    let abColumn = worksheet.getColumn('AB');
    let acColumn = worksheet.getColumn('AC');
    let adColumn = worksheet.getColumn('AD');
    let aeColumn = worksheet.getColumn('AE');

    let afColumn = worksheet.getColumn('AF');
    let agColumn = worksheet.getColumn('AG');

    // A: KOLOM => No
    aColumn.width = 10;
    bColumn.width = 80;
    cColumn.width = 80;
    dColumn.width = 16.29;
    eColumn.width = 11.71;
    fColumn.width = 17.14;
    gColumn.width = 11.71;
    hColumn.width = 11.71;
    iColumn.width = 17.14;

    // tahun 1: J-P
    worksheet.mergeCells('J1:P1');

    jColumn.width = 12.71;
    setCellValue('J2', `Target RKPD\r\nTahun ${dataTahun[0]}`)

    worksheet.mergeCells('J2:L2');

    kColumn.width = 11.86;
    lColumn.width = 19.43;

    mColumn.width = 10.14;
    setCellValue('M2', `Realisasi RKPD\r\nTahun ${dataTahun[0]}`)

    worksheet.mergeCells('M2:N2');

    nColumn.width = 19.71;

    oColumn.width = 16.43;
    setCellValue('O2', `Tingkat Realisasi\r\n(%)`)

    worksheet.mergeCells('O2:P2');

    pColumn.width = 16.71;

    // tahun 2: Q-W
    worksheet.mergeCells('Q1:W1');

    qColumn.width = 12.71;
    setCellValue('Q2', `Target RKPD\r\nTahun ${dataTahun[1]}`)

    worksheet.mergeCells('Q2:S2');

    rColumn.width = 11.86;
    sColumn.width = 19.43;

    tColumn.width = 10.14;
    setCellValue('T2', `Realisasi RKPD\r\nTahun ${dataTahun[1]}`)

    worksheet.mergeCells('T2:U2');

    uColumn.width = 19.71;

    vColumn.width = 16.43;
    setCellValue('V2', `Tingkat Realisasi\r\n(%)`);

    worksheet.mergeCells('V2:W2');

    // tahun 3: X-Z
    worksheet.mergeCells('X1:Z2');

    xColumn.width = 8.29;
    yColumn.width = 8.29;
    zColumn.width = 17.14;

    // AA: KOLOM => Perkiraan Realisasi Capaian Target RPJMD dan RENSTRA s/d Tahun Berjalan
    worksheet.mergeCells('AA1:AE1');

    aaColumn.width = 8.29;

    setCellValue('AA2', 'Realisasi Capaia\r\nProgram dan Kegiatan\r\ns/d Tahun Berjalan');

    worksheet.mergeCells('AA2:AC2');

    abColumn.width = 8.29;
    acColumn.width = 19.43;

    adColumn.width = 11.43;
    setCellValue('AD2', 'Tingkat Capaian\r\nRealisasi Target\r\ns/dTahun Berjalan');

    worksheet.mergeCells('AD2:AE2');

    aeColumn.width = 10.43;
    afColumn.width = 25;

    worksheet.mergeCells('AF1:AF2');

    agColumn.width = 50;

    worksheet.mergeCells('AG1:AG2');

    applyCellStyle(['A1', 'B1', 'C1', 'D1', 'G1', 'J1', 'J2', 'M2', 'O2', 'Q1', 'Q2', 'T2', 'V2', 'X1', 'AD2', 'AF1', 'AG1', 'AA1', 'AA2'], fontBold, vMiddleHCenterAlignment);

    worksheet.addRow([
        1,
        2,
        3,
        4,
        '',
        '',
        5,
        '',
        '',
        6,
        '',
        '',
        7,
        '',
        '8=(7/6)*100',
        '',
        9,
        '',
        '',
        10,
        '',
        '11=(10/9)*100',
        '',
        9,
        '',
        '',
        '10=(5+7+9)',
        '',
        '',
        '11=(10/4)*100',
        '',
        12,
        13,
    ]);

    worksheet.mergeCells('A3:A4');
    worksheet.mergeCells('B3:B4');
    worksheet.mergeCells('C3:C4');
    worksheet.mergeCells('D3:F3');

    setCellValue('D4', 'Kinerja');
    setCellValue('E4', 'Satuan');
    setCellValue('F4', 'Rp');

    worksheet.mergeCells('G3:I3');

    setCellValue('G4', 'Kinerja');
    setCellValue('H4', 'Satuan');
    setCellValue('I4', 'Rp');

    // dataTahun[0]
    // 6
    worksheet.mergeCells('J3:L3');

    setCellValue('J4', 'Kinerja');
    setCellValue('K4', 'Satuan');
    setCellValue('L4', 'Rp');

    worksheet.mergeCells('M3:N3'); // 7

    setCellValue('M4', 'Kinerja');
    setCellValue('N4', 'Rp');

    worksheet.mergeCells('O3:P3'); // 8

    setCellValue('O4', 'Kinerja');
    setCellValue('P4', 'Rp');

    // dataTahun[1]
    // 9
    worksheet.mergeCells('Q3:S3');

    setCellValue('Q4', 'Kinerja');
    setCellValue('R4', 'Satuan');
    setCellValue('S4', 'Rp');

    worksheet.mergeCells('T3:U3'); // 10

    setCellValue('T4', 'Kinerja');
    setCellValue('U4', 'Rp');

    worksheet.mergeCells('V3:W3'); // 11

    setCellValue('V4', 'Kinerja');
    setCellValue('W4', 'Rp');

    worksheet.mergeCells('X3:Z3'); // 9

    setCellValue('X4', 'Kinerja');
    setCellValue('Y4', 'Satuan');
    setCellValue('Z4', 'Rp');

    worksheet.mergeCells('AA3:AC3');  // 10=(5+7+9)

    setCellValue('AA4', 'Kinerja');
    setCellValue('AB4', 'Satuan');
    setCellValue('AC4', 'Rp');

    worksheet.mergeCells('AD3:AE3'); // 11=(10/4)*100

    setCellValue('AD4', 'Kinerja');
    setCellValue('AE4', 'Rp');

    worksheet.mergeCells('AF3:AF4'); // 12
    worksheet.mergeCells('AG3:AG4'); // 13

    applyCellStyle([
        'A3', 'B3', 'C3', 'D3', 'D4', 'E4', 'F4', 'G3', 'G4', 'H4', 'I4', 'J3', 'J4', 'K4', 'L4', 'M3', 'M4', 'N4', 'O3', 'O4', 'P4', 'Q3', 'Q4', 'R4', 'S4',
        'T3', 'T4', 'U4', 'V3', 'V4', 'W4', 'X3', 'X4', 'Y4', 'Z4', 'AA3', 'AA4', 'AB4', 'AC4', 'AD3', 'AD4', 'AE4', 'AF3', 'AG3'
    ]);

    worksheet.addRow([
        '',
        opd.namaOpd,
        '',
        '',
        '',
        0,
        '',
        '',
        0,
        '',
        '',
        0,
        '',
        0,
        '',
        0,
        '',
        '',
        0,
        '',
        0,
        '',
        0,
        '',
        '',
        0,
        0,
        '',
        0,
        '',
        0,
        '',
        '',
    ]);

    let b5 = worksheet.getCell('B5');
    b5.font = fontSmall;
    b5.border = borderStyle;

    applyCellStyle(['F5', 'I5', 'L5', 'N5', 'P5', 'S5', 'U5', 'W5', 'Z5', 'AA5', 'AC5', 'AE5'], fontSmall, vMiddleHCenterAlignment);

    let row5 = worksheet.getRow(5);

    // add border to each cell
    row5.eachCell((cell) => {
        cell.border = borderStyle;
    });

    // get data
    let data = await evaluasi(req);
    let dataEvaluasi = data.evaluasi;

    let i = 0;
    let leftColumns = ['B', 'C'];
    let centerColumns = ['A', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'AA', 'AB', 'AC', 'AD', 'AE'];

    for (const evaluasi of dataEvaluasi) {
        let dataIndikator = evaluasi.indikator;

        let rowData = [
            i + 1,
            `<b>Tujuan</b>: ${evaluasi.nama}`,
            dataIndikator[0].nama,
            dataIndikator[0].targetAkhirPeriode.kinerja,
            dataIndikator[0].targetAkhirPeriode.satuan,
            evaluasi.paguIndikatif ?? '',
            '',
            '',
            '',
        ];

        let j = 0;
        for (const targetDanRealisasi of dataIndikator[0].targetDanRealisasi) {
            if (j !== dataIndikator[0].targetDanRealisasi.length - 1) {
                rowData.push(targetDanRealisasi.target.kinerja ?? '');
                rowData.push(targetDanRealisasi.target.satuan ?? '');
                rowData.push(evaluasi.paguIndikatif ?? '');

                rowData.push(targetDanRealisasi.realisasi.kinerja ?? '');
                rowData.push(targetDanRealisasi.realisasi.rp ?? '');

                rowData.push('');
                rowData.push('');
            } else {
                rowData.push(targetDanRealisasi.target.kinerja ?? '');
                rowData.push(targetDanRealisasi.target.satuan ?? '');
                rowData.push(targetDanRealisasi.target.paguIndikatif ?? '');
            }

            j++;
        }

        rowData.push('');
        rowData.push('');
        rowData.push('');

        rowData.push('');
        rowData.push('');

        rowData.push('');
        rowData.push('');

        worksheet.addRow(rowData);

        if (dataIndikator.length > 1) {
            for (let j = 1; j < dataIndikator.length; j++) {
                let rowData = [
                    '',
                    '',
                    dataIndikator[j].nama,
                    dataIndikator[j].targetAkhirPeriode.kinerja,
                    dataIndikator[j].targetAkhirPeriode.satuan,
                    evaluasi.paguIndikatif,
                    '',
                    '',
                    '',
                ];

                let l = 0;
                for (const targetDanRealisasi of dataIndikator[j].targetDanRealisasi) {
                    if (l !== dataIndikator[j].targetDanRealisasi.length - 1) {
                        rowData.push(targetDanRealisasi.target.kinerja ?? '');
                        rowData.push(targetDanRealisasi.target.satuan ?? '');
                        rowData.push(evaluasi.paguIndikatif ?? '');

                        rowData.push(targetDanRealisasi.realisasi.kinerja ?? '');
                        rowData.push(targetDanRealisasi.realisasi.rp ?? '');

                        rowData.push('');
                        rowData.push('');
                    } else {
                        rowData.push(targetDanRealisasi.target.kinerja ?? '');
                        rowData.push(targetDanRealisasi.target.satuan ?? '');
                        rowData.push(targetDanRealisasi.target.paguIndikatif ?? '');
                    }

                    l++;
                }

                rowData.push('');
                rowData.push('');
                rowData.push('');

                rowData.push('');
                rowData.push('');

                rowData.push('');
                rowData.push('');

                worksheet.addRow(rowData);
            }
        }

        let dataSasaran = evaluasi.sasaran;
        let s = 0;

        for (const sasaran of dataSasaran) {
            let dataIndikator = sasaran.indikator;

            let rowData = [
                `${i+1}.${i+1}`,
                `<b>Sasaran</b>: ${sasaran.nama}`,
                dataIndikator[0].nama,
                dataIndikator[0].targetAkhirPeriode.kinerja,
                dataIndikator[0].targetAkhirPeriode.satuan,
                sasaran.paguIndikatif ?? '',
                '',
                '',
                '',
            ];

            let j = 0;
            for (const targetDanRealisasi of dataIndikator[0].targetDanRealisasi) {
                if (j !== dataIndikator[0].targetDanRealisasi.length - 1) {
                    rowData.push(targetDanRealisasi.target.kinerja ?? '');
                    rowData.push(targetDanRealisasi.target.satuan ?? '');
                    rowData.push(formatNumber(sasaran.paguIndikatif) ?? '');

                    rowData.push(targetDanRealisasi.realisasi.kinerja ?? '');
                    rowData.push(targetDanRealisasi.realisasi.rp ?? '');

                    rowData.push('');
                    rowData.push('');
                } else {
                    rowData.push(targetDanRealisasi.target.kinerja ?? '');
                    rowData.push(targetDanRealisasi.target.satuan ?? '');
                    rowData.push(targetDanRealisasi.target.paguIndikatif ?? '');
                }

                j++;
            }

            rowData.push('');
            rowData.push('');
            rowData.push('');

            rowData.push('');
            rowData.push('');

            rowData.push('');
            rowData.push('');

            worksheet.addRow(rowData);

            if (dataIndikator.length > 1) {
                for (let j = 1; j < dataIndikator.length; j++) {
                    let rowData = [
                        '',
                        '',
                        dataIndikator[j].nama,
                        dataIndikator[j].targetAkhirPeriode.kinerja,
                        dataIndikator[j].targetAkhirPeriode.satuan,
                        formatNumber(sasaran.paguIndikatif),
                        '',
                        '',
                        '',
                    ];

                    let l = 0;
                    for (const targetDanRealisasi of dataIndikator[j].targetDanRealisasi) {
                        if (l !== dataIndikator[j].targetDanRealisasi.length - 1) {
                            rowData.push(targetDanRealisasi.target.kinerja ?? '');
                            rowData.push(targetDanRealisasi.target.satuan ?? '');
                            rowData.push(formatNumber(sasaran.paguIndikatif) ?? '');

                            rowData.push(targetDanRealisasi.realisasi.kinerja ?? '');
                            rowData.push(formatNumber(targetDanRealisasi.realisasi.rp) ?? '');

                            rowData.push('');
                            rowData.push('');
                        } else {
                            rowData.push(targetDanRealisasi.target.kinerja ?? '');
                            rowData.push(targetDanRealisasi.target.satuan ?? '');
                            rowData.push(formatNumber(targetDanRealisasi.target.paguIndikatif) ?? '');
                        }

                        l++;
                    }

                    rowData.push('');
                    rowData.push('');
                    rowData.push('');

                    rowData.push('');
                    rowData.push('');

                    rowData.push('');
                    rowData.push('');

                    worksheet.addRow(rowData);
                }
            }

            let dataProgram = sasaran.program;
            let p = 0;

            for (const program of dataProgram) {
                let dataIndikator = program.indikator;

                let rowData = [
                    `${i+1}.${i+1}.${s+1}`,
                    `<b>Program</b>: ${program.nama}`,
                    dataIndikator[0].nama,
                    dataIndikator[0].targetAkhirPeriode.kinerja,
                    dataIndikator[0].targetAkhirPeriode.satuan,
                    formatNumber(program.paguIndikatif) ?? '',
                    '',
                    '',
                    '',
                ];

                let j = 0;
                for (const targetDanRealisasi of dataIndikator[0].targetDanRealisasi) {
                    if (j !== dataIndikator[0].targetDanRealisasi.length - 1) {
                        rowData.push(targetDanRealisasi.target.kinerja ?? '');
                        rowData.push(targetDanRealisasi.target.satuan ?? '');
                        rowData.push(formatNumber(program.paguIndikatif) ?? '');

                        rowData.push(targetDanRealisasi.realisasi.kinerja ?? '');
                        rowData.push(formatNumber(targetDanRealisasi.realisasi.rp) ?? '');

                        rowData.push('');
                        rowData.push('');
                    } else {
                        rowData.push(targetDanRealisasi.target.kinerja ?? '');
                        rowData.push(targetDanRealisasi.target.satuan ?? '');
                        rowData.push(formatNumber(targetDanRealisasi.target.paguIndikatif) ?? '');
                    }

                    j++;
                }

                rowData.push('');
                rowData.push('');
                rowData.push('');

                rowData.push('');
                rowData.push('');

                rowData.push('');
                rowData.push('');

                worksheet.addRow(rowData);

                if (dataIndikator.length > 1) {
                    for (let j = 1; j < dataIndikator.length; j++) {
                        let rowData = [
                            '',
                            '',
                            dataIndikator[j].nama,
                            dataIndikator[j].targetAkhirPeriode.kinerja,
                            dataIndikator[j].targetAkhirPeriode.satuan,
                           formatNumber( program.paguIndikatif),
                            '',
                            '',
                            '',
                        ];

                        let l = 0;
                        for (const targetDanRealisasi of dataIndikator[j].targetDanRealisasi) {
                            if (l !== dataIndikator[j].targetDanRealisasi.length - 1) {
                                rowData.push(targetDanRealisasi.target.kinerja ?? '');
                                rowData.push(targetDanRealisasi.target.satuan ?? '');
                                rowData.push(formatNumber(program.paguIndikatif) ?? '');

                                rowData.push(targetDanRealisasi.realisasi.kinerja ?? '');
                                rowData.push(formatNumber(targetDanRealisasi.realisasi.rp) ?? '');

                                rowData.push('');
                                rowData.push('');
                            } else {
                                rowData.push(targetDanRealisasi.target.kinerja ?? '');
                                rowData.push(targetDanRealisasi.target.satuan ?? '');
                                rowData.push(formatNumber(targetDanRealisasi.target.paguIndikatif) ?? '');
                            }

                            l++;
                        }

                        rowData.push('');
                        rowData.push('');
                        rowData.push('');

                        rowData.push('');
                        rowData.push('');

                        rowData.push('');
                        rowData.push('');

                        worksheet.addRow(rowData);
                    }
                }

                let dataKegiatan = program.kegiatan;
                let k = 0;

                for (const kegiatan of dataKegiatan) {
                    let dataIndikator = kegiatan.indikator;

                    let rowData = [
                        `${i+1}.${i+1}.${s+1}.${p+1}`,
                        `<b>Kegiatan</b>: ${kegiatan.nama}`,
                        dataIndikator[0].nama,
                        dataIndikator[0].targetAkhirPeriode.kinerja,
                        dataIndikator[0].targetAkhirPeriode.satuan,
                        formatNumber(kegiatan.paguIndikatif) ?? '',
                        '',
                        '',
                        '',
                    ];

                    let j = 0;
                    for (const targetDanRealisasi of dataIndikator[0].targetDanRealisasi) {
                        if (j !== dataIndikator[0].targetDanRealisasi.length - 1) {
                            rowData.push(targetDanRealisasi.target.kinerja ?? '');
                            rowData.push(targetDanRealisasi.target.satuan ?? '');
                            rowData.push(formatNumber(kegiatan.paguIndikatif) ?? '');

                            rowData.push(targetDanRealisasi.realisasi.kinerja ?? '');
                            rowData.push(formatNumber(targetDanRealisasi.realisasi.rp) ?? '');

                            rowData.push('');
                            rowData.push('');
                        } else {
                            rowData.push(targetDanRealisasi.target.kinerja ?? '');
                            rowData.push(targetDanRealisasi.target.satuan ?? '');
                            rowData.push(formatNumber(targetDanRealisasi.target.paguIndikatif) ?? '');
                        }

                        j++;
                    }

                    rowData.push('');
                    rowData.push('');
                    rowData.push('');

                    rowData.push('');
                    rowData.push('');

                    rowData.push('');
                    rowData.push('');

                    worksheet.addRow(rowData);

                    if (dataIndikator.length > 1) {
                        for (let j = 1; j < dataIndikator.length; j++) {
                            let rowData = [
                                '',
                                '',
                                dataIndikator[j].nama,
                                dataIndikator[j].targetAkhirPeriode.kinerja,
                                dataIndikator[j].targetAkhirPeriode.satuan,
                                formatNumber(kegiatan.paguIndikatif),
                                '',
                                '',
                                '',
                            ];

                            let l = 0;
                            for (const targetDanRealisasi of dataIndikator[j].targetDanRealisasi) {
                                if (l !== dataIndikator[j].targetDanRealisasi.length - 1) {
                                    rowData.push(targetDanRealisasi.target.kinerja ?? '');
                                    rowData.push(targetDanRealisasi.target.satuan ?? '');
                                    rowData.push(formatNumber(kegiatan.paguIndikatif) ?? '');

                                    rowData.push(targetDanRealisasi.realisasi.kinerja ?? '');
                                    rowData.push(formatNumber(targetDanRealisasi.realisasi.rp) ?? '');

                                    rowData.push('');
                                    rowData.push('');
                                } else {
                                    rowData.push(targetDanRealisasi.target.kinerja ?? '');
                                    rowData.push(targetDanRealisasi.target.satuan ?? '');
                                    rowData.push(formatNumber(targetDanRealisasi.target.paguIndikatif) ?? '');
                                }

                                l++;
                            }

                            rowData.push('');
                            rowData.push('');
                            rowData.push('');

                            rowData.push('');
                            rowData.push('');

                            rowData.push('');
                            rowData.push('');

                            worksheet.addRow(rowData);
                        }
                    }

                    let dataSubKegiatan = kegiatan.subKegiatan;
                    let sub = 0;

                    for (const subKegiatan of dataSubKegiatan) {
                        let dataIndikator = subKegiatan.indikator;

                        let rowData = [
                            `${i+1}.${i+1}.${s+1}.${p+1}.${k+1}.${sub+1}`,
                            `<b>Sub Kegiatan</b>: ${subKegiatan.nama}`,
                            dataIndikator[0].nama,
                            dataIndikator[0].targetAkhirPeriode.kinerja,
                            dataIndikator[0].targetAkhirPeriode.satuan,
                            formatNumber(subKegiatan.paguIndikatif) ?? '',
                            '',
                            '',
                            '',
                        ];

                        let j = 0;
                        for (const targetDanRealisasi of dataIndikator[0].targetDanRealisasi) {
                            if (j !== dataIndikator[0].targetDanRealisasi.length - 1) {
                                rowData.push(targetDanRealisasi.target.kinerja ?? '');
                                rowData.push(targetDanRealisasi.target.satuan ?? '');
                                rowData.push(formatNumber(subKegiatan.paguIndikatif) ?? '');

                                rowData.push(targetDanRealisasi.realisasi.kinerja ?? '');
                                rowData.push(formatNumber(targetDanRealisasi.realisasi.rp) ?? '');

                                rowData.push('');
                                rowData.push('');
                            } else {
                                rowData.push(targetDanRealisasi.target.kinerja ?? '');
                                rowData.push(targetDanRealisasi.target.satuan ?? '');
                                rowData.push(formatNumber(targetDanRealisasi.target.paguIndikatif) ?? '');
                            }

                            j++;
                        }

                        rowData.push('');
                        rowData.push('');
                        rowData.push('');

                        rowData.push('');
                        rowData.push('');

                        rowData.push('');
                        rowData.push('');

                        worksheet.addRow(rowData);

                        if (dataIndikator.length > 1) {
                            for (let j = 1; j < dataIndikator.length; j++) {
                                let rowData = [
                                    '',
                                    '',
                                    dataIndikator[j].nama,
                                    dataIndikator[j].targetAkhirPeriode.kinerja,
                                    dataIndikator[j].targetAkhirPeriode.satuan,
                                    formatNumber(subKegiatan.paguIndikatif),
                                    '',
                                    '',
                                    '',
                                ];

                                let l = 0;
                                for (const targetDanRealisasi of dataIndikator[j].targetDanRealisasi) {
                                    if (l !== dataIndikator[j].targetDanRealisasi.length - 1) {
                                        rowData.push(targetDanRealisasi.target.kinerja ?? '');
                                        rowData.push(targetDanRealisasi.target.satuan ?? '');
                                        rowData.push(formatNumber(subKegiatan.paguIndikatif) ?? '');

                                        rowData.push(targetDanRealisasi.realisasi.kinerja ?? '');
                                        rowData.push(formatNumber(targetDanRealisasi.realisasi.rp) ?? '');

                                        rowData.push('');
                                        rowData.push('');
                                    } else {
                                        rowData.push(targetDanRealisasi.target.kinerja ?? '');
                                        rowData.push(targetDanRealisasi.target.satuan ?? '');
                                        rowData.push(formatNumber(targetDanRealisasi.target.paguIndikatif) ?? '');
                                    }

                                    l++;
                                }

                                rowData.push('');
                                rowData.push('');
                                rowData.push('');

                                rowData.push('');
                                rowData.push('');

                                rowData.push('');
                                rowData.push('');

                                worksheet.addRow(rowData);
                            }
                        }

                        sub++;
                    }
                }
            }

            s++;
        }

        i++;
    }

    let lastRowNumber = worksheet.rowCount;

    for (let i = 6; i <= lastRowNumber; i++) {
        let row = worksheet.getRow(i);
        row.height = 45;

        row.eachCell((cell) => {
           let columnName = cell._column.letter;
           let cellValue = cell.value;

           if (cell.col === 2) {
               cell.value = {
                   richText: htmlToRichText(cellValue, {size: 10}),
               }
           }

           if (centerColumns.includes(columnName)){
               applyStyleToCell(cell, fontNormal, vMiddleHCenterAlignment);
           }
           else {
                applyStyleToCell(cell, fontNormal, vLeftHCenterAlignment);
           }
        });
    }

    try {
        await fs.promises.mkdir(path, {recursive: true});

        //remove old file
        const files = fs.readdirSync(path);
        for (const file of files) {
            try {
                fs.unlinkSync(`${path}/${file}`);
            } catch (err) {

            }
        }

        await workbook.xlsx.writeFile(`${path}/${fileName}`);

        return {
            code: 201,
            success: true,
            message: 'Berhasil membuat file laporan',
            data: {
                file_name: fileName,
            },
        }
    } catch (err) {
        console.error('Error:', err.message);

        return {
            code: 500,
            success: false,
            message: 'Gagal membuat file laporan',
            error: err.message,
        }
    }
}

export default {
    evaluasi,
    excel,
};