import pkg from 'exceljs';
import * as fs from "fs";
import {htmlToRichText} from "../utils/excel/html-converter.js";
import {formatNumber, formatRupiah} from "../utils/util.js";
import {PrismaClient} from "@prisma/client";
import moment from "moment";

moment.locale('id');

const prisma = new PrismaClient()
process.env.TZ = 'Asia/Jakarta'

const {Workbook} = pkg;

const renja = async (req, data) => {
    const tahun = parseInt(req.params.tahun);
    const opdId = parseInt(req.params.opd);

    const opd = await prisma.opdUser.findUnique({
        where: {
            opdId: opdId
        }
    });

    const path = `${process.cwd()}/public/laporan`;
    const fileName = `Laporan Evaluasi Renja Perangkat Daerah ${opd.namaOpd} Tahun ${tahun}-${Date.now()}.xlsx`;
    // const fileName = `file-${Date.now()}.xlsx`;
    const workbook = new Workbook();

    const alignmentStyle = {vertical: 'middle', horizontal: 'center', wrapText: true};
    const textAlignmentStyle = {vertical: 'middle', horizontal: 'left', wrapText: true};
    const borderStyle = {
        top: {style: 'thin'},
        left: {style: 'thin'},
        bottom: {style: 'thin'},
        right: {style: 'thin'},
    };
    const fillStyle = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: {argb: 'FFeef3ff'},
    };

    const worksheet = workbook.addWorksheet('Laporan');

    const headers = [
        {
            header: `Tabel Hasil\r
Evaluasi Renja Perangkat Daerah ${opd.namaOpd}\r
Tahun ${tahun}`, key: 'kop'
        },
    ]

    worksheet.columns = headers;

    worksheet.mergeCells('A1:H1');

    worksheet.getCell('A1').alignment = alignmentStyle;
    worksheet.getCell('A1').font = {bold: true, size: 16};

    const row1 = worksheet.getRow(1);
    row1.height = 100;

    // add row
    worksheet.addRow([
        'Kode',
        'Tujuan/Sasaran/Program\r\n/Kegiatan/Sub Kegiatan',
        'Indikator Kinerja\r\nTujuan, Sasaran,\r\nProgram, Kegiatan dan Sub Kegiatan',
        'Target dan Kinerja Anggaran Tahun 2023',
        '',
        '',
        'Bobot',
        'Triwulan  I',
        '',
        '',
        '',
        '',
        '',
        'Triwulan  II',
        '',
        '',
        '',
        '',
        '',
        'Triwulan  III',
        '',
        '',
        '',
        '',
        '',
        'Triwulan  IV',
        '',
        '',
        '',
        '',
        '',
        'Capaian Kinerja dan Realiasasi Anggaran Triwulan I s.d IV\r\nRenja Perangkat Daerah Tahun 2023',
        '',
        '',
        '',
        '',
        'Outcome, Output, Sub Output',
        '',
        '',
        'Faktor Pendorong\r\nKeberhasilan Kinerja',
        'Faktor Penghambat\r\nPencapaian Kinerja',
        'Rekomendasi Tindak Lanjut\r\nYang Diperlukan Dalam\r\nTriwulan Berikutnya'
    ]);

    worksheet.mergeCells('A2:A4'); //kode
    worksheet.mergeCells('B2:B4'); //tujuan/sasaran/program/kegiatan/sub kegiatan
    worksheet.mergeCells('C2:C4'); //indikator kinerja tujuan, sasaran, program, kegiatan dan sub kegiatan

    worksheet.getCell('A2').border = borderStyle;
    worksheet.getCell('B2').border = borderStyle;
    worksheet.getCell('C2').border = borderStyle;

    //target dan kinerja anggaran tahun 2023
    worksheet.mergeCells('D2:F2');
    worksheet.getColumn('D').width = 15;
    worksheet.getColumn('E').width = 15;
    worksheet.getCell('D2').alignment = alignmentStyle;
    // worksheet.getCell('D2').fill = fillStyle
    worksheet.getCell('D2').border = borderStyle;


    // kinerja
    const row3 = worksheet.getRow(3);
    row3.splice(4, 1, 'Kinerja');
    worksheet.mergeCells('D3:E3');
    worksheet.getCell('D3').alignment = alignmentStyle;
    worksheet.getCell('D3').font = {bold: true};
    worksheet.getCell('D3').border = borderStyle;

    row3.height = 40;

    row3.splice(6, 1, 'Pagu Indikatif (Rp)');
    worksheet.getColumn('F').width = 30;
    worksheet.getCell('F3').alignment = alignmentStyle;
    worksheet.getCell('F3').font = {bold: true};
    worksheet.mergeCells('F3:F4');
    worksheet.getCell('F3').border = borderStyle;

    // target dan satuan
    const row4 = worksheet.getRow(4);
    row4.splice(4, 2, 'Target', 'Satuan');

    row4.height = 40;

    worksheet.getCell('D4').alignment = alignmentStyle;
    worksheet.getCell('D4').font = {bold: true};
    worksheet.getCell('D4').border = borderStyle;

    worksheet.getCell('E4').alignment = alignmentStyle;
    worksheet.getCell('E4').font = {bold: true};
    worksheet.getCell('E4').border = borderStyle;

    worksheet.getCell('A2').alignment = alignmentStyle;
    worksheet.getCell('B2').alignment = alignmentStyle;
    worksheet.getCell('C2').alignment = alignmentStyle;

    worksheet.getRow(2).font = {bold: true};
    worksheet.getRow(2).height = 40;

    worksheet.getColumn('A').width = 20;
    worksheet.getColumn('B').width = 80;
    worksheet.getColumn('C').width = 80;

    // bobot
    worksheet.mergeCells('G2:G3');
    worksheet.getCell('G2').alignment = alignmentStyle;
    // worksheet.getCell('G2').fill = fillStyle;
    worksheet.getCell('G2').border = borderStyle;

    row4.splice(7, 1, '(%)');
    worksheet.getCell('G4').alignment = alignmentStyle;
    worksheet.getCell('G4').font = {bold: true};
    worksheet.getColumn('G').width = 20;
    worksheet.getCell('G4').border = borderStyle;

    // triwulan 1
    worksheet.mergeCells('H2:M2');
    worksheet.getCell('H2').border = borderStyle;

    // target kinerja
    row3.splice(8, 1, 'Target Kinerja');
    worksheet.getCell('H2').alignment = alignmentStyle;
    worksheet.mergeCells('H3:H4');
    worksheet.getColumn('H').width = 20;
    worksheet.getCell('H3').alignment = alignmentStyle;
    worksheet.getCell('H3').font = {bold: true};
    worksheet.getCell('H3').border = borderStyle;

    // satuan
    row3.splice(9, 1, 'Satuan');
    worksheet.getCell('I2').alignment = alignmentStyle;
    worksheet.mergeCells('I3:I4');
    worksheet.getColumn('I').width = 20;
    worksheet.getCell('I3').alignment = alignmentStyle;
    worksheet.getCell('I3').font = {bold: true};
    worksheet.getCell('I3').border = borderStyle;

    // realisasi kinerja
    row3.splice(10, 1, 'Realisasi Kinerja');
    worksheet.getCell('J2').alignment = alignmentStyle;
    worksheet.mergeCells('J3:J4');
    worksheet.getColumn('J').width = 20;
    worksheet.getCell('J3').alignment = alignmentStyle;
    worksheet.getCell('J3').font = {bold: true};
    worksheet.getCell('J3').border = borderStyle;

    // Satuan(%)
    row3.splice(11, 1, 'Satuan');
    worksheet.getCell('K2').alignment = alignmentStyle;
    worksheet.mergeCells('K3:K4');
    worksheet.getColumn('K').width = 20;
    worksheet.getCell('K3').alignment = alignmentStyle;
    worksheet.getCell('K3').font = {bold: true};
    worksheet.getCell('K3').border = borderStyle;

    worksheet.mergeCells('L3:M3');
    row3.splice(12, 1, 'Realisasi Anggaran TW I');
    row4.splice(12, 1, 'Rp');
    row4.splice(13, 1, '(%)');
    worksheet.getColumn('L').width = 15;
    worksheet.getColumn('M').width = 15;
    worksheet.getCell('L3').alignment = alignmentStyle;
    worksheet.getCell('L3').font = {bold: true};
    worksheet.getCell('L4').alignment = alignmentStyle;
    worksheet.getCell('L4').font = {bold: true};
    worksheet.getCell('M4').alignment = alignmentStyle;
    worksheet.getCell('M4').font = {bold: true};

    worksheet.getCell('L3').border = borderStyle;
    worksheet.getCell('L4').border = borderStyle;
    worksheet.getCell('M3').border = borderStyle;
    worksheet.getCell('M4').border = borderStyle;

    // triwulan 2
    worksheet.mergeCells('N2:S2');
    worksheet.getCell('N2').border = borderStyle;

    // target kinerja
    row3.splice(14, 1, 'Target Kinerja');
    worksheet.getCell('N2').alignment = alignmentStyle;
    worksheet.mergeCells('N3:N4');
    worksheet.getColumn('N').width = 20;
    worksheet.getCell('N3').alignment = alignmentStyle;
    worksheet.getCell('N3').font = {bold: true};
    worksheet.getCell('N3').border = borderStyle;

    // satuan
    row3.splice(15, 1, 'Satuan');
    worksheet.getCell('O2').alignment = alignmentStyle;
    worksheet.mergeCells('O3:O4');
    worksheet.getColumn('O').width = 20;
    worksheet.getCell('O3').alignment = alignmentStyle;
    worksheet.getCell('O3').font = {bold: true};
    worksheet.getCell('O3').border = borderStyle;

    // realisasi kinerja
    row3.splice(16, 1, 'Realisasi Kinerja');
    worksheet.getCell('P2').alignment = alignmentStyle;
    worksheet.mergeCells('P3:P4');
    worksheet.getColumn('P').width = 20;
    worksheet.getCell('P3').alignment = alignmentStyle;
    worksheet.getCell('P3').font = {bold: true};
    worksheet.getCell('P3').border = borderStyle;

    // Satuan(%)
    row3.splice(17, 1, 'Satuan');
    worksheet.getCell('Q2').alignment = alignmentStyle;
    worksheet.mergeCells('Q3:Q4');
    worksheet.getColumn('Q').width = 20;
    worksheet.getCell('Q3').alignment = alignmentStyle;
    worksheet.getCell('Q3').font = {bold: true};
    worksheet.getCell('Q3').border = borderStyle;

    worksheet.mergeCells('R3:S3');
    row3.splice(18, 1, 'Realisasi Anggaran TW II');
    row4.splice(18, 1, 'Rp');
    row4.splice(19, 1, '(%)');
    worksheet.getColumn('R').width = 15;
    worksheet.getColumn('S').width = 15;
    worksheet.getCell('R3').alignment = alignmentStyle;
    worksheet.getCell('R3').font = {bold: true};
    worksheet.getCell('R4').alignment = alignmentStyle;
    worksheet.getCell('R4').font = {bold: true};
    worksheet.getCell('S4').alignment = alignmentStyle;
    worksheet.getCell('S4').font = {bold: true};

    worksheet.getCell('R3').border = borderStyle;
    worksheet.getCell('R4').border = borderStyle;
    worksheet.getCell('S3').border = borderStyle;
    worksheet.getCell('S4').border = borderStyle;

    // triwulan 3
    worksheet.mergeCells('T2:Y2');
    worksheet.getCell('T2').border = borderStyle;

    // target kinerja
    row3.splice(20, 1, 'Target Kinerja');
    worksheet.getCell('T2').alignment = alignmentStyle;
    worksheet.mergeCells('T3:T4');
    worksheet.getColumn('T').width = 20;
    worksheet.getCell('T3').alignment = alignmentStyle;
    worksheet.getCell('T3').font = {bold: true};
    worksheet.getCell('T3').border = borderStyle;

    // satuan
    row3.splice(21, 1, 'Satuan');
    worksheet.getCell('U2').alignment = alignmentStyle;
    worksheet.mergeCells('U3:U4');
    worksheet.getColumn('U').width = 20;
    worksheet.getCell('U3').alignment = alignmentStyle;
    worksheet.getCell('U3').font = {bold: true};
    worksheet.getCell('U3').border = borderStyle;

    // realisasi kinerja
    row3.splice(22, 1, 'Realisasi Kinerja');
    worksheet.getCell('V2').alignment = alignmentStyle;
    worksheet.mergeCells('V3:V4');
    worksheet.getColumn('V').width = 20;
    worksheet.getCell('V3').alignment = alignmentStyle;
    worksheet.getCell('V3').font = {bold: true};
    worksheet.getCell('V3').border = borderStyle;

    // Satuan(%)
    row3.splice(23, 1, 'Satuan');
    worksheet.getCell('W2').alignment = alignmentStyle;
    worksheet.mergeCells('W3:W4');
    worksheet.getColumn('W').width = 20;
    worksheet.getCell('W3').alignment = alignmentStyle;
    worksheet.getCell('W3').font = {bold: true};
    worksheet.getCell('W3').border = borderStyle;

    worksheet.mergeCells('X3:Y3');
    row3.splice(24, 1, 'Realisasi Anggaran TW III');
    row4.splice(24, 1, 'Rp');
    row4.splice(25, 1, '(%)');
    worksheet.getColumn('X').width = 15;
    worksheet.getColumn('Y').width = 15;
    worksheet.getCell('X3').alignment = alignmentStyle;
    worksheet.getCell('X3').font = {bold: true};
    worksheet.getCell('X4').alignment = alignmentStyle;
    worksheet.getCell('X4').font = {bold: true};
    worksheet.getCell('Y4').alignment = alignmentStyle;
    worksheet.getCell('Y4').font = {bold: true};

    worksheet.getCell('X3').border = borderStyle;
    worksheet.getCell('X4').border = borderStyle;
    worksheet.getCell('Y3').border = borderStyle;
    worksheet.getCell('Y4').border = borderStyle;

    // triwulan 4
    worksheet.mergeCells('Z2:AE2');
    worksheet.getCell('Z2').border = borderStyle;

    // target kinerja
    row3.splice(26, 1, 'Target Kinerja');
    worksheet.getCell('Z2').alignment = alignmentStyle;
    worksheet.mergeCells('Z3:Z4');
    worksheet.getColumn('Z').width = 20;
    worksheet.getCell('Z3').alignment = alignmentStyle;
    worksheet.getCell('Z3').font = {bold: true};
    worksheet.getCell('Z3').border = borderStyle;

    // satuan
    row3.splice(27, 1, 'Satuan');
    worksheet.getCell('AA2').alignment = alignmentStyle;
    worksheet.mergeCells('AA3:AA4');
    worksheet.getColumn('AA').width = 20;
    worksheet.getCell('AA3').alignment = alignmentStyle;
    worksheet.getCell('AA3').font = {bold: true};
    worksheet.getCell('AA3').border = borderStyle;

    // realisasi kinerja
    row3.splice(28, 1, 'Realisasi Kinerja');
    worksheet.getCell('AB2').alignment = alignmentStyle;
    worksheet.mergeCells('AB3:AB4');
    worksheet.getColumn('AB').width = 20;
    worksheet.getCell('AB3').alignment = alignmentStyle;
    worksheet.getCell('AB3').font = {bold: true};
    worksheet.getCell('AB3').border = borderStyle;

    // Satuan(%)
    row3.splice(29, 1, 'Satuan');
    worksheet.getCell('AC2').alignment = alignmentStyle;
    worksheet.mergeCells('AC3:AC4');
    worksheet.getColumn('AC').width = 20;
    worksheet.getCell('AC3').alignment = alignmentStyle;
    worksheet.getCell('AC3').font = {bold: true};
    worksheet.getCell('AC3').border = borderStyle;

    worksheet.mergeCells('AD3:AE3');
    row3.splice(30, 1, 'Realisasi Anggaran TW IV');
    row4.splice(30, 1, 'Rp');
    row4.splice(31, 1, '(%)');
    worksheet.getColumn('AD').width = 15;
    worksheet.getColumn('AE').width = 15;
    worksheet.getCell('AD3').alignment = alignmentStyle;
    worksheet.getCell('AD3').font = {bold: true};
    worksheet.getCell('AD4').alignment = alignmentStyle;
    worksheet.getCell('AD4').font = {bold: true};
    worksheet.getCell('AE4').alignment = alignmentStyle;
    worksheet.getCell('AE4').font = {bold: true};

    worksheet.getCell('AD3').border = borderStyle;
    worksheet.getCell('AD4').border = borderStyle;
    worksheet.getCell('AE3').border = borderStyle;
    worksheet.getCell('AE4').border = borderStyle;

    // capaian kinerja dan realisasi anggaran triwulan I s.d IV
    worksheet.mergeCells('AF2:AJ2');
    worksheet.getCell('AF2').alignment = alignmentStyle;
    worksheet.getCell('AF2').font = {bold: true};
    // worksheet.getCell('AF2').fill = fillStyle;
    worksheet.getCell('AF2').border = borderStyle;

    // capaian kinerja
    row3.splice(32, 1, 'Capaian Kinerja');
    worksheet.getCell('AF3').alignment = alignmentStyle;
    worksheet.mergeCells('AF3:AF4');
    worksheet.getColumn('AF').width = 20;
    worksheet.getCell('AF3').alignment = alignmentStyle;
    worksheet.getCell('AF3').font = {bold: true};
    worksheet.getCell('AF3').border = borderStyle;

    // satuan
    row3.splice(33, 1, 'Satuan');
    worksheet.getCell('AG2').alignment = alignmentStyle;
    worksheet.mergeCells('AG3:AG4');
    worksheet.getColumn('AG').width = 20;
    worksheet.getCell('AG3').alignment = alignmentStyle;
    worksheet.getCell('AG3').font = {bold: true};
    worksheet.getCell('AG3').border = borderStyle;

    //realisasi anggaran
    row3.splice(34, 1, 'Realisasi Anggaran');
    worksheet.getCell('AH2').alignment = alignmentStyle;
    worksheet.mergeCells('AH3:AI3');
    worksheet.getCell('AH3').alignment = alignmentStyle;
    worksheet.getCell('AH3').font = {bold: true};
    worksheet.getCell('AH3').border = borderStyle;

    // rp
    row4.splice(34, 1, 'Rp');
    worksheet.getCell('AH4').alignment = alignmentStyle;
    worksheet.getCell('AH4').font = {bold: true};
    worksheet.getColumn('AH').width = 15;
    worksheet.getCell('AH4').border = borderStyle;

    // %
    row4.splice(35, 1, '(%)');
    worksheet.getCell('AI4').alignment = alignmentStyle;
    worksheet.getCell('AI4').font = {bold: true};
    worksheet.getColumn('AI').width = 15;
    worksheet.getCell('AI4').border = borderStyle;

    // satuan
    row3.splice(36, 1, 'Satuan');
    worksheet.getCell('AJ2').alignment = alignmentStyle;
    worksheet.mergeCells('AJ3:AJ4');
    worksheet.getColumn('AJ').width = 20;
    worksheet.getCell('AJ3').alignment = alignmentStyle;
    worksheet.getCell('AJ3').font = {bold: true};
    worksheet.getCell('AJ3').border = borderStyle;

    // outcome, output, sub output
    worksheet.mergeCells('AK2:AM2');
    worksheet.getCell('AK2').alignment = alignmentStyle;
    worksheet.getCell('AK2').font = {bold: true};
    // worksheet.getCell('AK2').fill = {
    //     type: 'pattern',
    //     pattern: 'solid',
    //     fgColor: {argb: 'FFeef3ff'},
    // };
    worksheet.getCell('AK2').border = borderStyle;

    //kinerja
    row3.splice(37, 1, 'Kinerja');
    worksheet.getCell('AK3').alignment = alignmentStyle;
    worksheet.getCell('AK3').font = {bold: true};
    worksheet.mergeCells('AK3:AK4');
    worksheet.getColumn('AK').width = 20;
    worksheet.getCell('AK3').border = borderStyle;

    // satuan
    row3.splice(38, 1, 'Satuan');
    worksheet.getCell('AL3').alignment = alignmentStyle;
    worksheet.getCell('AL3').font = {bold: true};
    worksheet.mergeCells('AL3:AL4');
    worksheet.getColumn('AL').width = 20;
    worksheet.getCell('AL3').border = borderStyle;

    // Persentase Capaian
    row3.splice(39, 1, 'Persentase Capaian');
    worksheet.getCell('AM3').alignment = alignmentStyle;
    worksheet.getCell('AM3').font = {bold: true};
    worksheet.mergeCells('AM3:AM4');
    worksheet.getColumn('AM').width = 20;
    worksheet.getCell('AM3').border = borderStyle;

    //Faktor Pendorong Keberhasilan Kinerja
    worksheet.mergeCells('AN2:AN4');
    worksheet.getCell('AN2').alignment = alignmentStyle;
    worksheet.getCell('AN2').font = {bold: true};
    worksheet.getColumn('AN').width = 80;
    // worksheet.getCell('AN2').fill = fillStyle;
    worksheet.getCell('AN2').border = borderStyle;

    //Faktor Penghambat Pencapaian Kinerja
    worksheet.mergeCells('AO2:AO4');
    worksheet.getCell('AO2').alignment = alignmentStyle;
    worksheet.getCell('AO2').font = {bold: true};
    worksheet.getColumn('AO').width = 80;
    // worksheet.getCell('AO2').fill = fillStyle;
    worksheet.getCell('AO2').border = borderStyle;

    //Tindak Lanjut yang diperlukan dalam Triwulan berikutnya
    worksheet.mergeCells('AP2:AP4');
    worksheet.getCell('AP2').alignment = alignmentStyle;
    worksheet.getCell('AP2').font = {bold: true};
    worksheet.getColumn('AP').width = 80;
    // worksheet.getCell('AP2').fill = fillStyle;
    worksheet.getCell('AP2').border = borderStyle;

    // end header

    // add row
    let rowData = [];

    // 4 to 30
    const centerizeColumns = [4, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39,];

    // add data
    data.forEach((row, key) => {
        let i = key + 1;

        let indikatorData = row.indikatorData;

        if (indikatorData) {
            indikatorData.forEach((indikator, key) => {
                let _row = worksheet.addRow([
                    i,
                    `<b>Tujuan</b>: ${row.namaTujuan}`,
                    indikator.NamaIndikator,
                    indikator.target,
                    indikator.satuan,
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    indikator.realisasiOutcome,
                    indikator.satuan,
                    `${indikator.persenRealisasiOutcome}%`,
                    '',
                    '',
                    '',
                ]);

                //height
                _row.height = 60;

                //add border to each cell
                _row.eachCell((cell) => {
                    //get cell value
                    const value = cell.value;

                    cell.border = borderStyle;
                    cell.alignment = textAlignmentStyle;

                    if (centerizeColumns.includes(cell.col)) {
                        cell.alignment = alignmentStyle;
                    }

                    if (cell.col === 2 || cell.col === 3) {
                        cell.value = {
                            'richText': htmlToRichText(value),
                        };
                    }
                });
            });
        }

        let dataSasaran = row.dataSasaran;
        if (dataSasaran) {
            dataSasaran.forEach((sasaran, key) => {
                let n = key + 1;

                let indikator = sasaran.indikatorData;
                if (indikator) {
                    let fRow = worksheet.addRow([
                        `${i}.${n}`,
                        `<b>Sasaran</b>: ${sasaran.namaSasaran}`,
                        indikator[0].NamaIndikator,
                        indikator[0].target,
                        indikator[0].satuan,
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        indikator[0].realisasiOutcome,
                        indikator[0].satuan,
                        `${indikator[0].persenRealisasiOutcome}%`,
                        '',
                        '',
                        '',
                    ]);

                    //get row number
                    const rowNumber = fRow.number;
                    const maxRowNumber = rowNumber + indikator.length - 1;

                    // merge A column
                    worksheet.mergeCells(`A${rowNumber}:A${maxRowNumber}`);
                    worksheet.mergeCells(`B${rowNumber}:B${maxRowNumber}`);

                    //height
                    fRow.height = 60;

                    //add border to each cell
                    fRow.eachCell((cell) => {
                        //get cell value
                        const value = cell.value;

                        cell.border = borderStyle;
                        cell.alignment = textAlignmentStyle;

                        if (centerizeColumns.includes(cell.col)) {
                            cell.alignment = alignmentStyle;
                        }

                        if (cell.col === 2 || cell.col === 3) {
                            cell.value = {
                                'richText': htmlToRichText(value),
                            };
                        }
                    });

                    indikator.forEach((item, key) => {
                        if (key === 0) {
                            return;
                        }

                        let thisRowNumber = rowNumber + key;

                        let _row = worksheet.getRow(thisRowNumber);
                        _row.getCell(3).value = item.NamaIndikator;
                        _row.getCell(4).value = item.target;
                        _row.getCell(5).value = item.satuan;

                        for (let i = 6; i <= 36; i++) {
                            _row.getCell(i).value = '';
                        }

                        _row.getCell(37).value = item.realisasiOutcome;
                        _row.getCell(38).value = item.satuan;
                        _row.getCell(39).value = `${item.persenRealisasiOutcome}%`;

                        _row.getCell(40).value = '';
                        _row.getCell(41).value = '';
                        _row.getCell(42).value = '';

                        _row.height = 60;

                        _row.eachCell((cell) => {
                            //get cell value
                            const value = cell.value;

                            cell.border = borderStyle;
                            cell.alignment = textAlignmentStyle;

                            if (centerizeColumns.includes(cell.col)) {
                                cell.alignment = alignmentStyle;
                            }

                            if (cell.col === 3) {
                                cell.value = {
                                    'richText': htmlToRichText(value),
                                };
                            }
                        });
                    });
                }

                let dataProgram = sasaran.dataProgram ?? [];
                if (dataProgram) {
                    dataProgram.forEach((program, index) => {
                        let number = index + 1;

                        let indikator = program.indikatorData;
                        if (indikator) {
                            let fRow = worksheet.addRow([
                                `${i}.${n}.${number}`,
                                `<b>Program</b>: ${program.namaProgram}`,
                                indikator[0].NamaIndikator,
                                indikator[0].target,
                                indikator[0].satuan,
                                formatRupiah(program.targetPaguIndikatif),
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                indikator[0].realisasiOutcome,
                                indikator[0].satuan,
                                `${indikator[0].persenRealisasiOutcome}%`,
                                '',
                                '',
                                '',
                            ]);

                            //get row number
                            const rowNumber = fRow.number;
                            const maxRowNumber = rowNumber + indikator.length - 1;

                            //height
                            fRow.height = 60;

                            //add border to each cell
                            fRow.eachCell((cell) => {
                                //get cell value
                                const value = cell.value;

                                cell.border = borderStyle;
                                cell.alignment = textAlignmentStyle;

                                if (centerizeColumns.includes(cell.col)) {
                                    cell.alignment = alignmentStyle;
                                }

                                if (cell.col === 2 || cell.col === 3) {
                                    cell.value = {
                                        'richText': htmlToRichText(value),
                                    };
                                }
                            });

                            indikator.forEach((item, key) => {
                                if (key === 0) {
                                    return;
                                }

                                let thisRowNumber = rowNumber + key;

                                let _row = worksheet.getRow(thisRowNumber);
                                _row.getCell(3).value = item.NamaIndikator;
                                _row.getCell(4).value = item.target;
                                _row.getCell(5).value = item.satuan;
                                _row.getCell(6).value = formatRupiah(program.targetPaguIndikatif);

                                for (let i = 7; i <= 36; i++) {
                                    _row.getCell(i).value = '';
                                }

                                _row.getCell(37).value = item.realisasiOutcome;
                                _row.getCell(38).value = item.satuan;
                                _row.getCell(39).value = `${item.persenRealisasiOutcome}%`;

                                _row.getCell(40).value = '';
                                _row.getCell(41).value = '';
                                _row.getCell(42).value = '';

                                _row.height = 60;

                                _row.eachCell((cell) => {
                                    //get cell value
                                    const value = cell.value;

                                    cell.border = borderStyle;
                                    cell.alignment = textAlignmentStyle;

                                    if (centerizeColumns.includes(cell.col)) {
                                        cell.alignment = alignmentStyle;
                                    }

                                    if (cell.col === 3) {
                                        cell.value = {
                                            'richText': htmlToRichText(value),
                                        };
                                    }
                                });
                            });
                        }

                        let dataKegiatan = program.dataKegiatan ?? [];
                        if (dataKegiatan) {
                            dataKegiatan.forEach((kegiatan, index) => {
                                let numberKegiatan = index + 1;

                                let indikator = kegiatan.indikatorData;
                                if (indikator) {
                                    let fRow = worksheet.addRow([
                                        `${i}.${n}.${number}.${numberKegiatan}`,
                                        `<b>Kegiatan</b>: ${kegiatan.namaKegiatan}`,
                                        indikator[0].NamaIndikator,
                                        indikator[0].target,
                                        indikator[0].satuan,
                                        formatRupiah(kegiatan.targetPaguIndikatif),
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        indikator[0].realisasiOutcome,
                                        indikator[0].satuan,
                                        `${indikator[0].persenRealisasiOutcome}%`,
                                        '',
                                        '',
                                        '',
                                    ]);

                                    //get row number
                                    const rowNumber = fRow.number;
                                    const maxRowNumber = rowNumber + indikator.length - 1;

                                    //height
                                    fRow.height = 60;

                                    //add border to each cell
                                    fRow.eachCell((cell) => {
                                        //get cell value
                                        const value = cell.value;

                                        cell.border = borderStyle;
                                        cell.alignment = textAlignmentStyle;

                                        if (centerizeColumns.includes(cell.col)) {
                                            cell.alignment = alignmentStyle;
                                        }

                                        if (cell.col === 2 || cell.col === 3) {
                                            cell.value = {
                                                'richText': htmlToRichText(value),
                                            };
                                        }
                                    });

                                    indikator.forEach((item, key) => {
                                        if (key === 0) {
                                            return;
                                        }

                                        let thisRowNumber = rowNumber + key;

                                        let _row = worksheet.getRow(thisRowNumber);
                                        _row.getCell(3).value = item.NamaIndikator;
                                        _row.getCell(4).value = item.target;
                                        _row.getCell(5).value = item.satuan;
                                        _row.getCell(6).value = formatRupiah(kegiatan.targetPaguIndikatif);

                                        for (let i = 7; i <= 36; i++) {
                                            _row.getCell(i).value = '';
                                        }

                                        _row.getCell(37).value = item.realisasiOutcome;
                                        _row.getCell(38).value = item.satuan;
                                        _row.getCell(39).value = `${item.persenRealisasiOutcome}%`;

                                        _row.getCell(40).value = '';
                                        _row.getCell(41).value = '';
                                        _row.getCell(42).value = '';

                                        _row.height = 60;

                                        _row.eachCell((cell) => {
                                            //get cell value
                                            const value = cell.value;

                                            cell.border = borderStyle;
                                            cell.alignment = textAlignmentStyle;

                                            if (centerizeColumns.includes(cell.col)) {
                                                cell.alignment = alignmentStyle;
                                            }

                                            if (cell.col === 3) {
                                                cell.value = {
                                                    'richText': htmlToRichText(value),
                                                };
                                            }
                                        });
                                    });
                                }

                                let dataSubKegiatan = kegiatan.dataSubKegiatan ?? [];
                                if (dataSubKegiatan) {
                                    dataSubKegiatan.forEach((subKegiatan, index) => {
                                        let subKegiatanNumber = index + 1;

                                        let indikator = subKegiatan.indikatorData;
                                        if (indikator) {
                                            let triwulan = [
                                                // 'target kinerja',
                                                // 'satuan (%)',
                                                // 'realisasi kinerja',
                                                // 'satuan (%)',
                                                // 'rp',
                                                // '(%)',
                                                // 'target kinerja',
                                                // 'satuan (%)',
                                                // 'realisasi kinerja',
                                                // 'satuan (%)',
                                                // 'rp',
                                                // '(%)',
                                                // 'target kinerja',
                                                // 'satuan (%)',
                                                // 'realisasi kinerja',
                                                // 'satuan (%)',
                                                // 'rp',
                                                // '(%)',
                                                // 'target kinerja',
                                                // 'satuan (%)',
                                                // 'realisasi kinerja',
                                                // 'satuan (%)',
                                                // 'rp',
                                                // '(%)',
                                            ];

                                            let targetDanCapaian = subKegiatan.targetDanCapaian;

                                            let targetPaguIndikatif = subKegiatan.targetPaguIndikatif;
                                            let totalCapaianTriwulan = 0;

                                            if (targetDanCapaian) {
                                                let dataTriwulan = targetDanCapaian.dataTriwulan;

                                                if (dataTriwulan) {
                                                    dataTriwulan[0].forEach((triwulanData, index) => {
                                                        totalCapaianTriwulan += triwulanData.rp;

                                                        triwulan.push(triwulanData.target);
                                                        triwulan.push(indikator[0] ? indikator[0].satuan : '');
                                                        triwulan.push(formatNumber(triwulanData.realisasiKinerja));
                                                        triwulan.push(indikator[0] ? indikator[0].satuan : '');
                                                        triwulan.push(formatRupiah(triwulanData.rp));
                                                        triwulan.push(formatNumber(triwulanData.persenRealisasi));
                                                    });
                                                }
                                            }

                                            let persenCapaianTriwulanBerdasarkanPaguIndikatif = (totalCapaianTriwulan / targetPaguIndikatif) * 100;

                                            let fRow = worksheet.addRow([
                                                `${i}.${n}.${number}.${numberKegiatan}.${subKegiatanNumber}`,
                                                `<b>Sub Kegiatan</b>: ${subKegiatan.namaSubKegiatan}`,
                                                indikator[0] ? indikator[0].NamaIndikator : '',
                                                indikator[0] ? indikator[0].target : '',
                                                indikator[0] ? indikator[0].satuan : '',
                                                formatRupiah(subKegiatan.targetPaguIndikatif),
                                                '100',
                                                ...triwulan,
                                                targetDanCapaian.TotalKinerja,
                                                indikator[0] ? indikator[0].satuan : '',
                                                formatRupiah(totalCapaianTriwulan),
                                                formatNumber(persenCapaianTriwulanBerdasarkanPaguIndikatif),
                                                '%',
                                                indikator[0] ? indikator[0].realisasiOutcome : '',
                                                indikator[0] ? indikator[0].satuan : '',
                                                indikator[0] ? `${indikator[0].persenRealisasiOutcome}%` : '',
                                                indikator[0] ? indikator[0].faktorPendorong : '',
                                                indikator[0] ? indikator[0].faktorPenghambat : '',
                                                indikator[0] ? indikator[0].tindakLanjut : '',
                                            ]);

                                            //height
                                            fRow.height = 60;

                                            //add border to each cell
                                            fRow.eachCell((cell) => {
                                                //get cell value
                                                const value = cell.value;

                                                cell.border = borderStyle;
                                                cell.alignment = textAlignmentStyle;

                                                if (centerizeColumns.includes(cell.col)) {
                                                    cell.alignment = alignmentStyle;
                                                }

                                                if (cell.col === 2 || cell.col === 3) {
                                                    cell.value = {
                                                        'richText': htmlToRichText(value),
                                                    };
                                                }
                                            });

                                            indikator.forEach((item, key) => {
                                                if (key === 0) {
                                                    return;
                                                }

                                                let thisRowNumber = rowNumber + key;

                                                let _row = worksheet.getRow(thisRowNumber);
                                                _row.getCell(3).value = item.NamaIndikator;
                                                _row.getCell(4).value = item.target;
                                                _row.getCell(5).value = item.satuan;
                                                _row.getCell(6).value = formatRupiah(subKegiatan.targetPaguIndikatif);

                                                for (let i = 7; i <= 36; i++) {
                                                    _row.getCell(i).value = '';
                                                }

                                                _row.getCell(37).value = item.realisasiOutcome;
                                                _row.getCell(38).value = item.satuan;
                                                _row.getCell(39).value = `${item.persenRealisasiOutcome}%`;

                                                _row.getCell(40).value = '';
                                                _row.getCell(41).value = '';
                                                _row.getCell(42).value = '';

                                                _row.height = 60;

                                                _row.eachCell((cell) => {
                                                    //get cell value
                                                    const value = cell.value;

                                                    cell.border = borderStyle;
                                                    cell.alignment = textAlignmentStyle;

                                                    if (centerizeColumns.includes(cell.col)) {
                                                        cell.alignment = alignmentStyle;
                                                    }

                                                    if (cell.col === 3) {
                                                        cell.value = {
                                                            'richText': htmlToRichText(value),
                                                        };
                                                    }
                                                });
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
    });

    // get last row
    let lastRow = worksheet.lastRow;

   // last row + 1
    let nextLastRow = lastRow.number + 2;

    let getLastRowCell = worksheet.getRow(nextLastRow);


    getLastRowCell.getCell('A').value = `Dicetak dari web PEDe pada ${moment().format('DD MMMM YYYY HH:mm:ss')}`;

    try {
        await fs.promises.mkdir(path, {recursive: true});

        //remove old file
        const files = fs.readdirSync(path);
        for (const file of files) {
            try {
                fs.unlinkSync(`${path}/${file}`);
            } catch (err) {

            }
        }

        await workbook.xlsx.writeFile(`${path}/${fileName}`);

        return {
            code: 201,
            success: true,
            message: 'Berhasil membuat file laporan',
            data: {
                file_name: fileName,
            },
        }
    } catch (err) {
        console.error('Error:', err.message);

        return {
            code: 500,
            success: false,
            message: 'Gagal membuat file laporan',
            error: err.message,
        }
    }
};

export default {
    renja,
}