const formatRupiah = (number) => {
    let formatted =  new Intl.NumberFormat('id-ID', {
        style: 'currency',
        currency: 'IDR',
        minimumFractionDigits: 0,
    }).format(number);

    return formatted;
}

const formatNumber = (number, maximumFraction = 2) => {
    // check is number is not a number
    if(isNaN(number)) {
        return '';
    }

    return new Intl.NumberFormat('id-ID', {
        maximumFractionDigits: maximumFraction,
    }).format(number);
}

export {formatRupiah, formatNumber};