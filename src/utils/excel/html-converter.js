const htmlToRichText = (string, font = {}) => {
    if (string === '' || string === null || string === undefined) {
        return {
            'font': {
                'size': font.size ?? 12,
                'color': {'theme': 1},
                'name': font.name ?? 'Calibri',
                'family': 2,
                'scheme': 'minor'
            }, 'text': ' ',
        };
    }

    const cellValue = [];
    const words = string.split(' ');

    words.forEach((word, index) => {
        if (word.includes('<b>') || word.includes('</b>') || word.includes('<strong>') || word.includes('</strong>')) {
            // get word
            const text = word.replace('<b>', '').replace('</b>', '').replace('<strong>', '').replace('</strong>', '');

            cellValue.push(
                {
                    'font': {
                        'bold': true,
                        'size': font.size ?? 12,
                        'color': {'theme': 1},
                        'name': font.name ?? 'Calibri',
                        'family': 2,
                        'scheme': 'minor'
                    }, 'text': `${text} `,
                }
            );
        } else if (word.includes('<i>') || word.includes('</i>') || word.includes('<em>') || word.includes('</em>')) {
            const text = word.replace('<i>', '').replace('</i>', '').replace('<em>', '').replace('</em>', '');

            cellValue.push(
                {
                    'font': {
                        'italic': true,
                        'size': font.size ?? 12,
                        'color': {'theme': 1},
                        'name': font.name ?? 'Calibri',
                        'family': 2,
                        'scheme': 'minor'
                    }, 'text': `${text} `,
                }
            );
        } else {
            cellValue.push(
                {
                    'font': {
                        'size': font.size ?? 12,
                        'color': {'theme': 1},
                        'name': font.name ?? 'Calibri',
                        'family': 2,
                        'scheme': 'minor'
                    }, 'text': `${word} `,
                }
            );
        }
    });

    return cellValue;
}

export {
    htmlToRichText
}