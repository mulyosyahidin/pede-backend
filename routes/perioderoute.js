import {Router} from 'express'
import { body, param} from 'express-validator'
import requestValidator from '../middlewares/requestValidator.js'
import * as controller from '../controllers/periodecontroller.js'
import cekToken from '../middlewares/cekTokenHeader.js'

const router = Router()

router.use(cekToken)
router.post(
    '/add',
    body('namaPeriode').notEmpty(), 
    body('waktuMulai').notEmpty(), 
    body('waktuAkhir').notEmpty(),
    requestValidator,
    controller.add
)
router.put(
    '/update',
    body('idPeriode').notEmpty().isNumeric(),
    body('status').notEmpty().isNumeric(),
    requestValidator,
    controller.update
)

router.get(
    '/list',
    controller.list
)

router.delete(
    '/hapus/:id',
    param("id").notEmpty().isNumeric(),
    requestValidator,
    controller.hapusPeriode
)

router.get(
    '/list/:id',
    param("id").notEmpty().isNumeric(),
    requestValidator,
    controller.getSingle
)

router.get(
    '/list/tahun/:tahun',
    param("tahun").notEmpty().isNumeric(),
    requestValidator,
    controller.getTahun
)

router.get(
    '/get',
    controller.getUser
)

export default router