import {Router} from 'express'
import { body, param} from 'express-validator'
import requestValidator from '../middlewares/requestValidator.js'
import * as controller from '../controllers/uraiancontroller.js'
import cekToken from '../middlewares/cekTokenHeader.js'

const router = Router()
router.use(cekToken)
router.post('/add',
    body('idSubKegiatan').notEmpty().isNumeric(),
    body('tahun').notEmpty().isNumeric(),
    body('opd').notEmpty().isNumeric(),
    body('namaUraian').notEmpty(),
    body('satuan').notEmpty(),
    body('targetVolume').notEmpty(),
    requestValidator,
    controller.adding
)

router.put('/update',
    body('idUraian').notEmpty().isNumeric(),
    body('tahun').notEmpty().isNumeric(),
    body('opd').notEmpty().isNumeric(),
    body('namaUraian').notEmpty(),
    body('satuan').notEmpty(),
    body('targetVolume').notEmpty(),
    requestValidator,
    controller.updating
)

router.get('/list/:tahun/:opd',
    param('tahun').notEmpty().isNumeric(),
    param('opd').notEmpty().isNumeric(),
    requestValidator,
    controller.fetch
)

router.delete('/hapus/:id',
    param('id').notEmpty().isNumeric(),
    requestValidator,
    controller.hapusData
)


export default router