import {Router} from "express";
import cekToken from "../middlewares/cekTokenHeader.js";
import rkpdController from "../src/controllers/rkpd-controller.js";

const router = Router();
router.use(cekToken);

router.get('/evaluasi/:idOpd', rkpdController.evaluasi);
router.get('/evaluasi/:idOpd/excel', rkpdController.excel);

export default router;