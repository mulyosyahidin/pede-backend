import {Router} from 'express'
import { body, param} from 'express-validator'
import requestValidator from '../middlewares/requestValidator.js'
import * as controller from '../controllers/rpjmdcontroller.js'
import cekToken from '../middlewares/cekTokenHeader.js'

const router = Router()
router.use(cekToken)
router.post('/input/capaian/add',
    body('idIndikator').notEmpty().isNumeric(),
    body('idPeriode').notEmpty().isNumeric(),
    body('tahun').notEmpty().isNumeric(),
    body('targetRpjmd').notEmpty(),
    body('capaianRpjmd').notEmpty(),
    requestValidator,
    controller.inputRealisasi
)

router.put('/input/capaian/update',
    body('idIndikator').notEmpty().isNumeric(),
    body('idPeriode').notEmpty().isNumeric(),
    body('tahun').notEmpty().isNumeric(),
    body('targetRpjmd').notEmpty(),
    body('capaianRpjmd').notEmpty(),
    requestValidator,
    controller.updateRealisasi
)

router.get('/input/capaian/list/:tahun/:idPeriode',
    param('tahun').notEmpty().isNumeric(),
    param('idPeriode').notEmpty().isNumeric(),
    requestValidator,
    controller.fetchCapaian
)

router.post('/input/faktor/add',
    body('tahun').notEmpty().isNumeric(),
    body('idPeriode').notEmpty().isNumeric(),
    body('faktorPenghambat').notEmpty(),
    body('faktorPendorong').notEmpty(),
    body('rekomTindakLanjut').notEmpty(),
    requestValidator,
    controller.createFaktor
)

router.put('/input/faktor/update',
    body('tahun').notEmpty().isNumeric(),
    body('idPeriode').notEmpty().isNumeric(),
    body('faktorPenghambat').notEmpty(),
    body('faktorPendorong').notEmpty(),
    body('rekomTindakLanjut').notEmpty(),
    requestValidator,
    controller.ubahFaktor
)

router.get('/input/faktor/list/:tahun/:idPeriode',
    param('tahun').notEmpty().isNumeric(),
    param('idPeriode').notEmpty().isNumeric(),
    requestValidator,
    controller.fetchFaktor
)

router.get('/report/:idPeriode',
    param('idPeriode').notEmpty().isNumeric(),
    requestValidator,
    controller.fetchReport
)

router.post('/input/finalisasi',
    body('idPeriode').notEmpty().isNumeric(),
    body('idRpjmd').notEmpty(),
    requestValidator,
    controller.approvRpjmd
)

router.get('/input/approv/:idPeriode',
    param('idPeriode').notEmpty().isNumeric(),
    requestValidator,
    controller.fetchApproval
)

router.post('/input/approval',
    body('idPeriode').notEmpty().isNumeric(),
    body('status').notEmpty().isNumeric(),
    body('idRpjmd').notEmpty(),
    requestValidator,
    controller.actionApproval
)

router.get('/input/revisi/:idPeriode',
    param('idPeriode').notEmpty().isNumeric(),
    requestValidator,
    controller.fetchRevisi
)

router.post('/input/catatan',
    body('idPeriode').notEmpty().isNumeric(),
    body('idRpjmd').notEmpty().isNumeric(),
    requestValidator,
    controller.fetchCatatan
)
export default router