import {Router} from 'express'
import { body, param} from 'express-validator'
import requestValidator from '../middlewares/requestValidator.js'
import * as controller from '../controllers/instrumencontroller.js'
import cekToken from '../middlewares/cekTokenHeader.js'

const router = Router()
router.use(cekToken)

router.post('/add',
    body('idSubUrusan').notEmpty().isNumeric(),
    body('dataDesc').notEmpty(),
    body('type').notEmpty(),
    requestValidator,
    controller.inputEvals
)

router.get('/list/:tahun/:opd',
    param('tahun').notEmpty().isNumeric(),
    param('opd').notEmpty().isNumeric(),
    requestValidator,
    controller.listEvals
)

router.get('/tujuan/:tahun/:opd',
    param('tahun').notEmpty().isNumeric(),
    param('opd').notEmpty().isNumeric(),
    requestValidator,
    controller.tujuanList
)

router.delete('/hapus/:id/:type',
    param('id').notEmpty().isNumeric(),
    param('type').notEmpty(),
    requestValidator,
    controller.hapusData
)

router.put('/update',
    body('idInstrumen').notEmpty().isNumeric(),
    body('tahun').notEmpty().isNumeric(),
    body('nama').notEmpty(),
    body('indikator').notEmpty(),
    body('type').notEmpty(),
    requestValidator,
    controller.updateData
)

router.get('/log/:idData',
    param('idData').notEmpty().isNumeric(),
    requestValidator,
    controller.instrumenLog
)

router.get('/data/tujuan/:opd/:tahun',
    param('opd').notEmpty().isNumeric(),
    param('tahun').notEmpty().isNumeric(),
    requestValidator,
    controller.getTujuanBysub
)

router.get('/data/sasaran/:opd/:tahun',
    param('opd').notEmpty().isNumeric(),
    param('tahun').notEmpty().isNumeric(),
    requestValidator,
    controller.getSasaranByTujuan
)

router.get('/data/program/:opd/:tahun',
    param('opd').notEmpty().isNumeric(),
    param('tahun').notEmpty().isNumeric(),
    requestValidator,
    controller.getProgramBySasaran
)

router.get('/data/kegiatan/:opd/:tahun',
    param('opd').notEmpty().isNumeric(),
    param('tahun').notEmpty().isNumeric(),
    requestValidator,
    controller.getKegiatanByProgram
)

router.get('/data/subkegiatan/:opd/:tahun',
    param('opd').notEmpty().isNumeric(),
    param('tahun').notEmpty().isNumeric(),
    requestValidator,
    controller.getSubByKegiatan
)

export default router