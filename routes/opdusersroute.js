import {Router} from 'express'
import { body, param} from 'express-validator'
import requestValidator from '../middlewares/requestValidator.js'
import * as controller from '../controllers/opdusercontroller.js'
import cekToken from '../middlewares/cekTokenHeader.js'

const router = Router()

router.use(cekToken)
router.get('/list/:page',
    param('page').notEmpty().isNumeric(), 
    requestValidator,
    controller.list
)
router.get('/list-all',controller.listAll);
router.post('/add',body('namaOpd').notEmpty(), requestValidator ,controller.add)
router.put('/update',body('id').notEmpty(), requestValidator ,controller.update)
router.delete('/hapus/:id',param('id').isNumeric().notEmpty(), requestValidator ,controller.hapus)
router.get('/single/:id',param('id').isNumeric().notEmpty(), requestValidator ,controller.getSingles)

router.get('/list/keyword/:page/:keyword',
    param('page').notEmpty().isNumeric(), 
    requestValidator,
    controller.listKeyword
)

export default router