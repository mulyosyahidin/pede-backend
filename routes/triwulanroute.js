import {Router} from 'express'
import { body, param} from 'express-validator'
import requestValidator from '../middlewares/requestValidator.js'
import * as controller from '../controllers/triwulancontroller.js'
import cekToken from '../middlewares/cekTokenHeader.js'

const router = Router()
router.use(cekToken)
router.get('/list/:tahun/:opd',
    param('tahun').notEmpty().isNumeric(),
    param('opd').notEmpty().isNumeric(),
    requestValidator,
    controller.listing
)
export default router