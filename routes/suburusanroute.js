import {Router} from 'express'
import { body, param} from 'express-validator'
import requestValidator from '../middlewares/requestValidator.js'
import * as controller from '../controllers/suburusancontoller.js'
import cekToken from '../middlewares/cekTokenHeader.js'

const router = Router()

router.use(cekToken)
router.post('/add',
    body("idUrusan").notEmpty().isNumeric(),
    body("namaSub").notEmpty(),
    requestValidator,
    controller.adds
)

router.put('/update',
    body("idSub").notEmpty().isNumeric(),
    body("data").notEmpty(),
    requestValidator,
    controller.updates
)

router.delete('/hapus/:idSub',
    param("idSub").notEmpty().isNumeric(),
    requestValidator,
    controller.deletes
)

router.get('/listall/:page',
    param("page").notEmpty().isNumeric(),
    requestValidator,
    controller.list
)

router.get('/list/:idUrusan/:page',
    param("idUrusan").notEmpty().isNumeric(),
    param("page").notEmpty().isNumeric(),
    requestValidator,
    controller.getUrusanId
)

router.get('/list/keyword/:idUrusan/:namaSub',
    param("idUrusan").notEmpty().isNumeric(),
    param("namaSub").notEmpty(),
    requestValidator,
    controller.getByname
)

router.get('/single/:idSub',
    param("idSub").notEmpty().isNumeric(),
    requestValidator,
    controller.getSingle
)

export default router