import {Router} from 'express'
import { body, param} from 'express-validator'
import requestValidator from '../middlewares/requestValidator.js'
import * as controller from '../controllers/tahuncontroller.js'
import cekToken from '../middlewares/cekTokenHeader.js'

const router = Router()
router.use(cekToken)
router.post('/add',
    body('tahunSatu').notEmpty().isNumeric(),
    body('tahunDua').notEmpty().isNumeric(),
    body('tahunTiga').notEmpty().isNumeric(),
    body('tahunEmpat').notEmpty().isNumeric(),
    body('tahunLima').notEmpty().isNumeric(),
    requestValidator,
    controller.tahun
)

router.put('/update',
    body('idTh').notEmpty().isNumeric(),
    body('data').notEmpty(),
    requestValidator,
    controller.tahunUpdate
)

router.get('/list',
    controller.tahunList
)

// router.get('/list',
//     param('idTh').notEmpty().isNumeric(),
//     requestValidator,
//     controller.tahunList
// )

export default router