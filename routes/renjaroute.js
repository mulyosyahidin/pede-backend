import {Router} from 'express'
import { body, param} from 'express-validator'
import requestValidator from '../middlewares/requestValidator.js'
import * as controller from '../controllers/renjacontroller.js'
import cekToken from '../middlewares/cekTokenHeader.js'

const router = Router()
router.use(cekToken)
router.post('/input/add',
    body('idPeriode').notEmpty().isNumeric(),
    body('idUraian').notEmpty().isNumeric(),
    body('dataCapaian').notEmpty(),
    requestValidator,
    controller.input
)


router.get('/list/:idPeriode',
    param('idPeriode').notEmpty().isNumeric(),
    requestValidator,
    controller.fetch
)

router.post('/rupiah/add',
    body('idPeriode').notEmpty().isNumeric(),
    body('idSubKegiatan').notEmpty().isNumeric(),
    body('dataPagu').notEmpty(),
    requestValidator,
    controller.rupiahInput
)

router.get('/rupiah/list/:idPeriode',
    param('idPeriode').notEmpty().isNumeric(),
    requestValidator,
    controller.rupiahList
)

router.post('/outcome/add',
    body('idIndikator').notEmpty().isNumeric(),
    body('idPeriode').notEmpty().isNumeric(),
    body('realisasiOutcome').notEmpty().isNumeric(),
    requestValidator,
    controller.inputOutcome
)

router.get('/outcome/list/:tahun/:opd',
    param('tahun').notEmpty().isNumeric(),
    param('opd').notEmpty().isNumeric(),
    requestValidator,
    controller.listOutcome
)

router.post('/faktor/add',
    body('idIndikator').notEmpty().isNumeric(),
    body('idPeriode').notEmpty().isNumeric(),
    requestValidator,
    controller.inputFaktor
)

router.get('/faktor/list/:tahun/:opd',
    param('tahun').notEmpty().isNumeric(),
    param('opd').notEmpty().isNumeric(),
    requestValidator,
    controller.laporanAkhir
)

router.get('/faktor/list/:tahun/:opd/excel',
    param('tahun').notEmpty().isNumeric(),
    param('opd').notEmpty().isNumeric(),
    requestValidator,
    controller.excelLaporan,
)

router.get('/final/list/:tahun',
    requestValidator,
    controller.listFinalisasi);

export default router
