import {Router} from 'express'
import { body, param} from 'express-validator'
import requestValidator from '../middlewares/requestValidator.js'
import * as controller from '../controllers/hakaksescontroller.js'
import cekToken from '../middlewares/cekTokenHeader.js'

const router = Router()
router.use(cekToken)

router.post('/add',
    body('idUser').notEmpty().isNumeric(),
    requestValidator,
    controller.addHak
)

export default router