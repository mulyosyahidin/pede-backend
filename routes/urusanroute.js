import {Router} from 'express'
import { body, param} from 'express-validator'
import requestValidator from '../middlewares/requestValidator.js'
import * as controller from '../controllers/urusancontroller.js'
import cekToken from '../middlewares/cekTokenHeader.js'

const router = Router()

router.use(cekToken)
router.post('/add',
    body('namaUrusan').notEmpty(),
    requestValidator,
    controller.add
)

router.put('/update',
    body('idUrusan').notEmpty().isNumeric(),
    requestValidator,
    controller.update
)

router.delete('/hapus/:id',
    param('id').notEmpty().isNumeric(),
    requestValidator,
    controller.hapus
)

router.get('/list/:page',
    param('page').notEmpty().isNumeric(),
    requestValidator,
    controller.listAlls
)

router.get('/list/:id',
    param('id').notEmpty().isNumeric(),
    requestValidator,
    controller.singleGet
)

router.get('/list/filter/:nama',
    param('nama').notEmpty(),
    requestValidator,
    controller.byName
)
export default router