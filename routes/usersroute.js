// const {Router} = require('express')
import {Router} from 'express'
import { body, param} from 'express-validator'
import requestValidator from './../middlewares/requestValidator.js'
import * as controller from './../controllers/usercontroller.js'
import cekToken from '../middlewares/cekTokenHeader.js'

const router = Router()

router.use(cekToken)
router.post('/add', body('username').notEmpty(), body('password').notEmpty(),body('email').notEmpty(),body('kodeOdp').notEmpty(),body('kodeRole').notEmpty(), requestValidator, controller.add)
router.put('/update', body('id').notEmpty(), requestValidator, controller.update)
router.delete('/delete/:id', param('id').isNumeric(), requestValidator ,controller.hapus)
router.get('/listall/:page',
    param('page').isNumeric().notEmpty(),
    requestValidator,
    controller.listAll
)
router.get('/get/:id',param('id').isNumeric(), requestValidator, controller.getUsers)
router.get('/getsingle', controller.getUserSingle)
router.delete('/removetoken', controller.removeTokens)
router.put('/updatesingle',controller.updateUsers)
router.get('/rolelist',controller.roleAlluser)
router.get('/list/keyword/:page/:keyword',
    param('page').isNumeric().notEmpty(),
    requestValidator,
    controller.listKeyword
)


export default router
