// const {Router} = require('express')
import {Router} from 'express'
import { body } from 'express-validator'
import requestValidator from './../middlewares/requestValidator.js'
import * as controller from './../controllers/logincontroller.js'


const router = Router()

router.post('/auth', body('username').notEmpty(), body('password').notEmpty(), requestValidator, controller.auth)

export default router
