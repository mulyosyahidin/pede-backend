// require("dotenv/config")
// const express = require("express")

import "dotenv/config"
import express from "express"
import router from "./routes.js"
import errorMidleware from "./middlewares/errormidlewarehandler.js"
import morgan from "morgan";
import cors from "cors";

process.env.TZ = 'Asia/Jakarta'

const app = express()
const host = process.env.HOST ?? '127.0.0.1'
const port = parseInt(process.env.PORT ?? '9898')

app.use(express.urlencoded({ extended: true }))
app.use(express.json())
app.use(morgan('dev'));
app.use(cors())
app.use('/', router)
app.use(errorMidleware)

app.listen(port, host, () => {
    console.log(`listen express service http://${host}:${port}`)
})


